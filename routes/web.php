<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*--------------------------------------MODULO ORGANIZACION-----------------------------------------------*/
Route::get('historia','OrganizacionController@historia');



/*--------------------------------------WORKFLOW SERVICIOS GENERALES---------------------------------------*/
/*                                           INDEX DE ESTE MODULO                                          */
/*---------------------------------------------------------------------------------------------------------*/

Route::get('servicios-empleados','ServiciosController@index');

/*--------------------------------------WORKFLOW SERVICIOS GENERALES---------------------------------------*/
/*                                           PDF                                         */
/*---------------------------------------------------------------------------------------------------------*/
Route::get('formulario', 'PdfController@index');
Route::post('pdf/create', 'PdfController@save');
Route::get('pdf', 'PdfController@visualizarpdf');


/*--------------------------------------WORKFLOW ADMINISTRACION--------------------------------------------*/
/*                                           MATERIAL PROVEEDURIA                                          */
/*---------------------------------------------------------------------------------------------------------*/

Route::get('workflow-material-proveeduria','WfadminController@index');



Route::get('reporte-mantenimiento','WfadminController@view_reporte_mantenimiento');
Route::get('reporte-mantenimiento/{id}', 'WfadminController@reporte_mantenimiento')->name('solicitud-mantenimiento.pdf');

/*--------------------------------------WORKFLOW ADMINISTRACION--------------------------------------------*/
/*                                           TRANSPORTE                                                 */
/*---------------------------------------------------------------------------------------------------------*/

Route::get('workflow-transporte','WfadminController@vista_Transporte');
Route::get('crear_workflow_transporte','WfadminController@crear_wf_transporte');
Route::post('registar_workflow_transporte','WfadminController@store_wf_transporte');
Route::get('servicios-empleados','WfadminController@view_trasnporte');


Route::get('editar_wf_transporte/{id}','WfadminController@editar_workflow_transporte');
Route::post('actualizarwftransporte','WfadminController@update_workflow_transporte');

/*--------------------------------------CONSULTAS GENERALES------------------------------------------------*/
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

Route::get('consulta-crediamigo','WfadminController@consulta_gh');
Route::get('consulta-transporte','WfadminController@consulta_ad');
Route::get('consulta-materiales-proveeduria','WfadminController@consulta_mp');
Route::get('consulta-viaticos','WfadminController@consulta_viaticos');
/*--------------------------------------REPORTES GENERALES E INDIVIDUALES----------------------------------*/
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
Route::get('reporte-transporte/{id}', 'WfadminController@reporte_transporte')->name('solicitud-transporte.pdf');
Route::get('reporte-transporte','WfadminController@view_reporte_transporte');




/*--------------------------------------ADMINISTRADOR DE INTRANET NOTICIAS---------------------------------*/
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
Route::get('ba-admin','AdministradorController@index');
Route::get('nueva-noticias','AdministradorController@create_new');
Route::get('categoria-noticia','AdministradorController@view_categoria');
Route::get('crear-categoria','AdministradorController@create_categoria');
Route::post('registrar-categoria','AdministradorController@store_categoria');
Route::get('categoria-noticia','AdministradorController@tabla_categoria');
Route::get('eliminar_cat/{id}','AdministradorController@destroy');
Route::get('categorias','AdministradorController@pgcategoria');
Route::get('editarcategoria/{id}','AdministradorController@edit');
Route::post('actualizarcategoria','AdministradorController@update');
Route::get('editar_categoria','AdministradorController@ver_categoria_edit');


Route::get('noticias','AdministradorController@create_noticia');
Route::post('crear_noticia','AdministradorController@store_noticia');
Route::get('eliminar_not/{id}','AdministradorController@destroy_noticia');
Route::get('editarnoticia/{id}','AdministradorController@edit_noticia');
Route::post('actualizarnoticia','AdministradorController@update_noticia');


Route::get('home','AdministradorController@mostrar_noticias');

Route::get('blog','AdministradorController@blog');


Route::get('blog','AdministradorController@consulta_noticia');

Route::get('categoria-institucional','AdministradorController@categoria_institucional');

Route::get('categoria-administracion','AdministradorController@categoria_administracion');
Route::get('categoria-rrhh','AdministradorController@categoria_rrhh');

Route::get('blog_noticias/{id_catenot}','AdministradorController@noticia_individual');
/*--------------------------------------ADMINISTRADOR SOLICITUDES------------------------------------------*/
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
Route::get('solicitudes','AdministradorController@solicitudes');
Route::get('nueva-solicitud','AdministradorController@nueva_solicitud');

Route::get('crear-solicitud','AdministradorController@create_solicitud');
Route::post('registrar-solicitud','AdministradorController@store_solicitud');

Route::get('eliminar_sol/{id}','AdministradorController@destroy_solicitud');

Route::get('editarsolicitud/{id}','AdministradorController@edit_solicitud');
Route::post('actualizarsolicitud','AdministradorController@update_solicitud');

/*--------------------------------------ADMINISTRADOR tips-------------------------------------------------*/
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

Route::get('ver-tips','AdministradorController@vertips');
Route::get('nuevo_tips','AdministradorController@nuevo_tips');
Route::get('categoria_tips','AdministradorController@categoria_tips');

Route::get('crear-tips','AdministradorController@create_tips');
Route::post('registrar-tips','AdministradorController@store_tips');

Route::get('crear-categoria_tips','AdministradorController@create_tips_categoria');
Route::post('registrar-categoria_tips','AdministradorController@store_tips_categoria');

Route::get('categoria_tips','AdministradorController@mostrar_tips');
Route::get('eliminar_catenot/{id}','AdministradorController@destroy_tips');


Route::get('editarctips/{id}','AdministradorController@edit_ctips');
Route::post('actualizarctips','AdministradorController@update_ctips');

Route::get('editartips/{id}','AdministradorController@edit_tips');
Route::post('actualizartipsbancario','AdministradorController@update_tips_bancario');

Route::get('eliminar_tipsbancarios/{id}','AdministradorController@destroy_tipsbancario');

Route::get('blog_tips','AdministradorController@blog_tips');

Route::get('vermastips/{id_catips}','AdministradorController@ver_mas');


/*--------------------------------------ADMINISTRADOR HISTORIA----------------------------------------------------*/
/*                                                                                                                */
/*----------------------------------------------------------------------------------------------------------------*/

Route::get('redactar_historia','AdministradorController@redactar_historia');
Route::get('vista_historia','AdministradorController@vista_historia');

Route::get('crear-historia','AdministradorController@create_historia');
Route::post('registrar-historia','AdministradorController@store_historia');

Route::get('historia','AdministradorController@mostrar_historia');

Route::get('editarhistoria/{id}','AdministradorController@edit_historia');
Route::post('actualizarhistoria','AdministradorController@update_historia');

Route::get('sliders','AdministradorController@slider');
Route::get('crear-slider','AdministradorController@create_slider');
Route::post('registrar-slider','AdministradorController@store_slider');

Route::get('editarslider/{id}','AdministradorController@edit_slider');
Route::post('actualizarslider','AdministradorController@update_slider');

Route::get('eliminar_sliders/{id}','AdministradorController@destroy_slider');

Route::get('mision','AdministradorController@mision');

Route::get('crear-mision','AdministradorController@create_mision');
Route::post('registrar-mision','AdministradorController@store_mision');

Route::get('editarmision/{id}','AdministradorController@edit_mision');
Route::post('actualizarmision','AdministradorController@update_mision');

Route::get('eliminar_mision/{id}','AdministradorController@destroy_mision');

Route::get('vision','AdministradorController@vision');
Route::get('crear-vision','AdministradorController@create_vision');
Route::post('registrar-vision','AdministradorController@store_vision');

Route::get('editarvision/{id}','AdministradorController@edit_vision');
Route::post('actualizarvision','AdministradorController@update_vision');

Route::get('eliminar_vision/{id}','AdministradorController@destroy_vision');


Route::get('valores','AdministradorController@valores');
Route::get('crear-valores','AdministradorController@create_valores');
Route::post('registrar-valores','AdministradorController@store_valores');

Route::get('editarvalores/{id}','AdministradorController@edit_valores');
Route::post('actualizarvalores','AdministradorController@update_valores');

Route::get('eliminar_valores/{id}','AdministradorController@destroy_valores');

/*--------------------------------------ADMINISTRADOR estructura organizativa--------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('estructuras-organizativas','AdministradorController@mostrar_estructura');
Route::get('estructuras-organizativas','AdministradorController@estructura_organizativa');
Route::get('vista_estructura','AdministradorController@ver_estructura');

Route::get('crear-estructura','AdministradorController@create_estructura');
Route::post('registrar-estructura','AdministradorController@store_estructura');

Route::get('editarestructura/{id}','AdministradorController@edit_estructura');
Route::post('actualizarestructura','AdministradorController@update_estructura');

Route::get('eliminar_estructura/{id}','AdministradorController@destroy_estructura');


/*--------------------------------------ADMINISTRADOR JUNTA DIRECTIVA----------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('junta-directiva','OrganizacionController@junta_directiva');
Route::get('vista_junta_directiva','AdministradorController@ver_junta');

Route::get('crear-junta','AdministradorController@create_junta');
Route::post('registrarjunta','AdministradorController@store_junta');

Route::get('editarjunta/{id}','OrganizacionController@edit_directiva');
Route::post('actualizarjunta','OrganizacionController@update_directiva');

Route::get('eliminar_junta/{id}','OrganizacionController@destroy_junta');
/*--------------------------------------ADMINISTRADOR EVENTOS cumpleaños-------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('ver-cumpleanos','AdministradorController@ver_cumpleanos');

Route::get('crearcumpleanos','AdministradorController@create_cumpleanos');
Route::post('registrarcumpleanos','AdministradorController@store_cumpleanos');

Route::get('editarcumplemes/{id}','AdministradorController@edit_cumplemes');
Route::post('actualizarcumplemes','AdministradorController@update_cumpleanos');

Route::get('ver-detalle-cumplemes/{id}','AdministradorController@detalle_cumplemes');

/*--------------------------------------ADMINISTRADOR EVENTOS nuevo ingreso----------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('ver_nuevoingresos','AdministradorController@ver_nuevoingresos');

Route::get('crear-nuevoingreso','AdministradorController@create_nuevoingreso');
Route::post('registrar-nuevoingreso','AdministradorController@store_nuevoingreso');

Route::get('editarnuevoingreso/{id}','AdministradorController@edit_nuevoingreso');
Route::post('actualizarnuevoingreso','AdministradorController@update_nuevoingreso');

Route::get('ver-detalle-nuevoingreso/{id}','AdministradorController@detalle_nuevoingreso');

/*--------------------------------------ADMINISTRADOR EVENTOS Calendario-------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('ver_calendario','AdministradorController@ver_calendario');
Route::get('/eventos/get','AdministradorController@get_evento');
Route::post('registraevento','AdministradorController@createventos');

Route::get('ver-calendario','AdministradorController@front_calendario');
Route::post('actualizaEventos','AdministradorController@update_calendario');

/*--------------------------------------ADMINISTRADOR DOCUMENTOS DESCARGABLES--------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('formulario-guarderia','AdministradorController@formulario_guarderia');
Route::get('formulario-uniformes','AdministradorController@formulario_uniformes');
Route::get('formulario-prestaciones','AdministradorController@formulario_prestaciones');
Route::get('formulario-hcm','AdministradorController@formulario_hcm');
Route::get('formulario-vacaciones','AdministradorController@formulario_vacaciones');
Route::get('formulario-recorrido-hab-trabajador','AdministradorController@formulario_recorrido_trabajador');
Route::get('ver_documentos_rrhh','AdministradorController@ver_documentos_rrhh');
Route::post('formulario','AdministradorController@store_formulario');
Route::post('registrouniformes','AdministradorController@store_formulario_uniformes');
Route::post('registroprestaciones','AdministradorController@store_formulario_prestaciones');
Route::post('registrohcm','AdministradorController@store_formulario_hcm');
Route::post('registrovacaciones','AdministradorController@store_formulario_vacaciones');
Route::post('registrotrabajador','AdministradorController@store_formulario_trabajador');
Route::get('servicios','AdministradorController@mostrar_file');

Route::get('edit-form-guarderia/{id}','AdministradorController@edit_formu_guarderia');
Route::post('actualizarformguarderia','AdministradorController@update_form_guarderia');

Route::get('edit-form-uniformes/{id}','AdministradorController@edit_formu_uniforemes');
Route::post('actualizarformuniformes','AdministradorController@update_form_uniformes');

Route::get('edit-form-prestaciones/{id}','AdministradorController@edit_formu_prestaciones');
Route::post('actualizarprestaciones','AdministradorController@update_form_prestaciones');

Route::get('edit-form-hcm/{id}','AdministradorController@edit_formu_hcm');
Route::post('actualizarhcm','AdministradorController@update_form_hcm');

Route::get('edit-formu-vacaciones/{id}','AdministradorController@edit_formu_vacaciones');
Route::post('actualizarvacaciones','AdministradorController@update_form_vacaciones');

Route::get('edit-form-recorrido-trabajador/{id}','AdministradorController@edit_formu_trabajador');
Route::post('actualizartrabajador','AdministradorController@update_form_trabajador');

Route::get('lista-guarderia','AdministradorController@lista_guarderia');
Route::get('lista-uniformes-escolares','AdministradorController@lista_uniformes_escolares');
Route::get('lista-prestaciones-sociales','AdministradorController@lista_prestaciones_sociales');
Route::get('lista-hcm','AdministradorController@lista_hcm');
Route::get('lista-vacaciones','AdministradorController@lista_vacaciones');
Route::get('lista-recorrido-trabajador','AdministradorController@lista_recorrido_trabajador');



Route::get('formulario-material-sede-principal','AdministradorController@form_sede_principal');
Route::get('formulario-mantenimiento','AdministradorController@form_mantenimiento');


Route::get('lista-material-sede-principal','AdministradorController@lista_sede_principal');
Route::get('lista-mantenimiento','AdministradorController@lista_mantenimiento');
Route::post('registrosedeprincipal','AdministradorController@store_formulario_sede_principal');
Route::post('registrosmantenimiento','AdministradorController@store_formulario_mantenimiento');

Route::get('edit-form-mantenimiento/{id}','AdministradorController@edit_formu_mantenimiento');
Route::post('actualizarmantenimiento','AdministradorController@update_form_mantenimiento');

Route::get('edit-form-material-agencia/{id}','AdministradorController@edit_formu_material_agencia');
Route::post('actualizarmaterialagencia','AdministradorController@update_form_meterial_agencia');
/*--------------------------------------WORKFLOW RECURSOS HUMANOS--------------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('workflow-crediamigo','RecursoshumanosController@index');
Route::get('reporte-crediamigo/{id}', 'RecursoshumanosController@reporte_crediamigo')->name('contrato-crediamigo.pdf');
Route::get('reporte-crediamigo','RecursoshumanosController@view_contrato_crediamigo');

/*--------------------------------------WORKFLOW uniformes escolares-----------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('workflow-uniformes-escolares','RecursoshumanosController@uniformes_escolares');


/*--------------------------------------VIATICOS-------------------------------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('workflow-viaticos','WfadminController@viaticos');

Route::post('registrarviatico','WfadminController@store_workflow_viaticos');

Route::get('servicios-generales','WfadminController@viaticos');

/*----------------------------------------emails-------------------------------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('email_transporte','WfadminController@email_transporte');


/*----------------------------------------USUARIOS-----------------------------------------------------------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('lista_usuarios','UsuariosController@lista_usuarios');
Route::get('ficha_empleado','UsuariosController@ficha_empleado');

Route::post('registrar_ficha','UsuariosController@store_ficha_empleado');


/*----------------------------------------UNIDAD ADMINISTRATIVA y red de agencia------------------- ---------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/

Route::get('ver_listados_unidad','AdministradorController@unidades_administrativas');	
Route::get('listado-red-agencias','AdministradorController@listado_red_agencia');
Route::get('listado-almacenes','AdministradorController@listado_almacenes_agencias');

Route::get('crear-oficinas','AdministradorController@create_red_oficinas');
Route::post('registrar-oficinas','AdministradorController@store_red_oficinas');

Route::get('edit-red-agencias/{id}','AdministradorController@edit_red_agencia');
Route::post('actualizaragencia','AdministradorController@update_red_agencia');

Route::get('eliminar_agencia/{id}','AdministradorController@eliminar_agencia');


Route::get('crear-almacenes','AdministradorController@create_almacen');
Route::post('registrar-almacen','AdministradorController@store_almacenes');

Route::get('edit-almacen/{id}','AdministradorController@edit_almacenes');
Route::post('actualizaralmacen','AdministradorController@update_almacen');

Route::get('eliminar_almacen/{id}','AdministradorController@eliminar_almacen');


Route::get('listado_unidad_administrativa','AdministradorController@listado_unidad_administrativa');

Route::get('crear-unidad-administrativa','AdministradorController@create_unidad_admin');
Route::post('registrarunidad','AdministradorController@store_unidad_admin');

Route::get('editar_unidad_admin/{id}','AdministradorController@edit_unidad_admin');
Route::post('actualizaralmacen','AdministradorController@update_unidad_admin');

Route::get('eliminar_unidad_admin/{id}','AdministradorController@eliminar_unidad_admin');

/*---------------------------------------MATERIAL DE PROVEEDURIA---------------------------------- ---------------------*/
/*                                                                                                                       */
/*-----------------------------------------------------------------------------------------------------------------------*/
Route::get('listado-material-proveeduria','AdministradorController@listado_material_proveeduria');


Route::get('categoria_materiales','AdministradorController@categoria_materiales');
Route::post('registrarcategoriameteriales','AdministradorController@store_cetegoria_materiales');


Route::get('edit-categoria-proveeduria/{id}','AdministradorController@edit_categoria_proveeduria');
Route::post('actualizarmateriales','AdministradorController@update_categoria_materiales');

Route::get('eliminar_categoria_proveeduria/{id}','AdministradorController@eliminar_categoria_proveeduria');

Route::get('crear_materiales','AdministradorController@create_materiales');
Route::post('registrarmateriales','AdministradorController@store_materiales_proveeduria');


Route::get('edit-materiales/{id}','AdministradorController@edit_materiales');
Route::post('actualizarproductosm','AdministradorController@update_materiales');

Route::get('eliminar_productosm/{id}','AdministradorController@eliminar_materiales');

