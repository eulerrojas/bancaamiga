<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mision extends Model {

    protected $table = 'misions';
    protected $fillable = [
        'titulo_mision','contenido_mision','image_mision',
    ];


	 public function scopeBuscador($query, $titulo_mision){

     return $query->where('titulo_mision', 'LIKE', "%$titulo_mision%");
        
    }

}
