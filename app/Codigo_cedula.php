<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Codigo_cedula extends Model {

    protected $table = 'codigo_cedulas';
    protected $fillable = [
        'nombre_valor',
    ];

}
