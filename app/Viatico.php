<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Viatico extends Model {

    protected $table = 'viaticos';
    protected $fillable = [
       'user_id','estado_id',
       'gastos_id',
       'gastos_id1',
       'gastos_id2',
       'tipo_servicio',
       'n_cuentabancaria',
       'fecha_salida',
       'hora_salida',
       'fecha_regreso',
       'hora_regreso',
       'n_dias',
       'motivo_viaje',
       'cant','costo_u',
       'monto_1',
       'cant2',
       'costo_u2',
       'monto_2',
       'cant3',
       'costo_u3',
       'monto_3',
       'subtotal1',
       'cant_gasto',
       'costo_gasto',
       'monto_gasto',
       'cant_gasto2',
       'costo_gasto2',
       'monto_gasto2',
       'cant_gasto3',
       'costo_gasto3',
       'monto_gasto3',
       'subtotal_gasto',
       'total_general',
       'observacion_viaticos',
       'supervisor_id',
       'estatus',
    ];
 public function scopeBuscador($query, $estatus){

     return $query->where('estatus', 'LIKE', "%$estatus%");
        
    }

}
