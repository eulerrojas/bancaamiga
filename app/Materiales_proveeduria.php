<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Materiales_proveeduria extends Model {

    protected $table = 'materiales_proveedurias';
    protected $fillable = [
        'cod_material','nom_material','id_categoria_material',
    ];


	 public function scopeBuscador($query, $cod_material){

     return $query->where('cod_material', 'LIKE', "%$cod_material%");
        
    }

}