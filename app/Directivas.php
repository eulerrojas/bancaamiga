<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Directivas extends Model {

    protected $table = 'directivas';
    protected $fillable = [
       'nombres_directiva','apellidos_directiva','profesion','foto_directiva','cargo_directiva','ubicacion_directva','resumen_curricular',
    ];


	 public function scopeBuscador($query, $nombres_directiva){

     return $query->where('nombres_directiva', 'LIKE', "%$nombres_directiva%");
        
    }

}
