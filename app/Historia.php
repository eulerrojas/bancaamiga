<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Historia extends Model {

    protected $table = 'historias';
    protected $fillable = [
        'titulo_principal','contenido',
    ];


	 public function scopeBuscador($query, $titulo_principal){

     return $query->where('titulo_principal', 'LIKE', "%$titulo_principal%");
        
    }

}
