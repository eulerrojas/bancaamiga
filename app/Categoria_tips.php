<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria_tips extends Model {

    protected $table = 'categoria_tips';
    protected $fillable = [
        'nombre_ctips','descripcion_ctips',
    ];


	 public function scopeBuscador($query, $nombre_ctips){

     return $query->where('nombre_ctips', 'LIKE', "%$nombre_ctips%");
        
    }

}
