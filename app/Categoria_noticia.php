<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria_noticia extends Model {

    protected $table = 'categoria_noticias';
    protected $fillable = [
        'nombre','descripcion',
    ];


	 public function scopeBuscador($query, $nombre){

     return $query->where('nombre', 'LIKE', "%$nombre%");
        
    }

}
