<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tips extends Model {

    protected $table = 'tips_bancarios';
    protected $fillable = [
        'titulo_tips', 'extracto_tips','image_tips','contenido_tips','id_catips',
    ];


	 public function scopeBuscador($query, $titulo_tips){

     return $query->where('titulo_tips', 'LIKE', "%$titulo_tips%");
        
    }

}
