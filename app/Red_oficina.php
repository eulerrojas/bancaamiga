<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Red_oficina extends Model {

    protected $table = 'red_oficinas';
    protected $fillable = [
        'cod_oficina','nom_oficina',
    ];

 public function scopeBuscador($query, $cod_oficina){

     return $query->where('cod_oficina', 'LIKE', "%$cod_oficina%");
        
    }

    public function almacenes()
	{
		 return $this->hasMany('almacenes');
	}

}