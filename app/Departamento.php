<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Departamento extends Model {

    protected $table = 'departamentos';
    protected $fillable = [
       'descripcion','cod_departamento'
    ];


	 public function scopeBuscador($query, $descripcion){

     return $query->where('descripcion', 'LIKE', "%$descripcion%");
        
    }
}
