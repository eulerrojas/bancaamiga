<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\AccionesFormRequest;
use App\Directivas;
class OrganizacionController extends Controller
{
  
  public function historia(){
    return view('organizacion.historia');
  }
  
  public function junta_directiva(Request $request){
  	 $junta = Directivas::Buscador($request->nombres_directiva)
          ->select('directivas.*')
          ->select('nombres_directiva','apellidos_directiva','profesion','foto_directiva','cargo_directiva','ubicacion_directva','resumen_curricular')->get();

      return view('organizacion.junta-directiva')->with('junta',$junta);
    }

    public function edit_directiva($id){
      $junta = Directivas::find($id);
      return view('organizacion.editar-junta')->with('junta',$junta);
       
    }
    public function update_directiva(Request $request){
      
       $junta = Directivas::find($request->id);
       
     if ($request->hasFile('foto_directiva')){
            $archivoimage=$request->file('foto_directiva');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/junta/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $junta->foto_directiva=$nombreimage; 

          }

    $junta->nombres_directiva = $request->nombres_directiva;
    $junta->apellidos_directiva = $request->apellidos_directiva;
    $junta->profesion = $request->profesion;
    $junta->cargo_directiva = $request->cargo_directiva;
    $junta->ubicacion_directva = $request->ubicacion_directva;
    $junta->resumen_curricular = $request->resumen_curricular;
    $junta->save();
       return redirect('vista_junta_directiva')->with('mjs2','La Información del Personal de Bancamiga a sido actualizada');   
    }

 public function destroy_junta($id){

      $junta = Directivas::find($id);
      $junta->delete();
  
     return  redirect('/vista_junta_directiva')->with('mjs1','La Información del Personal de Bancamiga fue Eliminada!');
   }
  public function estructura_organizativa(){
    return view('organizacion.estructura-organizativa');
  }
}