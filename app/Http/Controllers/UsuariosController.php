<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transporte;
use App\Persona;


class UsuariosController extends Controller
{

	public function lista_usuarios(Request $request){
		$user = User::Buscador($request->name)
					->select('users.*')
					->paginate(20);	
		return view('usuarios.lista_usuarios',compact('user'));
	}
	public function ficha_empleado(Request $request){
		
		return view('usuarios.ficha_empleado');
	}
	public function store_ficha_empleado(Request $request){
		  


	$persona = new Persona;


  $persona->user_id=$request->get('user_id');
  $persona->nombres=$request->get('nombres');
  $persona->apellidos=$request->get('apellidos');
  $persona->cod_cedula = $request->get('cod_cedula');
  $persona->cedula = $request->get('cedula');
  $persona->telefono_hab=$request->get('telefono_hab');
  $persona->celular=$request->get('celular');
  $persona->id_departamento=$request->get('id_departamento');
  $persona->id_rol=$request->get('id_rol');

  $persona->save(); 


		return redirect('lista_usuarios')->with('msj','La Ficha del Usuario a sido creada');;
	}
}