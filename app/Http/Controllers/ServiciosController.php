<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccionesFormRequest;

class ServiciosController extends Controller
{
  public function index(){
  	 $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.file_doc')
                        ->get();
    return view('servicio-empleados.servicios-generales')->with('data',$data);     
  }
 
}