<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\NoticiasRequest;
use App\Http\Requests\CategoriaNoticiaRequest;
use App\Http\Requests\SolicitudesRequest;
use App\Categoria_noticia;
use App\Categoria_tips;
use App\Noticia;
use App\Solicitud;
use App\Tips;
use App\Historia;
use App\Slider;
use App\Mision;
use App\Vision;
use App\Valores;
use App\Estructura_organizativa;
use App\Cumpleanos;
use App\Nuevo_ingreso;
use App\Evento;
use App\Directivas;
use App\Departamento;
use App\Documento;
use App\Red_oficina;
use App\Almacenes;
use App\Categoria_materiales;
use App\Materiales_proveeduria;
use Image;
use Mail;
use Storage;

class AdministradorController extends Controller
{
  /*-----------------------------------------------------------NOTICIAS----------------------------------------------- */
  public function index(Request $request){

  $noticia = Noticia::Buscador($request->titulo)
    	->select('noticias.*')
    	->select('id','titulo','subtitulo','image','created_at')
      ->OrderBy('id')
    	->paginate(7);
    return view('noticias.ba-admin')->with('noticia',$noticia);
  }
  public function create_new(){
    return view('noticias.crear_noticia');
  }
  public function store_new(){
    return redirect('ba-admin');
  }

  public function create_noticia(){
  
  $thumbnail = Noticia::Buscador($request->titulo)
        ->select('Categoria_noticia.* as c')
        ->select('titulo','subtitulo','id','image','created_at')
        ->paginate(5);

  	return view('noticias.ba-admin')->with('thumbnail',$thumbnail);

  }

  public function store_noticia(NoticiasRequest $request){

  	  $file = Input::file('image');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('noticia/'.$nombre); 
      $url = '/noticia/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(1920, 980);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $thumbnail = new Noticia();
   $thumbnail->titulo = Input::get('titulo');
   $thumbnail->subtitulo = Input::get('subtitulo');
   $thumbnail->contenido = Input::get('contenido');
   $thumbnail->extracto = Input::get('extracto');
   $thumbnail->id_catenot = Input::get('id_catenot');
   $thumbnail->image = $file->getClientOriginalName();
   $thumbnail->save();

    return redirect('/ba-admin')->with('msj','La Noticia a sido creada!');

  }

    public function destroy_noticia($id){

      $cat = Noticia::find($id);
      $cat->delete();
  
     return  redirect('/ba-admin')->with('mjs1','La Noticia fue Eliminada!');
   }
    public function edit_noticia($id){
       $noticia = Noticia::find($id);
       return view('noticias.editar_noticia')->with('noticia',$noticia);
       
    }
   
    public function update_noticia(NoticiasRequest $request){
         
    $noticia = Noticia::find($request->id);
       
     if ($request->hasFile('image')){
            $archivoimage=$request->file('image');
            $nombreimage=time().$archivoimage->getClientOriginalName();
            $archivoimage->move(public_path().'/noticia/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $noticia->image=$nombreimage; 

          }
       
    $noticia->titulo = $request->titulo;
    $noticia->subtitulo = $request->subtitulo;
    $noticia->extracto = $request->extracto;
    $noticia->contenido = $request->contenido;
    $noticia->id_catenot = $request->id_catenot;

  	$noticia->save();
       return redirect('ba-admin')->with('mjs2','La Noticia a sido actualizada');   
    }  

  /*------------------------------------------------CATEGORIA NOTICIAS------------------------------------------------ */ 
  public function view_categoria(){
    
    return view('noticias.categoria-noticia');
  }
	public function create_categoria(){
   
    return view('noticias.categoria-noticia');
  }
  public function store_categoria(CategoriaNoticiaRequest $request){

  $cat = new Categoria_noticia;
  
  $cat->nombre=$request->get('nombre');
  $cat->descripcion=$request->get('descripcion');
  $cat->save(); 

  return redirect('/categoria-noticia')->with('msj','La Categoría a sido creada!');
}
public function tabla_categoria(Request $request){

  $cat = Categoria_noticia::Buscador($request->nombre)
        ->select('Categoria_noticia.* as c')
        ->select('nombre','descripcion','id')
         ->OrderBy('id')
        ->paginate(7);
        
        return view('noticias.categoria-noticia')->with('cat',$cat);
     
}
  public function destroy($id){

      $cat = Categoria_noticia::find($id);
      $cat->delete();
  
     return  redirect('/categoria-noticia')->with('mjs1','La Categoría fue Eliminada!');
   }      

   public function ver_categoria_edit(){
   		 $cat = Categoria_noticia::Buscador($request->nombre)
        ->select('categoria-noticia.* as c')
        ->select('nombre','descripcion','id')
        ->paginate(5);
   		return view('noticias.editar_categoria')->with('cat',$cat);
   }
   public function edit($id){
       $cat = Categoria_noticia::find($id);
       return view('noticias.editar_categoria')->with('cat',$cat);
       
    }
    public function update(CategoriaNoticiaRequest $request){
        
    $cat = Categoria_noticia::find($request->id);
      
    $cat->nombre = $request->nombre;
    $cat->descripcion = $request->descripcion;
  	$cat->save();
       return redirect('categoria-noticia')->with('mjs2','La Categoría a sido actualizada');   
    }

  /*-----------------------------------MOSTRAR MODULO EN HOME---------------------------------------------- */

  	public function mostrar_noticias(Request $request){

  		$home = Noticia::select('noticias.*')
  				->join('categoria_noticias as c', 'id_catenot','=','c.id')
  				->select('image','titulo','subtitulo','noticias.id','c.nombre')->get();
		
		$home1 = Solicitud::select('solicitudes.*')
  				->select('image_solicitud','titulo_solicitud','link')->get();
  		
  		$home2 = Tips::select('tips_bancarios')
  				->join('categoria_tips as ca','id_catips','=','ca.id')
  				->select('image_tips','titulo_tips','extracto_tips','ca.nombre_ctips','tips_bancarios.created_at','tips_bancarios.id')->get();	
     $home3 = Cumpleanos::select('cumpleanos.*')
              ->select('titulo_eventoc','image_eventoc','cumpleanos.id')->first();

      $home4 = Nuevo_ingreso::select('nuevos_ingresos.*')
              ->select('titulo_eventoi','image_eventoi','nuevos_ingresos.id')->first();         

     $home5 = Evento::paginate(6);        

  		return view ('home',compact('home','home1','home2','home3','home4','home5'));
  	}

   /*-----------------------------------BLOG --------------------------------------------------------------- */ 	

    public function blog(){
    	return view('noticias.blog');
    }

    public function consulta_noticia(Request $request){

    	$blog = Noticia::Buscador($request->titulo)
    			->select('noticias.*')
    			->join('categoria_noticias as c','id_catenot','=','c.id')
    			->select('image','titulo','noticias.created_at','c.nombre','extracto','noticias.id')
    			->paginate(5);
    	return view('noticias.blog')->with('blog',$blog);
    }

    public function categoria_institucional(Request $request){
     
      $blog_categoria = Noticia::Buscador($request->titulo)
          ->select('noticias.*')
          ->join('categoria_noticias as c','id_catenot','=','c.id')
          ->select('image','titulo','noticias.created_at','c.nombre','extracto','noticias.id')
          ->where('c.nombre','=','Institucional')
          ->paginate(5);
          
      return view('noticias.categoria-institucional')->with('blog_categoria',$blog_categoria);
    }

     public function categoria_administracion(Request $request){
       $blog_categoria = Noticia::Buscador($request->titulo)
          ->select('noticias.*')
          ->join('categoria_noticias as c','id_catenot','=','c.id')
          ->select('image','titulo','noticias.created_at','c.nombre','extracto','noticias.id')
          ->where('c.nombre','=','Administración')
          ->paginate(5);
     
      return view('noticias.categoria-administracion')->with('blog_categoria',$blog_categoria);
    }
     public function categoria_rrhh(Request $request){
       $blog_categoria = Noticia::Buscador($request->titulo)
          ->select('noticias.*')
          ->join('categoria_noticias as c','id_catenot','=','c.id')
          ->select('image','titulo','noticias.created_at','c.nombre','extracto','noticias.id')
          ->where('c.nombre','=','Gestión Humana')
          ->paginate(5);
    
      return view('noticias.categoria-rrhh')->with('blog_categoria',$blog_categoria);
    }

    public function noticia_individual($id_catenot){

       $blog = Noticia::find($id_catenot);
      
        if(count($blog) > 0){
           $valor= Categoria_noticia::select('categoria_noticias.*')
          ->join('noticias as n','categoria_noticias.id','=','n.id_catenot')
          ->select('n.image','titulo','n.created_at','categoria_noticias.nombre','contenido')
          ->where('id_catenot',$blog->id_catenot)
          ->first();
  
  }

      return view('noticias.blog_noticias',compact('valor','blog'));
    }

    /*-----------------------------------FIN BLOG --------------------------------------------------------------- */ 

    /*-----------------------------------SOLICITUD BANCARIA ------------------------------------------------------ */ 

    public function solicitudes(Request $request){
    	$solicitud = Solicitud::Buscador($request->titulo_solicitud)
    			->select('solicitudes.*')
    			->select('id','image_solicitud','titulo_solicitud','link','created_at')
          ->OrderBy('id')
    			->paginate(7);
    	
    	return view('solicitudes.solicitudes')->with('solicitud',$solicitud);
    }

    public function nueva_solicitud(){

    	return view('solicitudes.nueva-solicitud');
    }

    public function create_solicitud(){

    	return view('solicitudes.nueva-solicitud');
    }

    public function store_solicitud(SolicitudesRequest $request){
 
 	  $file = Input::file('image_solicitud');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('solicitud/'.$nombre); 
      $url = '/solicitud/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(225, 225);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $thumbnail = new Solicitud();
   $thumbnail->titulo_solicitud = Input::get('titulo_solicitud');
   $thumbnail->link = Input::get('link');
   $thumbnail->image_solicitud = $file->getClientOriginalName();
   $thumbnail->save();

  	return redirect('solicitudes')->with('msj','La Solicitud a sido creada!');


    }

     public function destroy_solicitud($id){

      $solicitud = Solicitud::find($id);
      $solicitud->delete();
  
     return  redirect('solicitudes')->with('mjs1','La Solicitud fue Eliminada!');
   } 

    public function edit_solicitud($id){
       $solicitud = Solicitud::find($id);
       return view('solicitudes.editar-solicitud')->with('solicitud',$solicitud);
       
    }
    public function update_solicitud(SolicitudesRequest $request){
        

    	 $solicitud = Solicitud::find($request->id);
       
     if ($request->hasFile('image_solicitud')){
            $archivoimage=$request->file('image_solicitud');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/solicitud/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $solicitud->image_solicitud=$nombreimage; 

          }
	  	$solicitud->titulo_solicitud = $request->titulo_solicitud;
    	$solicitud->link = $request->link;
  		$solicitud->save();
       return redirect('solicitudes')->with('mjs2','La Solicitud a sido actualizada');   
    }
   /*-----------------------------------TIPS BANCARIOS--------------------------------------------------------------- */ 
    /*----------------------------------- --------------------------------------------------------------------------- */

    public function vertips(Request $request){
    	$tips = tips::Buscador($request->titulo_tips)
        ->select('tips_bancarios.* as t')
        ->select('id','titulo_tips','image_tips','extracto_tips','created_at')
        ->paginate(7);
       
    	return view('tips.ver-tips')->with('tips',$tips);
    }
    public function nuevo_tips(){

    	return view('tips.nuevo_tips');
    }
  public function categoria_tips(){

    	return view('tips.categoria_tips');
    }
  
  public function create_tips(){

  	return view('tips.nuevo_tips');
  }
  public function store_tips(Request $request){

  	 $file = Input::file('image_tips');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('tips/'.$nombre); 
      $url = '/tips/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(720, 440);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $tips = new Tips();
   $tips->titulo_tips = Input::get('titulo_tips');
   $tips->extracto_tips = Input::get('extracto_tips');
   $tips->contenido_tips = Input::get('contenido_tips');
   $tips->id_catips = Input::get('id_catips');
   $tips->image_tips = $file->getClientOriginalName();
   $tips->save();

  	return redirect('ver-tips')->with('msj','El tips de Seguridad a sido creada!');
  }

    public function edit_tips($id){
      $tips1 = Tips::find($id);
      return view('tips.editar_tips')->with('tips1',$tips1);
       
    }

    public function update_tips_bancario(Request $request){
         
    	 $tips = Tips::find($request->id);
       
     if ($request->hasFile('image_tips')){
            $archivoimage=$request->file('image_tips');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/tips/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $tips->image_tips=$nombreimage; 

          }

    $tips->titulo_tips = $request->titulo_tips;
    $tips->extracto_tips = $request->extracto_tips;
    $tips->contenido_tips = $request->contenido_tips;
    $tips->id_catips = $request->id_catips;
  	$tips->save();

       return redirect('ver-tips')->with('mjs2','El tips de Seguridad a sido actualizada');   
    }
      public function destroy_tipsbancario($id){

      $tips = Tips::find($id);
      $tips->delete();
  
     return  redirect('/ver-tips')->with('mjs1','La Categoría fue Eliminada!');
   }

public function create_tips_categoria(){

  	return view('tips.categoria_tips');
  }

public function store_tips_categoria(Request $request){

  $tips = new Categoria_tips;
  
  $tips->nombre_ctips=$request->get('nombre_ctips');
  $tips->descripcion_ctips=$request->get('descripcion_ctips');
  $tips->save(); 

  return redirect('/categoria_tips')->with('msj','La Categoría a sido creada!');

}
public function mostrar_tips(Request $request){
		 $tips = Categoria_tips::Buscador($request->nombre_ctips)
        ->select('Categoria_tips.* as c')
        ->select('nombre_ctips','descripcion_ctips','id')
        ->paginate(7);

        return view('tips.categoria_tips')->with('tips',$tips);
     
}
  public function destroy_tips($id){

      $tips = Categoria_tips::find($id);
      $tips->delete();
  
     return  redirect('/categoria_tips')->with('mjs1','La Categoría fue Eliminada!');
   }

     public function edit_ctips($id){
       $tips = Categoria_tips::find($id);
       return view('tips.editar_categoriatips')->with('tips',$tips);
       
    }
    public function update_ctips(Request $request){
        
    $tips = Categoria_tips::find($request->id);
      
    $tips->nombre_ctips = $request->nombre_ctips;
    $tips->descripcion_ctips = $request->descripcion_ctips;
  	$tips->save();
       return redirect('categoria_tips')->with('mjs2','La Categoría a sido actualizada');   
    }  

    public function ver_mas($id_catips){
/* la locura  ARREBLAR OJO*/
      $tips = Tips::find($id_catips);

      $valor_t = Categoria_tips::select('categoria_tips.*')
        ->join('tips_bancarios as t','categoria_tips.id','=','t.id_catips')
        ->select('titulo_tips','image_tips','t.contenido_tips','nombre_ctips','t.created_at')
        ->where('id_catips',$tips->id_catips)
        ->first();

      return view('tips.blog_tips',compact('tips','valor_t'));

    }

 /*-----------------------------------MODULO 2 HISTORIA--------------------------------------------------------------- */ 
    /*----------------------------------- ---------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN HISTORIA----------------------------------------------------- */

  	public function mostrar_historia(Request $request){

  		$hist = Historia::Buscador($request->titulo_principal)
  				->select('historias.*')
  				->select('titulo_principal','contenido')->first();

  		$slider = Slider::select('sliders.*')
  				->select('image_slider')->get();
		

		$mision = Mision::select('misions.*')
  				->select('image_mision','titulo_mision','contenido_mision')->first();

  		$vision = Vision::select('visions.*')
  				->select('image_vision','titulo_vision','contenido_vision')->first();

  		$valores = Valores::select('valores.*')
  				->select('image_valores','titulo_valores','contenido_valores')->first();					
  		return view ('organizacion.historia')->with('hist',$hist)->with('slider',$slider)->with('mision',$mision)->with('vision',$vision)->with('valores',$valores);
  	}

 /*-------------------------------------------------------------------------------------------------------------------- */

    public function redactar_historia(){
    
    	return view('organizacion.redactar_historia');
    }
	
    public function vista_historia(Request $request){
    	$hist = Historia::select('historias.*')
    	->select('id','titulo_principal','contenido','created_at')
    	->get();
    return view('organizacion.vista_historia')->with('hist',$hist);
    }

    public function create_historia(){

    	return view('organizacion.redactar_historia');
    }

    public function store_historia(Request $request){

   		$hist = new Historia;
  
 	    $hist->titulo_principal=$request->get('titulo_principal');
  		$hist->contenido=$request->get('contenido');
  		$hist->save(); 

    return redirect('vista_historia')->with('msj','El contenido de esta sección a sido creada!');	
    }

     public function edit_historia($id){
       $hist = Historia::find($id);
       return view('organizacion.editar_historia')->with('hist',$hist);
       
    }
    public function update_historia(Request $request){
        
    $hist = Historia::find($request->id);
      
    $hist->titulo_principal = $request->titulo_principal;
    $hist->contenido = $request->contenido;
  	$hist->save();
       return redirect('vista_historia')->with('mjs2','El contenido de esta sección a sido actualizado');   
    }

    public function slider(Request $request){
    	$slider = Slider::Buscador($request->titulo_slider)
  				->select('sliders.*')
  				->select('id','titulo_slider','image_slider','created_at')->get();

    	return view('organizacion.sliders')->with('slider',$slider);
    }  

     public function create_slider(){

    	return view('organizacion.crear-slider');
    } 

     public function store_slider(Request $request){

  	 $file = Input::file('image_slider');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('slider/'.$nombre); 
      $url = '/slider/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(540, 365);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $slider = new Slider();
   $slider->titulo_slider = Input::get('titulo_slider');
   $slider->image_slider = $file->getClientOriginalName();
   $slider->save();

  	return redirect('sliders')->with('msj','La Imagen a sido creada!');
  }

    public function edit_slider($id){
      $slider = Slider::find($id);
      return view('organizacion.editar_slider')->with('slider',$slider);
       
    }
    public function update_slider(Request $request){
      
    	 $slider = Slider::find($request->id);
       
     if ($request->hasFile('image_slider')){
            $archivoimage=$request->file('image_slider');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/slider/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $slider->image_slider=$nombreimage; 

          }

    $slider->titulo_slider = $request->titulo_slider;
  	$slider->save();
       return redirect('sliders')->with('mjs2','La Imagen a sido actualizada');   
    }
      public function destroy_slider($id){

      $slider = Slider::find($id);
      $slider->delete();
  
     return  redirect('/sliders')->with('mjs1','La Imagen fue Eliminada!');
   }

      public function mision(Request $request){
      $mision = Mision::Buscador($request->titulo_mision)
  				->select('misions.*')
  				->select('id','titulo_mision','contenido_mision','image_mision','created_at')->get();
	
    	return view('organizacion.mision')->with('mision',$mision);
    } 

public function create_mision(){

  	return view('organizacion.crear-mision');
  }

public function store_mision(Request $request){

  $file = Input::file('image_mision');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('agregados/'.$nombre); 
      $url = '/agregados/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(70, 60);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $mision = new Mision();
   $mision->titulo_mision = Input::get('titulo_mision');
   $mision->contenido_mision = Input::get('contenido_mision');
   $mision->image_mision = $file->getClientOriginalName();
   $mision->save();

  return redirect('/mision')->with('msj','el contenido de MISIÓN a sido creada!');

}

    public function edit_mision($id){
      $mision = Mision::find($id);
      return view('organizacion.editar-mision')->with('mision',$mision);
       
    }
    public function update_mision(Request $request){
      
    	 $mision = Mision::find($request->id);
       
     if ($request->hasFile('image_mision')){
            $archivoimage=$request->file('image_mision');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/agregados/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $mision->image_mision=$nombreimage; 

          }

    $mision->titulo_mision = $request->titulo_mision;
    $mision->contenido_mision = $request->contenido_mision;
  	$mision->save();
       return redirect('mision')->with('mjs2','el contenido de MISIÓN a sido actualizada');   
    }
      public function destroy_mision($id){

      $mision = Mision::find($id);
      $mision->delete();
  
     return  redirect('/mision')->with('mjs1','el contenido de MISIÓN fue Eliminada!');
   }

      public function vision(Request $request){
$vision = Vision::Buscador($request->titulo_mision)
  				->select('visions.*')
  				->select('id','titulo_vision','contenido_vision','image_vision','created_at')->get();
    	return view('organizacion.vision')->with('vision',$vision);
    }  

public function create_vision(){

  	return view('organizacion.crear-vision');
  }

public function store_vision(Request $request){

  $file = Input::file('image_vision');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('agregados/'.$nombre); 
      $url = '/agregados/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(70, 60);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $vision = new Vision();
   $vision->titulo_vision = Input::get('titulo_vision');
   $vision->contenido_vision = Input::get('contenido_vision');
   $vision->image_vision = $file->getClientOriginalName();
   $vision->save();

  return redirect('/vision')->with('msj','el contenido de VISION a sido creada!');

}

 public function edit_vision($id){
      $vision = Vision::find($id);
      return view('organizacion.editar-vision')->with('vision',$vision);
       
    }
    public function update_vision(Request $request){
      
    	 $vision = Vision::find($request->id);
       
     if ($request->hasFile('image_vision')){
            $archivoimage=$request->file('image_vision');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/agregados/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $vision->image_vision=$nombreimage; 

          }

    $vision->titulo_vision = $request->titulo_vision;
    $vision->contenido_vision = $request->contenido_vision;
  	$vision->save();
       return redirect('vision')->with('mjs2','el contenido de VISIÓN a sido actualizada');   
    }
      public function destroy_vision($id){

      $vision = Vision::find($id);
      $vision->delete();
  
     return  redirect('/vision')->with('mjs1','el contenido de VISIÓN fue Eliminada!');
   }

      public function valores(Request $request){
$valores = Valores::Buscador($request->titulo_valores)
  				->select('valores.*')
  				->select('id','titulo_valores','contenido_valores','image_valores','created_at')->get();
    	return view('organizacion.valores')->with('valores',$valores);
    }  

	public function create_valores(){

  	return view('organizacion.crear-valores');
  }

public function store_valores(Request $request){

  $file = Input::file('image_valores');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('agregados/'.$nombre); 
      $url = '/agregados/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(70, 60);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $valores = new Valores();
   $valores->titulo_valores = Input::get('titulo_valores');
   $valores->contenido_valores = Input::get('contenido_valores');
   $valores->image_valores = $file->getClientOriginalName();
   $valores->save();

  return redirect('/valores')->with('msj','el contenido de VALORES a sido creada!');

}

 public function edit_valores($id){
      $valores = Valores::find($id);
      return view('organizacion.editar-valores')->with('valores',$valores);
       
    }
    public function update_valores(Request $request){
      
    	 $valores = Valores::find($request->id);
       
     if ($request->hasFile('image_valores')){
            $archivoimage=$request->file('image_valores');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/agregados/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $valores->image_valores=$nombreimage; 

          }

    $valores->titulo_valores = $request->titulo_valores;
    $valores->contenido_valores = $request->contenido_valores;
  	$valores->save();
       return redirect('valores')->with('mjs2','el contenido de VALORES a sido actualizada');   
    }
      public function destroy_valores($id){

      $valores = Valores::find($id);
      $valores->delete();
  
     return  redirect('/valores')->with('mjs1','el contenido de VALORES fue Eliminada!');
   }
 /*-----------------------------------MODULO 3 JUNTA DIRECTIVA--------------------------------------------------------------- */ 
    /*----------------------------------- ---------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN DIRECTIVA----------------------------------------------------- */


    public function junta_directiva(){

      return view('organizacion.junta-directiva');
    }

     public function ver_junta(Request $request){
 $junta = Directivas::Buscador($request->nombres_directiva)
          ->select('directivas.*')
          ->select('id','nombres_directiva','apellidos_directiva','profesion','foto_directiva','cargo_directiva')->get();
       
      return view('organizacion.vista_junta_directiva')->with('junta',$junta);
    }

    public function create_junta(){

      return view('organizacion.crear-junta');
    }

    public function store_junta(Request $request){

  $file = Input::file('foto_directiva');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('junta/'.$nombre); 
      $url = '/junta/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(80, 80);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $junta = new Directivas();
   $junta->nombres_directiva = Input::get('nombres_directiva');
   $junta->apellidos_directiva = Input::get('apellidos_directiva');
   $junta->profesion = Input::get('profesion');
   $junta->cargo_directiva = Input::get('cargo_directiva');
   $junta->ubicacion_directva = Input::get('ubicacion_directva');
   $junta->resumen_curricular = Input::get('resumen_curricular');
   $junta->foto_directiva = $file->getClientOriginalName();
   $junta->save();

  return redirect('/vista_junta_directiva')->with('msj','La Información del Personal de Bancamiga a sido creada!');

}

/*-----------------------------------MODULO 4 ESTRUCTURA ORGANIZATIVA--------------------------------------------------------------- */ 
    /*----------------------------------- ---------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN ORGANIZACIONAL----------------------------------------------------- */
    
  public function estructura_organizativa(){
  $est = Estructura_organizativa::select('historias.*')
          ->select('titulo_estructura','subtitulo_estructura','image_estructura')->first();

  return view('organizacion.estructuras-organizativas')->with('est',$est);    
  }

public function ver_estructura(Request $request){
$est = Estructura_organizativa::Buscador($request->titulo_valores)
          ->select('estructuras_organizativas.*')
          ->select('id','titulo_estructura','subtitulo_estructura','image_estructura','created_at')->get();

    return view('organizacion.vista_estructura')->with('est',$est);
  }

public function create_estructura(){

    return view('organizacion.crear-estructura');
  }

public function store_estructura(Request $request){

  $file = Input::file('image_estructura');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('estructura-organizativa/'.$nombre); 
      $url = '/estructura-organizativa/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(1000, 700);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $estructura = new Estructura_organizativa();
   $estructura->titulo_estructura = Input::get('titulo_estructura');
   $estructura->subtitulo_estructura = Input::get('subtitulo_estructura');
   $estructura->image_estructura = $file->getClientOriginalName();
   $estructura->save();

  return redirect('/vista_estructura')->with('msj','La Estructura Organizativa a sido creada!');

}

public function edit_estructura($id){
      $est = Estructura_organizativa::find($id);
      return view('organizacion.editar-estructura')->with('est',$est);
       
    }
    public function update_estructura(Request $request){
      
       $est = Estructura_organizativa::find($request->id);
       
     if ($request->hasFile('image_estructura')){
            $archivoimage=$request->file('image_estructura');
            $nombreimage=time().$archivoimage->getClientOriginalName(); 
            $archivoimage->move(public_path().'/estructura-organizativa/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $est->image_estructura=$nombreimage; 

          }

    $est->titulo_estructura = $request->titulo_estructura;
    $est->subtitulo_estructura = $request->subtitulo_estructura;
    $est->save();
       return redirect('vista_estructura')->with('mjs2','La Estructura Organizativa a sido actualizada');   
    }
      public function destroy_estructura($id){

      $est = Estructura_organizativa::find($id);
      $est->delete();
  
     return  redirect('/vista_estructura')->with('mjs1','La Estructura Organizativa fue Eliminada!');
   }

/*-----------------------------------MODULO 3 EVENTOS cumpleaños------------------------------------------------------------ */ 
    /*----------------------------------- ---------------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN ORGANIZACIONAL----------------------------------------------------- */
  
    public function ver_cumpleanos(){

      $cumple = Cumpleanos::select('cumpleanos.*')
      ->select('id','titulo_eventoc','image_eventoc','updated_at')
      ->OrderBy('id')
      ->get();
      return view('eventos.ver-cumpleanos')->with('cumple',$cumple);
    }

   public function create_cumpleanos(){

    return view('eventos.crear-cumpleanos');

  }

  public function store_cumpleanos(Request $request){

      $file = Input::file('image_eventoc');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('evento_cumpleanos/'.$nombre); 
      $url = '/evento_cumpleanos/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(300, 250);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $thumbnail = new Cumpleanos();
   $thumbnail->titulo_eventoc = Input::get('titulo_eventoc');
   $thumbnail->image_eventoc = $file->getClientOriginalName();
   $thumbnail->save();

    return redirect('/ver-cumpleanos')->with('msj','El Evento a sido creado!');

  }
  public function edit_cumplemes($id){
       $cumplemes = Cumpleanos::find($id);
       return view('eventos.editar-cumpleanos')->with('cumplemes',$cumplemes);
       
    }
   
    public function update_cumpleanos(Request $request){
         
    $cumplemes = cumpleanos::find($request->id);
       
     if ($request->hasFile('image_eventoc')){
            $archivoimage=$request->file('image_eventoc');
            $nombreimage=time().$archivoimage->getClientOriginalName();
            $archivoimage->move(public_path().'/evento_cumpleanos/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $cumplemes->image_eventoc=$nombreimage; 

          }
       
    $cumplemes->titulo_eventoc = $request->titulo_eventoc;
    $cumplemes->save();
       return redirect('ver-cumpleanos')->with('mjs2','El Evento a sido actualizado');   
    }  
  public function detalle_cumplemes($id){
    $cumplemes = Cumpleanos::find($id);
    return view('eventos.ver-detalle-cumplemes')->with('cumplemes',$cumplemes);
  }
/*-----------------------------------MODULO 3 EVENTOS Nuevo Ingreso--------------------------------------------------------- */ 
    /*----------------------------------- ---------------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN ORGANIZACIONAL----------------------------------------------------- */

  public function ver_nuevoingresos(){
      $nvo = Nuevo_ingreso::select('nuevos_ingresos.*')
      ->select('id','titulo_eventoi','image_eventoi','updated_at')
      ->OrderBy('id')
      ->first();

      return view('eventos.nuevo-ingreso')->with('nvo',$nvo);
    }

   public function create_nuevoingreso(){

    return view('eventos.crear-nuevoingreso');

  }

  public function store_nuevoingreso(Request $request){

      $file = Input::file('image_eventoi');
   
      $nombre = $file->getClientOriginalName();
      $path = public_path('evento_ingreso/'.$nombre); 
      $url = '/evento_ingreso/'.$nombre;
      $image = Image::make($file->getRealPath())->resize(300, 250);
      $image->save($path);
      
  //Guardamos nombre y nombreOriginal en la BD
   $thumbnail = new Nuevo_ingreso();
   $thumbnail->titulo_eventoi = Input::get('titulo_eventoi');
   $thumbnail->image_eventoi = $file->getClientOriginalName();
   $thumbnail->save();

    return redirect('/ver_nuevoingresos')->with('msj','El Evento a sido creado!');

  }
 public function edit_nuevoingreso($id){
       $nvo = Nuevo_ingreso::find($id);
       return view('eventos.editar-nuevoingreso')->with('nvo',$nvo);
       
    }
   
    public function update_nuevoingreso(Request $request){
         
    $nvo = Nuevo_ingreso::find($request->id);
       
     if ($request->hasFile('image_eventoi')){
            $archivoimage=$request->file('image_eventoi');
            $nombreimage=time().$archivoimage->getClientOriginalName();
            $archivoimage->move(public_path().'/evento_ingreso/', $nombreimage);

  // esta es la línea que faltaba. Llamo a la foto del modelo y le asigno la foto recogida por el formulario de actualizar.          
        $nvo->image_eventoi=$nombreimage; 

          }
       
    $nvo->titulo_eventoi = $request->titulo_eventoi;
    $nvo->save();
       return redirect('ver_nuevoingresos')->with('mjs2','El Evento a sido actualizado');   
    }  

  public function detalle_nuevoingreso($id){
      $nvo = Nuevo_ingreso::find($id);
    return view('eventos.ver-detalle-nuevoingreso')->with('nvo',$nvo);
  }
/*-----------------------------------MODULO 3 EVENTOS Calendario--------------------------------------------------------- */ 
    /*----------------------------------- ---------------------------------------------------------------------------------- */
  /*-----------------------------------MOSTRAR MODULO EN ORGANIZACIONAL----------------------------------------------------- */

  public function ver_calendario(){

    return view('eventos.calendario');
  }

  public function front_calendario(){

      return view('eventos.ver-calendario');
    }

  public function get_evento(){

    $events = Evento::select("id","titulo as title","fecha_inicio as start","fecha_final as end","color")->get()->toArray();

    return response()->json($events);


}

    public function createventos( Request $request){

        $input = $request->all();

        $input["fecha_inicio"] = $input["fecha_inicio"]. " " .date("H:m:s", strtotime($input["hora_inicio"]));
        $input["fecha_final"] = $input["fecha_final"]. " " .date("H:m:s", strtotime($input["hora_final"]));

      /*  $input["color"] = "#d5d5d5"; */
        Evento::create($input);

        return redirect('ver_calendario')->with('msje','Registro de Evento Exitoso');
    }

       public function update_calendario(){
        //Valores recibidos via ajax
        $id = $_POST['id'];
        $title = $_POST['titulo'];
        $start = $_POST['fecha_inicio'];
        $end = $_POST['fecha_final'];
        $back = $_POST['color'];
        $evento=Evento::find($id);
        if($end=='NULL'){
            $evento->fechaFin=NULL;
        }else{
            $evento->fechaFin=$end;
        }
        $evento->fechaIni=$start;
        $evento->todoeldia=$allDay;
        $evento->color=$back;
        $evento->titulo=$title;
        //$evento->fechaFin=$end;
        $evento->save();
   }
/*------------------------------------MODULO DE CONSULTAS DE FORMULARIOS-------------------------------------------------- */ 
/*------------------------------------- ---------------------------------------------------------------------------------- */
/*------------------------------------------------------------------------------------------------------------------------ */

   public function formulario_guarderia(){

      return view('documentacion-pdf.formulario-guarderia');
   }
   
   public function store_formulario(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('ver_documentos_rrhh')->with('msj','Registro Exitoso');
   }
    public function edit_formu_guarderia($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-guarderia',compact('data'));
    }

    public function update_form_guarderia(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/file/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/file',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-guarderia')->with('mjs2','Formulario Actualizado');
    }
    public function lista_guarderia(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 1)
                        ->get();
      return view('documentacion-pdf.lista-guarderia')->with('data',$data);
   }
    public function lista_uniformes_escolares(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 9)
                        ->get();
      return view('documentacion-pdf.lista-uniformes-escolares')->with('data',$data);
   }

   public function formulario_uniformes(){

      return view('documentacion-pdf.formulario-uniformes');
   }
   
   public function store_formulario_uniformes(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/uniformes',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-uniformes-escolares')->with('msj','Registro Exitoso');
   }
      public function edit_formu_uniforemes($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-uniformes',compact('data'));
    }

    public function update_form_uniformes(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/uniformes/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/uniformes',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-uniformes-escolares')->with('mjs2','Formulario Actualizado');
    }
      public function formulario_prestaciones(){

      return view('documentacion-pdf.formulario-prestaciones');
   }
      public function lista_prestaciones_sociales(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 2)
                        ->get();
      return view('documentacion-pdf.lista-prestaciones-sociales')->with('data',$data);
   }
   
   public function store_formulario_prestaciones(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/prestaciones',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-prestaciones-sociales')->with('msj','Registro Exitoso');
   }
   public function edit_formu_prestaciones($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-prestaciones',compact('data'));
    }

    public function update_form_prestaciones(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/prestaciones/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/prestaciones',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-prestaciones-sociales')->with('mjs2','Formulario Actualizado');
    }
    public function formulario_hcm(){

      return view('documentacion-pdf.formulario-hcm');
   }
     public function lista_hcm(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 3)
                        ->get();
      return view('documentacion-pdf.lista-hcm')->with('data',$data);
   }
     public function store_formulario_hcm(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/hcm',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-hcm')->with('msj','Registro Exitoso');
   }
    public function edit_formu_hcm($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-hcm',compact('data'));
    }

    public function update_form_hcm(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/hcm/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/hcm',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-hcm')->with('mjs2','Formulario Actualizado');
    }
        public function formulario_vacaciones(){

      return view('documentacion-pdf.formulario-vacaciones');
   }
     public function lista_vacaciones(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 4)
                        ->get();
      return view('documentacion-pdf.lista-vacaciones')->with('data',$data);
   }
     public function store_formulario_vacaciones(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/vacaciones',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-vacaciones')->with('msj','Registro Exitoso');
   }
    public function edit_formu_vacaciones($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-vacaciones',compact('data'));
    }

    public function update_form_vacaciones(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/vacaciones/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/vacaciones',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-vacaciones')->with('mjs2','Formulario Actualizado');
    }
        
    public function formulario_recorrido_trabajador(){

      return view('documentacion-pdf.formulario-recorrido-hab-trabajador');
   }

     public function lista_recorrido_trabajador(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 5)
                        ->get();
      return view('documentacion-pdf.lista-recorrido-trabajador')->with('data',$data);
   }
   public function store_formulario_trabajador(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/recorrido-trabajador',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-recorrido-trabajador')->with('msj','Registro Exitoso');
   }
   public function edit_formu_trabajador($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-recorrido-trabajador',compact('data'));
    }

    public function update_form_trabajador(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/recorrido-trabajador/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/recorrido-trabajador',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-recorrido-trabajador')->with('mjs2','Formulario Actualizado');
    }
/*------------------------------------MODULO DE FORMULARIOS ADMINISTRADOR----------------------------------------------- */ 
/*------------------------------------- -------------------------------------------------------------------------------- */
/*------------------------------------------------------------------------------------------------------------------------ */

 public function form_sede_principal(){

      return view('documentacion-pdf.formulario-material-sede-principal');
   }

  public function lista_sede_principal(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 7)
                        ->get();
      return view('documentacion-pdf.lista-material-sede-principal')->with('data',$data);
   }

   public function store_formulario_sede_principal(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/sede-principal',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-material-sede-principal')->with('msj','Registro Exitoso');
   }
    public function edit_formu_sede_principal($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-sede-principal',compact('data'));
    }

    public function update_form_sede_principal(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/sede-principal/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/sede-principal',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-material-sede-principal')->with('mjs2','Formulario Actualizado');
    }
    public function form_mantenimiento(){

      return view('documentacion-pdf.formulario-mantenimiento');
   }
     public function lista_mantenimiento(){
      $data = Documento::select('documentos.*')
                        ->join('users as u', 'user_id','=','u.id')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('documentos.nombre_doc','documentos.file_doc','documentos.created_at','u.name','c.nombre_documentos','documentos.id')
                        ->where('id_catd','=', 8)
                        ->get();
      return view('documentacion-pdf.lista-mantenimiento')->with('data',$data);
   }
public function store_formulario_mantenimiento(Request $request){

        $data = new \App\Documento();
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        $file = $request->file('file_doc');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/mantenimiento',$newName);
        $data->file_doc = $newName;
        $data->save();

     return redirect('lista-mantenimiento')->with('msj','Registro Exitoso');
   }

   public function edit_formu_mantenimiento($id){
       
        $data = \App\Documento::findOrFail($id);
        return view('documentacion-pdf.edit-form-mantenimiento',compact('data'));
    }

   public function update_form_mantenimiento(Request $request)
    {
        $data = \App\Documento::find($request->id);
        $data->nombre_doc = $request->input('nombre_doc');
        $data->id_catd = $request->input('id_catd');
        $data->user_id = $request->input('user_id');
        if (empty($request->file('file_doc'))){
            $data->file_doc = $data->file_doc;
        }
        else{
            unlink('uploads/mantenimiento/'.$data->file_doc); //menghapus file lama
            $file = $request->file('file_doc');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/mantenimiento',$newName);
            $data->file_doc = $newName;
        }
        $data->save();
        return redirect('lista-mantenimiento')->with('mjs2','Formulario Actualizado');
    }
       public function contact(Request $request){
        $subject = "Asunto del correo";
        $for = "juanbgedu@gmail.com";
        Mail::send('email',$request->all(), function($msj) use($subject,$for){
            $msj->from("rojascecibel@gmail.com","Bancamiga Banco Microfinanciero");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back();
    }

    public function unidades_administrativas(){

      $unidad = Departamento::All();

      return view('unidad_administrativa.ver_listados_unidad');
    }
/*-------------------------------------------------------------------------------------------------------------------- */
/*-------------------------------------------------------RED DE AGENCIAS-----------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

  public function listado_red_agencia(Request $request){

    $listado = Red_oficina::Buscador($request->cod_oficina)
                            ->paginate(10);

    return view('red-agencias.listado-red-agencias',compact('listado'));
  }
 public function listado_almacenes_agencias(Request $request){

    $almacenes = Almacenes::Buscador($request->cod_almacen)
                            ->select('almacenes.*')
                            ->join('red_oficinas as r','almacenes.cod_oficina','=','r.id')
                            ->select('almacenes.cod_almacen','almacenes.nom_almacen','r.cod_oficina','r.nom_oficina','almacenes.created_at','almacenes.id')
                            ->paginate(10);
    return view('red-agencias.listado-almacenes',compact('almacenes'));
  }

  public function create_red_oficinas(){

    return view('red-agencias.crear-oficinas'); 
  }

  public function store_red_oficinas(Request $request){

     $agencia = new Red_oficina();

     $agencia->cod_oficina = $request->input('cod_oficina');
     $agencia->nom_oficina = $request->input('nom_oficina');
     $agencia->save();

    return redirect('/listado-red-agencias')->with('msj','Oficina Bancamiga creada con exito!');
  }

    public function edit_red_agencia($id){
       $agencia = Red_oficina::find($id);
       return view('red-agencias.editar_red_agencia')->with('agencia',$agencia);
       
    }
    public function update_red_agencia(Request $request){
        
    $agencia = Red_oficina::find($request->id);
      
    $agencia->cod_oficina = $request->cod_oficina;
    $agencia->nom_oficina = $request->nom_oficina;
    $agencia->save();
       return redirect('listado-red-agencias')->with('mjs2','La Oficina Red de Agencia Bancamiga a sido actualizada');   
    }
    public function eliminar_agencia($id){

      $agencia = Red_oficina::find($id);
      $agencia->delete();
  
     return  redirect('/listado-red-agencias')->with('mjs1','La Oficina Red de Agencia Bancamiga fue Eliminada!');
   }
  /*-------------------------------------------------------------------------------------------------------------------- */
/*-------------------------------------------------------RED DE AGENCIAS-ALMACEN-----------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */


public function create_almacen(){


  return view('red-agencias.crear-almacenes');
}

  public function store_almacenes(Request $request){

     $almacen = new Almacenes();

     $almacen->cod_almacen = $request->input('cod_almacen');
     $almacen->nom_almacen = $request->input('nom_almacen');
     $almacen->cod_oficina = $request->input('cod_oficina');
     $almacen->save();

    return redirect('/listado-almacenes')->with('msj','Oficina Bancamiga creada con exito!');
  }

 public function edit_almacenes($id){
       $almacen = Almacenes::find($id);
       return view('red-agencias.editar_almacenes')->with('almacen',$almacen);
       
    }
    public function update_almacen(Request $request){
        
    $almacen = Almacenes::find($request->id);
      
    $almacen->cod_almacen = $request->cod_almacen;
    $almacen->nom_almacen = $request->nom_almacen;
    $almacen->cod_oficina = $request->cod_oficina;
    $almacen->save();
       return redirect('listado-almacenes')->with('mjs2','Almacen de Red de Agencia Bancamiga a sido actualizada');   
    }

public function eliminar_almacen($id){

      $almacen = Almacenes::find($id);
      $almacen->delete();
  
     return  redirect('/listado-almacenes')->with('mjs1','Almacen Red de Agencia Bancamiga fue Eliminada!');
   }
  /*-------------------------------------------------------------------------------------------------------------------- */
/*-------------------------------------------DEPARTAMENTO UNIDAD ADMINISTRATIVA----------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

public function listado_unidad_administrativa(Request $request){

  $unidad = Departamento::Buscador($request->descripcion)
                          ->paginate(10);

  return view('red-agencias.listado_unidad_administrativa',compact('unidad'));
}

public function create_unidad_admin(){

  return view('red-agencias.crear_unidad_administrativa');
}

  public function store_unidad_admin(Request $request){

     $departamento = new Departamento();

     $departamento->cod_departamento = $request->input('cod_departamento');
     $departamento->descripcion = $request->input('descripcion');
     $departamento->save();

    return redirect('/listado_unidad_administrativa')->with('msj','Unidad Administrativa Bancamiga creada con exito!');
  }

  public function edit_unidad_admin($id){
       $departamento = Departamento::find($id);
       return view('red-agencias.editar_unidad_administrativa')->with('departamento',$departamento);
       
    }
    public function update_unidad_admin(Request $request){
        
    $departamento = Departamento::find($request->id);
      
    $departamento->cod_departamento = $request->cod_departamento;
    $departamento->descripcion = $request->descripcion;
    $departamento->save();
       return redirect('/listado_unidad_administrativa')->with('mjs2','Unidad Administrativa Bancamiga a sido actualizada');   
    }
public function eliminar_unidad_admin($id){

      $departamento = Departamento::find($id);
      $departamento->delete();
  
     return  redirect('/listado_unidad_administrativa')->with('mjs1','Unidad Administrativa Bancamiga fue Eliminada!');
   }
  /*-------------------------------------------------------------------------------------------------------------------- */
/*-------------------------------------------MATERIALES DE PROVEEDURIA--------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

public function listado_material_proveeduria(Request $request){
  $materiales = Materiales_proveeduria::Buscador($request->cod_material)
                                        ->select('materiales_proveedurias.*')
                                        ->join('categoria_materiales as c','id_categoria_material','=','c.id')
                                        ->select('materiales_proveedurias.cod_material','materiales_proveedurias.nom_material','c.categoria_proveeduria','materiales_proveedurias.created_at','materiales_proveedurias.id')
                                        ->paginate(15);

  return view('material-proveeduria.listado-material-proveeduria',compact('materiales'));
}

public function categoria_materiales(){

  return view('material-proveeduria.categoria_materiales');
}

public function store_cetegoria_materiales(Request $request){

   $cate_material = new Categoria_materiales();

     $cate_material->categoria_proveeduria = $request->input('categoria_proveeduria');
     $cate_material->save();

    return redirect('/categoria_materiales')->with('msj','categoría creada con exito!');
}
 public function edit_categoria_proveeduria($id){
       $cate_material = Categoria_materiales::find($id);
       return view('material-proveeduria.editar_categoria_materiales')->with('cate_material',$cate_material);
       
    }
    public function update_categoria_materiales(Request $request){
        
    $cate_material = Categoria_materiales::find($request->id);
      
    $cate_material->categoria_proveeduria = $request->categoria_proveeduria;
    $cate_material->save();
       return redirect('/categoria_materiales')->with('mjs2','La Categoría de Materiales de Proveeduría a sido actualizada');   
    }
public function eliminar_categoria_proveeduria($id){

      $cate_material = Categoria_materiales::find($id);
      $cate_material->delete();
  
     return  redirect('/categoria_materiales')->with('mjs1','La Categoría de Materiales de Proveeduría fue Eliminada!');
   }

   public function create_materiales(){

    return view('material-proveeduria.crear_materiales');
   }

   public function store_materiales_proveeduria(Request $request){
     

     $material = new Materiales_proveeduria();

     $material->cod_material = $request->input('cod_material');
     $material->nom_material = $request->input('nom_material');
     $material->id_categoria_material = $request->input('id_categoria_material');
     $material->save();


    return redirect('/listado-material-proveeduria')->with('msj','El material se a creado con exito!');
   }
 public function edit_materiales($id){
       $materiales = Materiales_proveeduria::find($id);
       return view('material-proveeduria.editar_materiales')->with('materiales',$materiales);
       
    }
    public function update_materiales(Request $request){
        
    $materiales = Materiales_proveeduria::find($request->id);
      
    $materiales->cod_material = $request->cod_material;
    $materiales->nom_material = $request->nom_material;
    $materiales->id_categoria_material = $request->id_categoria_material;
    $materiales->save();
       return redirect('/listado-material-proveeduria')->with('mjs2','El material a sido actualizado');   
    }
public function eliminar_materiales($id){

      $materiales = Materiales_proveeduria::find($id);
      $materiales->delete();
  
     return  redirect('/listado-material-proveeduria')->with('mjs1','El material fue Eliminada!');
   }

}