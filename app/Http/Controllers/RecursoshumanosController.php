<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transporte;
use App\Viatico;
use PDF;

class RecursoshumanosController extends Controller
{

/* Workflow crediamigo */
public function index(Request $request){
	  $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
     
      $consulta1 = Viatico::Buscador($request->estatus)
             ->select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
	return view('servicio-empleados.wf-crediamigo')->with('consulta',$consulta)->with('consulta1',$consulta1);
}

/* Workflow uniformes escolares */
public function uniformes_escolares(Request $request){
	 $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
	return view('servicio-empleados.wf-unif-escolares')->with('consulta',$consulta);
}

/*-------------------------------------------------------------------------------------------------------------------- */
/*---------------------------------------------------------CONTRATOS CREDIAMIGO----------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

   public function reporte_crediamigo($id){

    $datos= Transporte::find($id);
    $tipo = Transporte::select('transportes.*')
            ->join('Tipo_servicios as t','servicio_id','t.id')
            ->select('t.descripcion')
            ->first();
    $pdf = PDF::loadView('wf-reporte.reporte-crediamigo', compact('datos','tipo'));
    return $pdf->download('contrato-crediamigo.pdf');
} 

public function view_contrato_crediamigo(){

  return view('wf-reporte.reporte-crediamigo');
}



}