<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TransporteRequest;
use Illuminate\Support\Facades\Auth;
use App\Tipo_servicio;
use App\Cant_persona;
use App\Cant_unidad;
use App\Transporte;
use App\Persona;
use App\Documento;
use App\User;
use App\Viatico;
use PDF;
use Mail;
use App\Roles;

class WfadminController extends Controller
{

/* Workflow Transporte */

public function vista_Transporte(Request $request)
   {
     $supervisores = Persona::select('personas.*')->where('id_rol', 2)->get();
   

      $dato = Persona::select('personas.*')
                      ->join('users as u','user_id','=','u.id')
                      ->join('departamentos as d','id_departamento','=','d.id')
                      ->join('codigo_cedulas as c','cod_cedula','=','c.id')
                      ->join('roles as r','personas.id_rol','=','r.id')
                      ->select('personas.nombres', 'personas.apellidos','c.nombre_valor','personas.cedula','personas.celular','d.descripcion','r.nombre_roles')
                      ->where('user_id', Auth::user()->id)
                      ->first();

      $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
     
      $consulta1 = Viatico::Buscador($request->estatus)
             ->select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);


       
	return view('servicio-empleados.wf-transporte',compact('dato','supervisores'))->with('consulta',$consulta)->with('consulta1',$consulta1);
}

public function crear_wf_transporte(){

  return view('servicio-empleados.wf-transporte');
}

public function store_wf_transporte(Request $request){

  $wft = new Transporte;
  $roles = new Roles();

  $god = $roles->where("supervisor_flag",TRUE)->first();

  $fechas = date_create($request->get('fecha'));
  $format = date_format($fechas, 'Y-m-d');
  
  $horas = date_create($request->get('hora'));
  $format1 = date_format($horas, 'H:m:s');

  $wft->user_id=auth()->user()->id;
  $wft->servicio_id=$request->get('servicio_id');
  $wft->servicio_otros=$request->get('servicio_otros');
  $wft->fecha = $format;
  $wft->hora=$format1;
  $wft->cantp_id=$request->get('cantp_id');
  $wft->unidad_id=$request->get('unidad_id');
  $wft->ivuelta=$request->get('ivuelta');
  $wft->espera=$request->get('espera');
  $wft->motivo=$request->get('motivo');
  $wft->id_supervisor=$god->id;
  $wft->estado = 'pendiente';

  $wft->save(); 

        $subject = "Servicio de Transporte";
        $for = "juanbgedu@gmail.com";
        Mail::send('emails.email_transporte',$request->all(), function($msj) use($subject,$for){
            $msj->from("rojascecibel@gmail.com","Bancamiga Banco Universal");
            $msj->subject($subject);
            $msj->to($for);
        });


  return redirect('/servicios-empleados')->with('msj','El Servicio Transporte a sido creado');
}

public function editar_workflow_transporte($id){

      $transporte = Transporte::find($id);
      $supervisores = Persona::select('personas.*')->where('vigente', 'S' )->get();
      
      $tipo_serv = Tipo_servicio::select('Tipo_servicios.*')
                                  ->select('descripcion')  
                                  ->get();

                                    
  
      $dato = Persona::select('personas.*')
                      ->join('users as u','user_id','=','u.id')
                      ->join('departamentos as d','id_departamento','=','d.id')
                      ->join('codigo_cedulas as c','cod_cedula','=','c.id')
                      ->join('roles as r','personas.id_rol','=','r.id')
                      ->select('personas.nombres', 'personas.apellidos','c.nombre_valor','personas.cedula','personas.celular','d.descripcion','r.nombre_roles','personas.user_id')
                      ->where('user_id', $transporte->user_id)
                      ->first();

      $consulta = Transporte::select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->paginate(5);
     
      $consulta1 = Viatico::select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);

       return view('servicio-empleados.editar_wf_transporte',compact('dato','supervisores','consulta','consulta1','tipo_serv'))->with('transporte',$transporte);

}

public function update_workflow_transporte(Request $request){

  $transporte = Transporte::find($request->id);
  $roles = new Roles();
  $god = $roles->where("supervisor_flag",TRUE)->first();

  $fecha_rev = date_create($request->get('fecha_revision'));
  $format2 = date_format($fecha_rev, 'Y-m-d');


  $fechas = date_create($request->get('fecha'));
  $format = date_format($fechas, 'Y-m-d');
  
  $horas = date_create($request->get('hora'));
  $format1 = date_format($horas, 'H:m:s');

  $transporte->user_id=$request->user_id;
  $transporte->servicio_id = $request->servicio_id;
  $transporte->servicio_otros = $request->servicio_otros;
  $transporte->fecha =  $format;
  $transporte->hora = $format1;
  $transporte->cantp_id = $request->cantp_id;
  $transporte->unidad_id = $request->unidad_id;
  $transporte->ivuelta = $request->ivuelta;
  $transporte->espera = $request->espera;
  $transporte->motivo = $request->motivo;
  $transporte->id_supervisor=$god->id;
  $transporte->estado = $request->estado;
  $transporte->fecha_revision = $format2;
  $transporte->observacion_dev = $request->observacion_dev;
  $transporte->save();

  return redirect('/servicios-empleados')->with('msjwf','Notificación de Respuesta de Solicitud de Servicio de Transporte');
}

public function email_transporte(){

  return view('emails.email_transporte');
}

public function view_trasnporte(Request $request){


/* Formularios de transporte pendientes */

$objPersona = new Persona();
$dataUser = $objPersona->where("user_id",auth()->user()->id)->first();

if(!$dataUser){
  return redirect('/ficha_empleado')->with('msj','Complete todos sus datos');  
}
 
$objTransporte = new Transporte();



$listarPendientes = $objTransporte->listar(array("rol"=>$dataUser->id_rol))->get();

   $data = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 1)
                        ->first(); 
    $data1 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 9)
                        ->first();
    $data2 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 2)
                        ->first();
    $data3 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 3)
                        ->first();
    $data4 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 4)
                        ->first();
    $data5 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 5)
                        ->first();   
    $data6 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 7)
                        ->first();   
    $data7 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 8)
                        ->first(); 

      $wfv = Viatico::Buscador($request->estatus)
        ->select('viaticos.*')
        ->join('users as u','user_id','u.id')
        ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
        ->where('user_id', Auth::user()->id)
        ->paginate(10);                   
    
        return view('servicio-empleados.servicios-generales', compact('wfv','data','data1','data2','data3','data4','data5','data6','data7','listarPendientes'))->with('wfv',$wfv);
     
}

public function reporte_transporte($id){

    $datos= Transporte::find($id);
    $solicitud = Transporte::select('transportes.*')
                  ->join('users as u','user_id','=','u.id')
                  ->join('personas as p','user_id','=','p.id')
                  ->join('roles as r','id_rol','=','r.id')
                  ->join('codigo_cedulas as c','cod_cedula','=','c.id')
                  ->select('p.nombres','p.apellidos','p.cod_cedula','p.cedula','p.celular')
                  ->get();
    $pdf = PDF::loadView('wf-reporte.reporte-transporte', compact('datos'));


    return $pdf->download('solicitud-transporte.pdf');
} 	


public function view_reporte_transporte(){

  return view('wf-reporte.reporte-transporte');
}

/*-----------------------------------------------------------PDF INDIVIDUAL Y GENERAL-------------------------------- */

/*-----------------------------------------------------------CONSULTAS GENERALES------------------------------------- */

  public function consulta_gh(){
     $data = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 1)
                        ->first(); 
    $data1 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 9)
                        ->first();
    $data2 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 2)
                        ->first();
    $data3 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 3)
                        ->first();
    $data4 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 4)
                        ->first();
    $data5 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 5)
                        ->first();   
    $data6 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 7)
                        ->first();   
    $data7 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 8)
                        ->first(); 
      return view('servicio-empleados.consulta-gestion-humana', compact('data','data1','data2','data3','data4','data5','data6','data7'));
   }
    public function consulta_ad(){
      /* Formularios de transporte pendientes */

$objPersona = new Persona();
$dataUser = $objPersona->where("user_id",auth()->user()->id)->first();

$objTransporte = new Transporte();



$listarPendientes = $objTransporte->listar(array("rol"=>$dataUser->id_rol))->get();
   $data = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 1)
                        ->first(); 
    $data1 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 9)
                        ->first();
    $data2 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 2)
                        ->first();
    $data3 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 3)
                        ->first();
    $data4 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 4)
                        ->first();
    $data5 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 5)
                        ->first();   
    $data6 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 7)
                        ->first();   
    $data7 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 8)
                        ->first(); 
      return view('servicio-empleados.consulta-administracion', compact('data','data1','data2','data3','data4','data5','data6','data7','listarPendientes'));
   }

   public function consulta_mp(){

   $data = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 1)
                        ->first(); 
    $data1 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 9)
                        ->first();
    $data2 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 2)
                        ->first();
    $data3 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 3)
                        ->first();
    $data4 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 4)
                        ->first();
    $data5 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 5)
                        ->first();   
    $data6 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 7)
                        ->first();   
    $data7 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 8)
                        ->first(); 
      return view('servicio-empleados.consulta-materiales', compact('data','data1','data2','data3','data4','data5','data6','data7'));
   }
    public function consulta_viaticos(){

   $data = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 1)
                        ->first(); 
    $data1 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 9)
                        ->first();
    $data2 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 2)
                        ->first();
    $data3 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 3)
                        ->first();
    $data4 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 4)
                        ->first();
    $data5 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 5)
                        ->first();   
    $data6 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 7)
                        ->first();   
    $data7 = Documento::select('documentos.*')
                        ->join('categoria_documentos as c','id_catd','c.id')
                        ->select('file_doc')
                        ->where('id_catd','=', 8)
                        ->first(); 
      return view('servicio-empleados.consulta-viaticos', compact('data','data1','data2','data3','data4','data5','data6','data7'));
   }
/*-------------------------------------------------------------------------------------------------------------------- */
/*---------------------------------------------------------MANTENIMIENTO-----------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

   public function index(Request $request)
   {
      $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
     
      $consulta1 = Viatico::Buscador($request->estatus)
             ->select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
      return view('servicio-empleados.wf-material-proveeduria')->with('consulta',$consulta)->with('consulta1',$consulta1);
   }

   public function reporte_mantenimiento($id){

    $datos= Transporte::find($id);
    $tipo = Transporte::select('transportes.*')
            ->join('Tipo_servicios as t','servicio_id','t.id')
            ->select('t.descripcion')
            ->first();
    $pdf = PDF::loadView('wf-reporte.reporte-mantenimiento', compact('datos','tipo'));
    return $pdf->download('solicitud-mantenimiento.pdf');
}   


public function view_reporte_mantenimiento(){

  return view('wf-reporte.reporte-mantenimiento');
}

/*-------------------------------------------------------------------------------------------------------------------- */
/*---------------------------------------------------------VIATICOS----------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------- */

public function viaticos(Request $request){
    $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
     
      $consulta1 = Viatico::Buscador($request->estatus)
             ->select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
      return view('servicio-empleados.wf-viaticos')->with('consulta',$consulta)->with('consulta1',$consulta1);
   }

public function store_workflow_viaticos(Request $request){
   
   $sv = new Viatico;

   $fecha_salida = date_create($request->get('fecha_salida'));
   $format = date_format($fecha_salida, 'Y-m-d');

   $hora_salida = date_create($request->get('hora_salida'));
   $format1 = date_format($hora_salida, 'H:m:s');

   $fecha_regreso = date_create($request->get('fecha_regreso'));
   $format2 = date_format($fecha_regreso, 'Y-m-d');

   $hora_regreso = date_create($request->get('hora_regreso'));
   $format3 = date_format($hora_regreso, 'H:m:s');


   $sv->user_id=$request->get('user_id');
   $sv->estado_id=$request->get('estado_id');
   $sv->fecha_salida = $format;
   $sv->hora_salida = $format1;
   $sv->fecha_regreso = $format2;
   $sv->hora_regreso = $format3;
   $sv->n_dias=$request->get('n_dias');
   $sv->tipo_servicio=$request->get('tipo_servicio');
   $sv->n_cuentabancaria=$request->get('n_cuentabancaria');
   $sv->motivo_viaje=$request->get('motivo_viaje');
   $sv->cant=$request->get('cant');
   $sv->costo_u=$request->get('costo_u');
   $sv->monto_1=$request->get('monto_1');
   $sv->cant2=$request->get('cant2');
   $sv->costo_u2=$request->get('costo_u2');
   $sv->monto_2=$request->get('monto_2');
   $sv->cant3=$request->get('cant3');
   $sv->costo_u3=$request->get('costo_u3');
   $sv->monto_3=$request->get('monto_3');
   $sv->subtotal1=$request->get('subtotal1'); 
   $sv->gastos_id=$request->get('gastos_id');
   $sv->gastos_id1=$request->get('gastos_id1');
   $sv->gastos_id2=$request->get('gastos_id2');
   $sv->cant_gasto=$request->get('cant_gasto');
   $sv->costo_gasto=$request->get('costo_gasto');
   $sv->monto_gasto=$request->get('monto_gasto');
   $sv->cant_gasto2=$request->get('cant_gasto2');
   $sv->costo_gasto2=$request->get('costo_gasto2');
   $sv->monto_gasto2=$request->get('monto_gasto2');
   $sv->cant_gasto3=$request->get('cant_gasto3');
   $sv->costo_gasto3=$request->get('costo_gasto3');
   $sv->monto_gasto3=$request->get('monto_gasto3');
   $sv->subtotal_gasto=$request->get('subtotal_gasto');
   $sv->total_general=$request->get('total_general');
   $sv->observacion_viaticos=$request->get('observacion_viaticos');
   $sv->supervisor_id=$request->get('supervisor_id');
   $sv->estatus = 'pendiente';
   $sv->save();


  return redirect('/servicios-empleados')->with('msj','El Servicio Viatico a sido creado');
}

public function material_proveeduria(Request $request){
  $consulta = Transporte::Buscador($request->name)
             ->select('transportes.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','transportes.created_at','transportes.id','transportes.estado','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
     
      $consulta1 = Viatico::Buscador($request->estatus)
             ->select('viaticos.*')
             ->join('users as u','user_id','=','u.id')
             ->select('u.name','viaticos.created_at','viaticos.id','viaticos.estatus','u.id')
             ->where('user_id', Auth::user()->id)
             ->paginate(5);
    return view('servicio-empleados.wf-material-proveeduria')->with('consulta',$consulta)->with('consulta1',$consulta1);          

}



}
 
  