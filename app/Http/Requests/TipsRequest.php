<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipsRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     public function messages()
    {
    return [
        'titulo_tips.required' => 'El :Título es obligatorio.',
        'contenido_tips.required' => 'El :Contenido es obligatorio.',
        'extracto_tips.required' => 'El :Extracto es obligatorio.',
        'id_catips.required' => 'El :La Categoría es obligatoria.',
        'image_tips.required' => 'El :La Imagen debe tener los tamaños exactos 720 Ancho / 440 alto.',
    ];
    }
    public function rules()
    {
        return [
            'titulo_tips' => 'required',
            'contenido_tips' => 'required',
            'extracto_tips' => 'required|max:150',
            'id_catips' => 'required',
            'image_tips' => 'dimensions:min_width=720,min_height=440',
        ];
    }
}
