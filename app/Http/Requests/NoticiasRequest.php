<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function messages()
    {
    return [
        'titulo.required' => 'El :Título es obligatorio.',
        'subtitulo.required' => 'El :Subtítulo es obligatorio.',
        'contenido.required' => 'El :Contenido es obligatorio.',
        'extracto.required' => 'El :Extracto es obligatorio.',
        'id_catenot.required' => 'El :La Categoría es obligatoria.',
        'image.required' => 'El :La Imagen debe tener los tamaños exactos 1920 Ancho / 980 alto.',
    ];
    }
    
    public function rules()
    {
        return [
            'titulo' => 'required|string',
            'subtitulo' => 'required|string',
            'contenido' => 'required|string',
            'extracto' => 'required|max:150',
            'id_catenot' => 'required',
            'image' => 'dimensions:min_width=1920,min_height=980|max:5000',
        ];
    }
}
