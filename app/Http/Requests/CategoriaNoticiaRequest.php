<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaNoticiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function messages()
    {
    return [
        'nombre.required' => 'El :Nombre es obligatorio.',
        'descripcion.required' => 'La :descripcion es obligatoria.',
    ];
    }
    public function rules()
    {
        return [
        'nombre' => 'required',
        'descripcion' => 'required',
        ];
    }
}
