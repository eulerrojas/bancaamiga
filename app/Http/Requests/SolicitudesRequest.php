<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SolicitudesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
       public function messages()
    {
    return [
        'titulo_solicitud.required' => 'El :Título es obligatorio.',
        'link.required' => 'El :Link es obligatorio.',        
        'image_solicitud.required' => 'El :La Imagen debe tener los tamaños exactos 225 Ancho / 225 alto.',
    ];
    }
    public function rules()
    {
        return [
            'titulo_solicitud' => 'required|string',
            'link' => 'required',  'required',
            'image_solicitud' => 'dimensions:min_width=225,min_height=225|max:5000',
        ];
    }
}
