<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransporteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
    return [
        'servicio_id.required' => 'El :Tipo de Servicio es obligatorio.',
        'fecha.required' => 'La :Fecha de la solicitud es obligatoria.',
        'hora.required' => 'La :Hora de la solicitud es obligatoria.',
        'cantp_id.required' => 'El :Numero de Personas es obligatoria.',
        'unidad_id.required' => 'El :Numero de Unidades es obligatorio.',
        'ivuelta.required' => 'El :Valor Ida y Vuelta es obligatorio.',
        'espera.required' => 'El :Tiempo de espera es obligatorio.',
        'motivo.required' => 'El :Motivo es obligatorio.',
        'id_supervisor.required' => 'El :Supervisor es obligatorio.',
    ];
    }
    public function rules()
    {
        return [
            'servicio_id' => 'requerido',
            'fecha' => 'requerido',
            'hora' => 'requerido',
            'cantp_id' => 'requerido',
            'unidad_id' => 'requerido',
            'ivuelta' => 'requerido',
            'espera' => 'requerido',
            'motivo' => 'requerido',
            'id_supervisor' => 'requerido',
            
        ];
    }
}
