<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tipo_servicio extends Model {

    protected $table = 'tipo_servicios';
    protected $fillable = [
        'descripcion', 'otros',
    ];



}
