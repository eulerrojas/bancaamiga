<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Slider extends Model {

    protected $table = 'sliders';
    protected $fillable = [
        'titulo_slider', 'imagen_slider',
    ];

 public function scopeBuscador($query, $titulo_slider){

     return $query->where('titulo_slider', 'LIKE', "%$titulo_slider%");
        
    }

}
