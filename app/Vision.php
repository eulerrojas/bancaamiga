<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class vision extends Model {

    protected $table = 'visions';
    protected $fillable = [
        'titulo_vision','contenido_vision','image_vision',
    ];


	 public function scopeBuscador($query, $titulo_vision){

     return $query->where('titulo_vision', 'LIKE', "%$titulo_vision%");
        
    }

}
