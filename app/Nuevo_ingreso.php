<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nuevo_ingreso extends Model {

    protected $table = 'nuevos_ingresos';
    protected $fillable = [
        'titulo_eventoi','image_eventoi',
    ];


	 public function scopeBuscador($query, $titulo_eventoi){

     return $query->where('titulo_eventoi', 'LIKE', "%$titulo_eventoi%");
        
    }

}
