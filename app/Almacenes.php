<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Almacenes extends Model {

    protected $table = 'almacenes';
    protected $fillable = [
        'cod_almacen','nom_almacen','cod_oficina'
    ];


	 public function scopeBuscador($query, $cod_almacen){

     return $query->where('cod_almacen', 'LIKE', "%$cod_almacen%");
        
    }
 	public function red_agencia()
	{
		 return $this->belongsTo('red_agencias');
	}
}