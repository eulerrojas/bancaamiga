<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Estructura_organizativa extends Model {

    protected $table = 'estructuras_organizativas';
    protected $fillable = [
        'titulo_estructura','subtitulo_estructura','image_estructura',
    ];


	 public function scopeBuscador($query, $titulo_estructura){

     return $query->where('titulo_estructura', 'LIKE', "%$titulo_estructura%");
        
    }

}
