<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria_documento extends Model {

    protected $table = 'categoria_documentos';
    protected $fillable = [
        'nombre_documentos',
    ];


}
