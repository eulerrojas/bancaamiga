<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mantenimiento extends Model {

    protected $table = 'mantenimientos';
    protected $fillable = [
        'nombre_agencia', 'id_electricidad','id_infraestructura','id_plomeria','id_mobiliario','id_equipo','id_seguridad','id_publicidad','id_otros_servicios','observacion_mantenimiento','id_supervisor','estatus_m',
    ];

 public function scopeBuscador($query, $nombre_agencia){

     return $query->where('nombre_agencia', 'LIKE', "%$nombre_agencia%");
        
    }
}
