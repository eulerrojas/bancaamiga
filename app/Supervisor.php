<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Supervisor extends Model {

    protected $table = 'supervisores';
    protected $fillable = [
        'nombre_supervisor	','apellido_supervisor','cedula_supervisor',
    ];


	 public function scopeBuscador($query, $titulo_solicitud){

     return $query->where('titulo_solicitud', 'LIKE', "%$titulo_solicitud%");
        
    }

}
