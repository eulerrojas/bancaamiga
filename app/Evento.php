<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Evento extends Model
{
    protected $fillable = [
        'titulo', 'fecha_inicio', 'fecha_final','color',
    ];

    public $timestamps = false;

     public function scopeBuscador($query, $titulo){

     return $query->where('titulo', 'LIKE', "%$titulo%");
        
    }

}