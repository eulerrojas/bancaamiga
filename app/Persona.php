<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Persona extends Model {

    protected $table = 'personas';
    protected $fillable = [
        'user_id','nombres', 'apellidos','cod_cedula','cedula','telefono_hab','celular','id_departamento','id_rol',''
    ];



}
