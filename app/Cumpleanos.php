<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cumpleanos extends Model {

    protected $table = 'cumpleanos';
    protected $fillable = [
        'titulo_eventoc','image_eventoc',
    ];


	 public function scopeBuscador($query, $titulo_eventoc){

     return $query->where('titulo_eventoc', 'LIKE', "%$titulo_eventoc%");
        
    }

}
