<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Transporte extends Model {

    protected $table = 'transportes';
    protected $fillable = [
        'user_id', 'motivo','servicio_id','servicio_otros','unidad_id','cantp_id','fecha','hora','ivuelta','espera','estado','id_supervisor','fecha_revision','observacion_dev',
    ];

 public function scopeBuscador($query, $name){

     return $query->where('name', 'LIKE', "%$name%");
        
    }

public function listar($params){

	

	$response = $this->select("*",
		DB::raw("(SELECT CONCAT(p.nombres,' ',p.apellidos) from personas as p inner join roles as r on r.id = p.id_rol where p.vigente = 'S') as supervisor"),
		DB::raw("(SELECT name from users as u where u.id = ".$this->table.".user_id) as solicitante")
	);

	if(array_key_exists('estado', $params)){
		$response->where($this->table.'.estado',$params['estado']);
	}

	if(array_key_exists('rol', $params)){
		if($params['rol'] != 2){
			$response->where("user_id",auth()->user()->id);
		}
		
	}
	//echo $response->toSql();
	//die();
	return $response;

}


}
