<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Noticia extends Model {

    protected $table = 'noticias';
    protected $fillable = [
        'titulo','subtitulo','extracto','contenido','id_catenot','image',
    ];


	 public function scopeBuscador($query, $titulo){

     return $query->where('titulo', 'LIKE', "%$titulo%");
        
    }
  public function Categoria_noticia(){
         
         return $this->hasMany('App\Categoria_noticia','id');
     }
     

}
