<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Estados extends Model
{
    protected $fillable = [
        'estado',
    ];

    public $timestamps = false;



}