<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Solicitud extends Model {

    protected $table = 'solicitudes';
    protected $fillable = [
        'titulo_solicitud','link','image_solicitud',
    ];


	 public function scopeBuscador($query, $titulo_solicitud){

     return $query->where('titulo_solicitud', 'LIKE', "%$titulo_solicitud%");
        
    }

}
