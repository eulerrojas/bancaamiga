<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria_materiales extends Model {

    protected $table = 'categoria_materiales';
    protected $fillable = [
        'categoria_proveeduria',
    ];


}
