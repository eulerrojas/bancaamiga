<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Documento extends Model {

    protected $table = 'documentos';
    protected $fillable = [
       'nombre_doc	','id_catd','file_doc','user_id',
    ];


	 public function scopeBuscador($query, $nombre_doc){

     return $query->where('nombre_doc', 'LIKE', "%$nombre_doc%");
        
    }

}
