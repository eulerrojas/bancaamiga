<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Valores extends Model {

    protected $table = 'valores';
    protected $fillable = [
        'titulo_valores','contenido_valores','image_valores',
    ];


	 public function scopeBuscador($query, $titulo_valores){

     return $query->where('titulo_valores', 'LIKE', "%$titulo_valores%");
        
    }

}
