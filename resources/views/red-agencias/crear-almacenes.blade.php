@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Red de Agencias Bancamiga <i class="fa fa-angle-right"></i> Nuevo Almacén </h3>
     </div>
</div> 
<br>
<div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
<ul>
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul> 
	<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('registrar-almacen')}}">
      {{ csrf_field() }} 
							<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Código de Almacén</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="cod_almacen" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Nombre del Almacén</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="nom_almacen" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Agencia al que pertenece:</label>
								
								<div class="col-sm-8">
									<select name="cod_oficina" class="select2" data-allow-clear="true" data-placeholder="Seleccione Oficina">
										<option></option>
										<optgroup label="Red de Agencias Bancarias">
										@php
                                        $agencias = App\Red_oficina::all();
                                        @endphp
                                        @foreach($agencias as $key)
											<option value="{{$key->id}}">{{$key->cod_oficina}} - {{$key->nom_oficina}}</option>
										@endforeach	
										</optgroup>
									</select>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/listado-almacenes')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
@endsection