@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Listado Unidad Administrativa</h3>
     </div>
      <br>
      <br>
      <br>  
            <div class="col-md-12">
   <div class="col-md-3"></div>
        <div class="col-md-3"></div>
         <div class="col-md-6">
            <div class="form-group">
       <form role="search" class="form-horizontal form-groups-bordered" action="{{url('listado_unidad_administrativa')}}" method="GET" >
                                    
                        
                         <div class="input-group">

                             <input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="Teclee el nombre de la Unidad...">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
 
        </div>
        </div>
 </div> 
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
    
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important; text-align: center">
                            <th style="font-size: 12px !important; text-align: center">COD</th>
                            <th style="font-size: 12px !important; text-align: center">Unidad Administrativa Bancamiga</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Creación</th>
                            <th style="font-size: 12px !important; text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      
                    @foreach($unidad as $key)
                      <tr> 
                        <td style="text-align: center; font-weight: 700">{{$key->cod_departamento}}</td>
                        <td style="text-align: center; font-weight: 700">{{$key->descripcion}}</td>
                        <td style="text-align: center; font-weight: 700">{{$key->created_at}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editar_unidad_admin', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                            <button type="button" class="btn btn-danger">
                              <a href="{{url('eliminar_unidad_admin', $key->id)}}" onclick="return confirm('Deseas Eliminar? ')" style="color: #fff"><i class="fa fa-trash" title="Eliminar"></i></a>
                            </button>
                        </td>
                      </tr>
                    @endforeach                  
                    </tbody>
            </table>

         <div style="float: right;"></div> 
        </div>
      
    </div>
@endsection