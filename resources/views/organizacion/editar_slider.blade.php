@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar slider</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/slider')}}">
                  <i class="fa fa-reorder"></i>
                 Todas las Imágenes
                </a>
              </li>
           
            </ol>
          </div>   
     <br>
      <br>
      <br>  
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarslider')}}" enctype="multipart/form-data">
   <input type="hidden" name="id" value="{{$slider->id}}"> 
      {{ csrf_field() }} 
							    <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Imagen</label>
                
                <div class="col-sm-5">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                      <img src="/slider/{{$slider->image_slider}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                    <div>
                      <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Seleccionar imagen</span>
                        <span class="fileinput-exists">Cambio</span>
                        <input type="file" name="image_slider" accept="image/*" value="{{$slider->image_slider}}">
                      </span>
                      <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Retirar</a>
                    </div>
                    <span style="font-size: 10px; color: red"><strong>Atención!</strong> La Imágen debe tener un tamaño de <strong>540px ancho</strong> y<strong> 365px alto</strong></span>
                  </div>
                </div>
              </div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Título</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="titulo_slider" required="required"  value="{{$slider->titulo_slider}}">
								</div>
							</div>
						
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/ba-admin')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
@endsection