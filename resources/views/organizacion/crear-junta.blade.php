@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Agregar nuevo</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/vista_junta_directiva')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar
                </a>
              </li>
            
            </ol>
          </div>   
     <br>
      <br>
      <br>  
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('registrarjunta')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
							    <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Imagen</label>
                
                <div class="col-sm-5">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 80px; height: 80px;" data-trigger="fileinput">
                      <img src="{{asset('img/default-foto.png')}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 80px; max-height: 80px"></div>
                    <div>
                      <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Seleccionar imagen</span>
                        <span class="fileinput-exists">Cambio</span>
                        <input type="file" name="foto_directiva" accept="image/*">
                      </span>
                      <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Retirar</a>
                    </div>
                    <span style="font-size: 10px; color: red"><strong>Atención!</strong> La Imágen debe tener un tamaño de <strong>80px ancho</strong> y<strong> 80px alto</strong></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Nombres </label>
                
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="field-1" name="nombres_directiva" required="required">
                </div>
              </div>
              <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Nombres </label>
                
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="field-1" name="apellidos_directiva" required="required">
                </div>
              </div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Cargo Actual</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="cargo_directiva" required="required">
								</div>
							</div>
						  <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Profesión</label>
                
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="field-1" name="profesion" required="required">
                </div>
              </div>
              <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ubicación</label>
                
                <div class="col-sm-8">
                 <textarea class="form-control" name="ubicacion_directva" required="required"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="field-ta" class="col-sm-3 control-label">Resumen Curricular</label>
                
                <div class="col-sm-8">
                        <textarea class="form-control ckeditor" name="resumen_curricular" required="required"></textarea>
                </div>
              </div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/vista_junta_directiva')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
@endsection