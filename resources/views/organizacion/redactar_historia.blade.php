@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Redactar Historia de la Empresa</h3>
     </div>
</div> 
<br>  
     <br>
      <br>
      <br>  
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('registrar-historia')}}">
      {{ csrf_field() }} 
							 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Título Principal</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="titulo_principal" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Contenido</label>
								
								<div class="col-sm-8">
												<textarea class="form-control ckeditor" name="contenido" required="required"></textarea>
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/vista_historia')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
@endsection