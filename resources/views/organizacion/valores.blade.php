@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Valores</h3>
     </div>
   <br>
    <!-- Breadcrumb 1 -->
 <!--    <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li><a href="{{asset('crear-valores')}}">
              <i class="entypo-list-add"></i>
              Crear Valores</a></li>

            </ol>
          </div>  --> 
     <br>
      <br>
      <br>  
    
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
   
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                     
                            <th style="font-size: 12px !important">Título </th>
                            <th style="font-size: 12px !important;">Imagen</th>
                            <th style="font-size: 12px !important" width="60%">Contenido</th>
                            <th style="font-size: 12px !important">Fecha de Creación</th>
                            <th style="font-size: 12px !important">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      
         @foreach($valores as $key)
                      <tr> 
                       
                        <td>{{$key->titulo_valores}}</td>
                        <td><img src="/agregados/{{$key->image_valores}}" alt="..." width="70px" height="60px"></td>
                        <td>{{$key->contenido_valores}}</td>
                        <td>{{$key->created_at}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editarvalores', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                       <!--   <button type="button" class="btn btn-danger">
                              <a href="" onclick="return confirm('Deseas Eliminar? ')" style="color: #fff"><i class="fa fa-trash" title="Eliminar"></i></a>
                            </button> -->
                        </td>
                      </tr>
          @endforeach                      
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
@endsection