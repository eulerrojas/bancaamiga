@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Slider</h3>
     </div>
   <br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li><a href="{{asset('crear-slider')}}">
              <i class="entypo-picture"></i>
              Crear Imagen</a></li>

            </ol>
          </div>   
     <br>
      <br>
      <br>  
            <div class="col-md-12">
   <div class="col-md-3"></div>
        <div class="col-md-3"></div>
         <div class="col-md-6">
            <div class="form-group">
       <form role="form" class="form-horizontal form-groups-bordered" action="" method="GET" >
                                    
                        
                         <div class="input-group">
                             <input type="text" id="transporte" name="id" class="form-control" placeholder="Teclee para ver sugerencias...">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
 
        </div>
        </div>
 </div> 
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
   
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                     
                            <th style="font-size: 12px !important">Título </th>
                            <th style="font-size: 12px !important;" width="20%">Imagen</th>
                            <th style="font-size: 12px !important">Fecha de Creación</th>
                            <th style="font-size: 12px !important">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      
            @foreach($slider as $key)
                      <tr> 
                       
                        <td>{{$key->titulo_slider}}</td>
                        <td><img src="/slider/{{$key->image_slider}}" alt="..." width="150px"></td>
                        <td>{{$key->created_at}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editarslider', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                          <button type="button" class="btn btn-danger">
                              <a href="{{url('eliminar_sliders', $key->id)}}" onclick="return confirm('Deseas Eliminar? ')" style="color: #fff"><i class="fa fa-trash" title="Eliminar"></i></a>
                            </button> 
                        </td>
                      </tr>
            @endforeach                        
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
@endsection