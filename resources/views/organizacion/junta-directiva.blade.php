@extends('layouts.intranet')

@section('content')
 <div id="about1" class="about1-area area-padding">
        <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">
     
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Junta Directiva</h2>
          </div>
           
        </div>
 
      </div>
    
    </div>

           <div class="container">

    <div class="well" style="background-color: #f5f5f547"> 
  @foreach($junta as $key)    
     <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-1" style="padding: 0px">
                            <img src="/junta/{{$key->foto_directiva}}" style="width: 80px; border-radius: 50px;border:1px solid #b9b9b9;float: left; margin-left: 15px; margin-top: 14px">                        
                        </div>
                        <div class="col-md-11" style="padding: 10px 10px 10px 20px; line-height: 1.4;">
                             <span style="margin-top: 5px; font-size: 12px !important;font-weight: 700">{{$key->cargo_directiva}}
</span>
                             <br>
                            <span style="font-size: 14px;"><a href="#">{{$key->profesion}}. {{$key->nombres_directiva}} {{$key->apellidos_directiva}}<em style="color: #000"> - </em></a></span><span style="font-size: 10px; padding: 0 0 0 5px;">{{$key->ubicacion_directva}}</span>
                            <br>
                            <br>
                            <span>{!! html_entity_decode($key->resumen_curricular) !!}</span>
                            <hr>
                        </div>

    </div>

</div>
@endforeach
    </div>
</div>
  </div>
  
@endsection