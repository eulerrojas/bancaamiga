@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Junta Directiva</h3>
     </div>
      <br>
      <br>
      <br>  
    
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
   
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                     
                            <th style="font-size: 12px !important">Nombres y Apellidos</th>
                            <th style="font-size: 12px !important">Foto</th>
                            <th style="font-size: 12px !important">Cargo</th>
                            <th style="font-size: 12px !important">Profesión</th>
                            <th style="font-size: 12px !important">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      
         @foreach($junta as $key)
                      <tr> 
                       
                        <td>{{$key->nombres_directiva}} {{$key->apellidos_directiva}}</td>
                        <td><img src="/junta/{{$key->foto_directiva}}" alt="..." width="80px" style="width: 80px; border-radius: 50px;border:1px solid #b9b9b9;"></td>
                        <td>{{$key->cargo_directiva}}</td>
                        <td>{{$key->profesion}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editarjunta', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                         <button type="button" class="btn btn-danger">
                              <a href="{{url('eliminar_junta', $key->id)}}" onclick="return confirm('Deseas Eliminar? ')" style="color: #fff"><i class="fa fa-trash" title="Eliminar"></i></a>
                            </button>
                        </td>
                      </tr>
          @endforeach                   
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
@endsection