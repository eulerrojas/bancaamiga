@extends('layouts.intranet')

@section('content')
<!-- primera seccion imagen principal -->

  <div id="about1" class="about1-area area-padding">
        <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">
       @if(!empty($est)) 
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>{{$est->titulo_estructura}}</h2>
          </div>
           <div class="single-well">
              <p style="text-align: center;font-size:16px ">
               {{$est->subtitulo_estructura}}
              </p>  
            </div>
        </div>
      </div>
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center; margin-bottom: 50px">
       
           
				<img class="estructura" src="/estructura-organizativa/{{$est->image_estructura}}" alt="">
			
            
      
        </div>
        <!-- End col-->
      </div>
      @else
      <div class="row">
      
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-warning"><strong>Observación!</strong> No hay Información de esta sección en la Base de Datos</div>
      </div>

    </div>

    @endif 
    </div>
  </div>
@endsection