@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar Misión de la Empresa</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/ba-admin')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar 
                </a>
              </li>
            
            </ol>
          </div>   
     <br>
      <br>
      <br>  
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarmision')}}" enctype="multipart/form-data">
	<input type="hidden" name="id" value="{{$mision->id}}"> 
      {{ csrf_field() }} 
							    <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Imagen</label>
                
                <div class="col-sm-5">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 70px; height: 60px;" data-trigger="fileinput">
                      <img src="/agregados/{{$mision->image_mision}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                    <div>
                      <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Seleccionar imagen</span>
                        <span class="fileinput-exists">Cambio</span>
                        <input type="file" name="image_mision" accept="image/*" value="{{$mision->image_mision}}">
                      </span>
                      <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Retirar</a>
                    </div>
                    <span style="font-size: 10px; color: red"><strong>Atención!</strong> La Imágen debe tener un tamaño de <strong>70px ancho</strong> y<strong> 60px alto</strong></span>
                  </div>
                </div>
              </div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Título Principal</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="titulo_mision" required="required" value="{{$mision->titulo_mision}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Contenido</label>
								
								<div class="col-sm-8">
												<textarea class="form-control ckeditor" name="contenido_mision" required="required">{{$mision->contenido_mision}}</textarea>
								</div>
							</div>
							
						
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/ba-admin')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
@endsection