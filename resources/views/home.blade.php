@extends('layouts.inicio')

@section('content')
 <!--==========================
    PRIMERA PARTE SLIDER DE NOTICIAS BANCAMIGA
  ============================-->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
   <!-- Indicators -->
    <ol class="carousel-indicators">
      @foreach( $home as $key )
         <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
       @endforeach
    </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    @foreach( $home as $key )
                     <div class="item {{ $loop->first ? ' active' : '' }}" >
                            <img src="noticia/{{$key->image }}" alt="">
                   <div class="carousel-caption">
             <div class="row">

  <div class="col-xs-6 col-md-6"> <a class="logo" href="{{asset('/home')}}" title="Home"><img alt="Bancamiga" src="{{ asset('img/logo-transparente-blanco.png') }}" width="300px" class="movil-logo" ></a></div>
  <div class="col-xs-6 col-md-6 loginamiga">
    <div class="dropdown">
  <button class="btn btn-dafault dropdown-toggle" type="button" data-toggle="dropdown"><i class="entypo-user" style="color: #035786!important"></i><span style="color: #333!important; font-weight: 600"> {{Auth::user()->name}}</span>
  <span class="caret"></span></button>
  <ul class="dropdown-menu movil-drop" style="margin-left: 110px;">
    <li><a href="{{asset('ba-admin')}}"><i class="fa fa-dashboard"></i> Administrar Web</a></li>
    <li><a href="#">
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="fa fa-plug">Desconectar</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                          </form>
    </li>
  </ul>
</div>
  </div>
</div>
      <a href="{{url('blog_noticias',$key->id)}}">      <div class="row">
  <div class="col-xs-6 col-md-6"><h1 class="titulo_principal">{{$key->titulo }}</h1>
  <h4 class="subtitulo_principal">{{$key->subtitulo }}</h4>
  </div>
  <div class="col-xs-6 col-md-6"></div>
</div></a>
          </div>
                        </div>
    
    @endforeach
  </div>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
 <!--==========================
    CULMINACION DEL 1ER BLOQUE SLIDER
  ============================--> 
    <nav class="navbar navbar-default navbar-fixed-button" style="background-color: #f3f3f3 !important; border-color: #fff !important;border-bottom: 1px solid #f3f3f3 !important; font-size: 13px">
   <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    
    <div class="collapse navbar-collapse js-navbar-collapse">
     <ul class="nav navbar-nav navbar-left" style="color: #005694;">   
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">La Organización<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" style="font-weight: 700 !important">
            <li><a href="{{asset('/historia')}}">Historia</a></li>
            <li><a href="{{asset('/junta-directiva')}}">Junta Directiva</a></li>
            <li><a href="{{asset('/estructuras-organizativas')}}">Estructura Organizativa</a></li>
          </ul>
        </li>
          <li><a href="{{asset('/servicios-empleados')}}">Servicio al Empleado</a></li> 
      </ul>

        <ul class="nav navbar-nav navbar-left" style="color: #005694; font-weight: 700">   
        <li>
          <a href="#">Biblioteca de Documentos</a>
        </li>
      </ul>
        <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
           <ul class="user-info pull-left pull-right-xs pull-none-xsm nav navbar-nav navbar-right" style="margin-top: 10px">
    
          <!-- Raw Notifications -->
          <li class="notifications dropdown">
    
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
              <i class="fa fa-bell"></i>
              <span class="badge badge-danger">0</span>
            </a>
    
            <ul class="dropdown-menu">
              <li class="top">
                <p class="small">
                  Tienes <strong>0</strong> notificaciones.
                </p>
              </li>
              
              <li>
                <ul class="dropdown-menu-list scroller">
                  <li class="unread notification-info">
                    <a href="#">
                      <i class="fa fa-car pull-right"></i>
                      
                      <span class="line">
                        <strong>Solicitud Transporte</strong>
                      </span>
                      
                      <span class="line small">
                        30 mint
                      </span>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="external">
                <a href="#">Ver todas las Notificaciones</a>
              </li>
            </ul>
    
          </li>
        </ul>
       <ul class="nav navbar-nav navbar-right">
          <li><a href="{{asset('categoria-institucional')}}">Institucional</a></li>
          <li><a href="{{asset('categoria-administracion')}}">Administración</a></li>
          <li><a href="{{asset('categoria-rrhh')}}">Gestión Humana</a></li>
        </ul>
    </div><!-- /.nav-collapse -->
  </div>
  </nav>

   <!--==========================
      Servicios y Accesos
    ============================-->
    <section id="servi_amiga">
      <div class="container servi_container">

        <div class="row">
          @foreach($home1 as $key)
          <div class="col-md-6 col-lg-3 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <img src="solicitud/{{$key->image_solicitud }}" width="100px">
              <h4 class="title"><a href="">{{$key->titulo_solicitud}}</a></h4>
                <a href="{{$key->link}}" class="btn boton_amiga" target="blank">Solicitar <i class="fa fa-angle-right"></i></a>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section><!-- #servicios -->
     <!--==========================
    PARRALAX 3ERA SECCION
  ============================-->
  <div class="wellcome-area">
    <div class="well-bg">
      <div class="test-overly"></div>
      <div class="container servi_container">
    
   <div class="row">

        <div class="col-md-6 col-lg-4">
   <div class="card card-default" style="margin-top: 50px">
 <div class="colorear">
  <div class="row">
     <p style="color: #fff; font-weight: 700; font-size: 16px; text-align: center;margin-top: 70px; text-decoration: underline;">{{$home3->titulo_eventoc}}</p>
  </div>
  
 </div>
  <div class="card-body">
    <div class="row">
  <a href="{{url('ver-detalle-cumplemes',$home3->id)}}"><img class="img-cumple" src="/evento_cumpleanos/{{$home3->image_eventoc}}" width="300px" height="250px" style="margin-left: 10px"></a>
</div>

    <hr>
    <p class="listados"><a href="{{url('ver-detalle-cumplemes',$home3->id)}}" style="color: #5fa90e !important;">Ver Listados</a></p>
  </div>
</div>
        </div>

        <div class="col-md-6 col-lg-4">
         <div class="card card-default" style="margin-top: 50px">
  <div class="colorear1">
      <div class="row">
     <p style="color: #fff; font-weight: 700; font-size: 16px; text-align: center;margin-top: 70px; text-decoration: underline;">{{$home4->titulo_eventoi}}</p>
  </div>
  
  </div> 
  <div class="card-body">
        <div class="row">
<a href="{{url('ver-detalle-nuevoingreso',$home4->id)}}"><img class="img-ingreso" src="/evento_ingreso/{{$home4->image_eventoi}}" width="300px" height="250px" style="margin-left: 10px"></a>
</div>

    <hr>
    <p class="listados"><a href="{{url('ver-detalle-nuevoingreso',$home4->id)}}" style="color: #5fa90e !important;">Ver Listados</a></p>
  </div>
</div>
        </div>

        <div class="col-md-6 col-lg-4">
       <div class="card card-default" style="margin-top: 50px">
  <div class="colorear2">
      <div class="row">
     <p style="color: #fff; font-weight: 700; font-size: 16px; text-align: center;margin-top: 70px; text-decoration: underline;">Eventos</p>
  </div>
  
  </div> 
  <div class="card-body">

            <div class="row">

  <table class="table tablet-movil" style="width: 80%; margin-left: 30px">
  <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important">
                            <th style="font-size: 12px !important"></th>
                            <th style="font-size: 12px !important">Evento</th>
                            <th style="font-size: 12px !important">Fecha</th>
                        </tr>
                    </thead>
   <tbody>
      @foreach($home5 as $key)
     <tr>
       <td><i class="fa fa-calendar" style="float: right ;color: #666666"></i></td>
       <td>{{$key->titulo}}</td>
       <td>{{$key->fecha_inicio}}</td>
     </tr>
     @endforeach 
   </tbody>                 
  </table>
 <hr width="90%">
 
</div>
   
    <p class="listados"><a href="{{asset('ver-calendario')}}" style="color: #5fa90e !important;">Ver Listados</a></p>
  </div>
</div>
        </div>
      </div>

      </div>
    </div>
  </div>
   <!--==========================
    CULMINACION DEL PARALLAX
  ============================-->
    <!--==========================
     NOTICIAS
    ============================-->
    <section id="blog" class="section-bg padd-section wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h3 style="text-decoration: underline;">Tips de Seguridad</h3>
        </div>

       <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media">
  
        <div class="carousel-inner">
           @foreach($home2->chunk(3) as $count => $item)
          <div class="item {!! $count == 0 ? 'active' : '' !!}">
            <div class="row">
                  @foreach($item as $event)
              <div class="col-md-4">
                <div class="card card-blog">
            <div class="card-img">
              <a href="{{url('vermastips', $event->id)}}"><img src="tips/{{$event->image_tips}}" alt="" class="img-fluid"></a>
            </div>
            <div class="card-body">
              <div class="card-category-box">
                <div class="card-category">
                  <h6 class="category">{{$event->created_at}}</h6>
                </div>
              </div>
              <h3 class="card-title"><a href="{{url('vermastips', $event->id)}}" style="border-left: 2px solid color: #60a516">{{$event->titulo_tips}} / {{$event->nombre_ctips}}</a></h3>
              <p class="card-description">
                {{$event->extracto_tips}}
              </p>
            </div>
            <div class="card-footer">
              <div class="post-author">
                <a href="{{url('vermastips', $event->id)}}">
                  <span class="author" style="text-align: right; font-weight: 700; text-decoration: underline;">Leer Más</span>
                </a>
              </div>
            </div>
          </div>
              </div>  
              @endforeach 
            </div>
          </div>   
          @endforeach    
        </div>

       
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media" class="right carousel-control">›</a>
      </div>                          
    </div>
  </div>
      </div>
    </section><!-- #NOTICIAS -->

@endsection
