<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> Solicitud de Servicio de Mantenimiento</title>
    <link rel="stylesheet" href="css/pdf.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <div id="company" class="clearfix">
     <p><strong style="font-size: 12px">SOLICITUD DE SERVICIO DE MANTENIMIENTO (AGENCIAS)</strong><strong> <span style="font-size: 16px">SM-001</span></strong></p>
     <p style="margin-left: 280px"><strong>Fecha:</strong>12/01/1988</p>
      </div>
      <div id="project">
      <div id="logo">
        <img src="img/Bancamiga-BancoUniversal-EE.jpg">
      </div>
      </div>
    </header>
  <main>
      <table>
        <thead style="background-color: #266ea7; color: #fff">
            <tr>
        <th colspan="2">I. DATOS DE LA UNIDAD SOLICITANTE</th>
      </tr>     
        </thead>
        <tbody>
          <tr>
        <td width="10%" style="text-align: right;"><div><label style="color: #333; font-weight: 700; font-size: 12px; text-align: right;">AGENCIA :</label></div></td>    
        <td>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;">Agencia Bancamiga Altamira.</label></div>
        </td>
        </tr>
        </tbody>
      </table>
      <table>
         <thead style="background: #266ea7">
          <tr>
            <th colspan="4">II. DESCIPCIÓN DE LA SOLICITUD</th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td colspan="4" style="background: #dbd9d9">
            <div><label style="color: #333; font-weight: 700; font-size: 10px">ELECTRICIDAD / ILUMINACIÓN</label></div>
            </td>
          </tr>
              <tr style="background: #f5f5f591;">
            <td>
             <div><label style="color: #333; font-weight: 700; font-size: 9px">SERVICIO SOLICITADO</label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Ninguno</label></div>
            </td>
              <td width="50%">
              <div><label style="color: #333; font-weight: 700; font-size: 9px">DESCRIPCION DE LA AVERÍA</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Ningun Reporte de Esta Solicitud</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 9px">UNIDAD RESPONSABLE </label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Gestión Humana</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 9px">N° FECHA DE SOLUCIÓN</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">18/06/2019</label></div>
            </td>
             
          </tr>
              <tr>
            <td colspan="4" style="background: #dbd9d9">
            <div><label style="color: #333; font-weight: 700; font-size: 10px">INFRAESTRUCTURA</label></div>
            </td>
          </tr>
              <tr style="background: #f5f5f591;">
            <td>
             <div><label style="color: #333; font-weight: 700; font-size: 9px">SERVICIO SOLICITADO</label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Ninguno</label></div>
            </td>
              <td width="50%">
              <div><label style="color: #333; font-weight: 700; font-size: 9px">DESCRIPCION DE LA AVERÍA</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Ningun Reporte de Esta Solicitud</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 9px">UNIDAD RESPONSABLE </label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Gestión Humana</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 9px">N° FECHA DE SOLUCIÓN</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">18/06/2019</label></div>
            </td>
             
          </tr>
          
        </tbody>
      </table>
      <table>
        <thead style="background: #266ea7">
          <tr>
            <th colspan="1">III. OBSERVACIONES</th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Ninguno</label></div>
            </td>
          </tr>
        </tbody>
      </table>
      <table>
        <thead style="background: #266ea7">
          <tr>
            <th colspan="4">IV. FIRMAS</th>
             <tr style="background-color: #e8e6e6;">
              <td colspan="2" style=" text-align: center; color: #333; font-weight: 700; font-size: 10px">
                ELABORADO POR:
              </td>
              <td colspan="2" style=" text-align: center; color: #333; font-weight: 700; font-size: 10px">
                APROBADO POR:
              </td>
            </tr>
          </tr> 

        </thead>
           <tbody>
          <tr>
            <td>
              <div><label style="color: #333; font-weight: 700; font-size: 10px">APELLIDOS Y NOMBRES</label></div>
               <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Cecibel Rojas López</label></div>
               <br>
               <div><label style="color: #333; font-weight: 700; font-size: 10px">CARGO</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Programación</label></div>
            </td>
            <td  style="text-align: center">
              <br>
              <label>____________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA</label>
            </td>

              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 10px">APELLIDOS Y NOMBRES</label></div>
               <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Jose Rojas</label></div>
               <br>
               <div><label style="color: #333; font-weight: 700; font-size: 10px">CARGO</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Administración</label></div>
            </td>
            <td  style="text-align: center">
              <br>
              <label>____________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA</label>
            </td>
          </tr>
           
          
        </tbody>
      </table>
       <table>
        <thead style="background: #266ea7">
          <tr>
            <th colspan="4">V. USO EXCLUSIVO DE LA GERENCIA DE ADMINISTRACIÓN</th>
             <tr style="background-color: #e8e6e6;">
              <td colspan="2" style=" text-align: center; color: #333; font-weight: 700; font-size: 10px">
                PROCESADO POR (Coordinación de Servicios Generales):
              </td>
              <td colspan="2" style=" text-align: center; color: #333; font-weight: 700; font-size: 10px">
                AUTORIZADO POR (Gerente de Administración)
              </td>
            </tr>
          </tr> 

        </thead>
           <tbody>
          <tr>
            <td>
              <div><label style="color: #333; font-weight: 700; font-size: 10px">APELLIDOS Y NOMBRES</label></div>
               <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Cecibel Rojas López</label></div>
               <br>
               <div><label style="color: #333; font-weight: 700; font-size: 12px">CARGO</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Programación</label></div>
            </td>
            <td  style="text-align: center">
              <br>
              <label>____________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA</label>
            </td>

              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 10px">APELLIDOS Y NOMBRES</label></div>
               <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Jose Rojas</label></div>
               <br>
               <div><label style="color: #333; font-weight: 700; font-size: 12px">CARGO</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Administración</label></div>
            </td>
            <td  style="text-align: center">
              <br>
              <label>____________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA</label>
            </td>
          </tr>
           
          
        </tbody>
      </table>
      <div id="notices">
        <div><strong style="font-size: 0.9em;">FO.AD.001.V1.021</strong></div>
        <div class="notice">Solicitud de Servicio de Mantenimiento - Agencias</div>
      </div>
  </main>
    <footer>
      <span style="font-size: 8px">Todos los derechos reservados</span>
      <br>
      <span style="font-size: 8px">BANCAMIGA, Banco Microfinanciero, C.A.</span>
      <br>
      <span style="font-size: 8px">CATEGORÍA PRIVADA</span>
    </footer>
  </body>
</html>