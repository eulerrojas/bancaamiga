<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> Solicitud de Servicio de Transporte</title>
    <link rel="stylesheet" href="css/pdf.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <div id="company" class="clearfix">
     <p><strong style="font-size: 14px">SOLICITUD DE SERVICIO DE TRANSPORTE.</strong><strong> <span style="font-size: 16px">ST-00</span></strong></p>
     <p style="margin-left: 250px"><strong>Fecha:</strong>12/01/1988</p>
      </div>
      <div id="project">
      <div id="logo">
        <img src="img/Bancamiga-BancoUniversal-EE.jpg">
      </div>
      </div>
    </header>
  <main>
      <table>
        <thead style="background-color: #266ea7; color: #fff">
            <tr>
        <th colspan="3">I. DATOS DEL SOLICITANTE</th>
      </tr>     
        </thead>
        <tbody>
          <tr>
        <td colspan="2">
        <div><label style="color: #333; font-weight: 700; font-size: 12px">APELLIDOS Y NOMBRES DEL SOLICITANTE :</label></div>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;">cecibel</label></div>
        </td>
        <td>
        <div><label style="color: #333; font-weight: 700; font-size: 12px">CÉDULA DE IDENTIDAD :</label></div>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;"> v-18761015</label></div>
        </td>
        </tr>
         <tr style="background: #f5f5f591;">
        <td>
        <div><label style="color: #333; font-weight: 700; font-size: 12px">NRO. CELULAR:</label></div>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;">0416-832-70-86</label></div>
        </td>
        <td>
        <div><label style="color: #333; font-weight: 700; font-size: 12px">UNIDAD ADMINISTRATIVA DE ADSCRIPCIÓN:</label></div>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;">Informática</label></div>
        </td>
        <td>
        <div><label style="color: #333; font-weight: 700; font-size: 12px">CARGO:</label></div>
        <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700;">Programadora</label></div>
        </td>
      </tr>
        </tbody>
      </table>
      <table>
         <thead style="background: #266ea7">
          <tr>
            <th colspan="6">II. DATOS DEL SERVICIO</th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td colspan="6">
            <div><label style="color: #333; font-weight: 700; font-size: 12px">TIPO DE SERVICIO:</label></div><div class="fondo-input"></div>
            </td>
          </tr>
              <tr style="background: #f5f5f591;">
            <td>
             <div><label style="color: #333; font-weight: 700; font-size: 12px">FECHA:</label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">05-05-2019</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">HORA:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">02:00:00</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">N° PERSONAS:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">25</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">N° DE UNIDADES:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">25</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">IDA Y VUELTA:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">SI</label></div>
            </td>
              <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">T. DE ESPERA: </label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">24 Horas</label></div>
            </td>
          </tr>
           <tr>
            <td colspan="3">
              <div><label style="color: #333; font-weight: 700; font-size: 12px">LUGAR DE SALIDA:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Plaza la Candelaria, Caracas</label></div>
            </td>
            <td colspan="3">
              <div><label style="color: #333; font-weight: 700; font-size: 12px">DIRECCION DE DESTINO:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Plaza Altamira. Primera sede Bancamiga</label></div>
            </td>
          </tr>
        </tbody>
      </table>
      <table>
        <thead style="background: #266ea7">
          <tr>
            <th colspan="1">III. MOTIVO DE TRASLADO:</th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Entrgar Documentos a las Sedes</label></div>
            </td>
          </tr>
        </tbody>
      </table>
      <table>
        <thead style="background: #266ea7">
          <tr>
            <th colspan="3">IV. FIRMAS</th>
          </tr> 
        </thead>
           <tbody>
          <tr>
            <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">APELLIDOS Y NOMBRES DEL SOLICITANTE:</label></div>
               <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Cecibel Rojas López</label></div>
            </td>
            <td>
              <div><label style="color: #333; font-weight: 700; font-size: 12px">CÉDULA DE IDENTIDAD:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">v-18761015</label></div>
            </td>
            <td  style="text-align: center">
              <br>
              <label>____________________________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA DEL SOLICITANTE</label>
            </td>
          </tr>
            <tr>
            <td style="background: #f5f5f591">
              <div><label style="color: #333; font-weight: 700; font-size: 12px">APELLIDOS Y NOMBRES SUPERVISOR INMEDIATO</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Santiago Navas</label></div>
            </td>
            <td style="background: #f5f5f591">
              <div><label style="color: #333; font-weight: 700; font-size: 12px">CARGO:</label></div>
              <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">Director</label></div>
            </td>
            <td  style="text-align: center; background: #f5f5f591">
              <br>
              <label>____________________________________</label>
              <br>
              <label style="color: #333; font-weight: 700; font-size: 12px; text-align: center">FIRMA DEL SOLICITANTE</label>
            </td>
          </tr>
          
        </tbody>
      </table>
      <div id="notices">
        <div><strong style="font-size: 0.9em;">FO.AD.002.V1.0214</strong></div>
        <div class="notice">Solicitud de Servicio de Transporte</div>
      </div>
  </main>
      <footer>
      <span style="font-size: 8px">Todos los derechos reservados</span>
      <br>
      <span style="font-size: 8px">BANCAMIGA, Banco Microfinanciero, C.A.</span>
      <br>
      <span style="font-size: 8px">CATEGORÍA PRIVADA</span>
    </footer>
  </body>
</html>