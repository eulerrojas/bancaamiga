<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> Contrato Crediamigo</title>
    <link rel="stylesheet" href="css/pdf.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <div id="company" class="clearfix">
     <p><strong style="font-size: 14px">CONTRATO CREDIAMIGO</strong></p>
      </div>
      <div id="project">
      <div id="logo">
        <img src="img/Bancamiga-BancoUniversal-EE.jpg">
      </div>
      </div>
    </header>
  <main style="margin-top: -40px">
    <div>
      <p style="text-align: justify; line-height: 1.5">
        Entre <strong>BANCAMIGA BANCO UNIVERSAL, C.A.,</strong> inscrita originalmente como <strong>BANCAMIGA BANCO DE DESARROLLO, C.A.,</strong> por ante el Registro Mercantil Quinto de la Circunscripción Judicial del Distrito Capital y Estado Miranda, en fecha 08 de agosto de 2006, bajo el N° 52, Tomo 1387-A., posteriormente cambiada su denominación a <strong>BANCAMIGA BANCO MICROFINANCIERO, C.A.,</strong> según consta en el Acta de Asamblea General Extraordinaria de Accionistas, inscrita por ante el citado Registro Mercantil, en fecha 15 de noviembre de 2011, bajo el N° 25, Tomo 358-A., siendo modificados sus Estatutos Sociales en varias oportunidades, constando la última de ellas así como su transformación en <strong>BANCO UNIVERSAL,</strong> en el Acta de la Asamblea General Extraordinaria de Accionistas celebrada el 29 de septiembre de 2016, inscrita en el Registro Mercantil Quinto del Distrito Capital, en fecha 17 de noviembre de 2017, bajo el N° 18, Tomo 372-A REGISTRO MERCANTIL V (CÓD 224), según lo acordado por la Superintendencia de las Instituciones del Sector Bancario, en la Resolución N° 102.17 del 14 de septiembre de 2017, publicada en la Gaceta Oficial de la República Bolivariana de Venezuela N° 41.265 del 26 de octubre de 2017, identificada en el Registro Único de Información Fiscal (R.I.F) bajo el N° J-31628759-9, representada en este acto por su Presidente Ejecutivo ciudadano <strong>ALBERTO DE ARMAS BASTERRECHEA</strong> venezolano, mayor de edad, de estado civil soltero, domiciliado en la ciudad de Caracas, titular de la Cédula de Identidad N° <strong>V- 5.532.672</strong>, inscrito por ante el Registro de Información Fiscal <strong>(R.I.F)</strong> bajo el N° V-05532672-6, por una parte y por la otra, el trabajador de <strong>BANCAMIGA</strong> suficientemente identificado en el <strong>"FORMULARIO DE SOLICITUD DE CREDIAMIGO"</strong> anexa a este contrato, en lo adelante <strong>"EL TRABAJADOR PRESTATARIO"</strong>, conjuntamente denominadas <strong>"LAS PARTES"</strong>; se conviene en suscribir el presente documento de crédito personal, que se regirá por las siguientes cláusulas:
      </p>
      <p style="text-align: justify; line-height: 1.5">
          i) Considerando que los numerales 15 y 16 del artículo 97 de la Ley de las Instituciones del Sector Bancario vigente autoriza por vía de excepción a los Bancos a conceder directa o indirectamente créditos hipotecarios, créditos producto de programas generales para cubrir necesidades razonables  y créditos personales garantizados con sus prestaciones sociales a sus empleados en los términos allí expuestos;
      </p>
      <p style="text-align: justify; line-height: 1.5">
          ii) Considerando que para otorgar este beneficio, BANCAMIGA diseñó "CREDIAMIGO", que establece las bases para el otorgamiento del presente Préstamo personal;
      </p>
      <p style="text-align: justify; line-height: 1.5">
          iii) Considerando que el TRABAJADOR PRESTATARIO goza de una antiguedad en la empresa superior  a un (1) año, lo cual lo hace beneficiario de CREDIAMIGO, en los términos particulares de su contrato de trabajo:
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>PRIMERA :</u> Monto del Préstamo.</strong> 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong>BANCAMIGA</strong> otorga a <strong>EL TRABAJADOR PRESTATARIO</strong> un préstamo personal garantizado con sus prestaciones sociales; y éste así lo acepta, por la cantidad en Bolivares: 
      </p>
      <table>
        <tbody>
          <tr>
            <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>MONTO APROBADO</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">00,0</label></div>
           </td>
           <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>BOLÍVARES CON</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">00.0</label></div>
           </td>
           <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>CÉNTIMOS</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;"><strong>(Bs.00)</strong></label></div>
           </td>
          </tr>
        </tbody>
      </table>
      <p style="text-align: justify; line-height: 1.5">
        por concepto de capital.
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>SEGUNDA :</u> Forma de Pago.</strong> 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong>EL TRABAJADOR PRESTATARIO</strong> se obliga en este acto a devolver a <strong>BANCAMIGA</strong> el <strong>Monto del Préstamo</strong>, en el plazo fijo de dieciocho (18) meses, contado a partir de la fecha de la liquidación del <strong>Monto del Préstamo</strong>, mediante el pago de dieciocho (18) cuotas mensuales y consecutivas, inicialmente calculadas de manera referencial en la cantidad de Bolívares:  
      </p>
          <table>
        <tbody>
          <tr>
            <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>MONTO APROBADO</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">00,0</label></div>
           </td>
           <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>BOLÍVARES CON</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;">00.0</label></div>
           </td>
           <td>
            <div><label style="color: #333; font-weight: 700; font-size: 9px"><strong>CÉNTIMOS</strong></label></div>
             <div class="fondo-input"><label class="form-control" style="color: #333; font-weight: 700; background: #eff1e1 !important;"><strong>(Bs.00)</strong></label></div>
           </td>
          </tr>
        </tbody>
      </table>
      <p style="text-align: justify; line-height: 1.5">
        cada una, contentivas de amortización a capital e intereses convencionales, todo lo cual se evidencia en la <strong>"Tabla de Amortización"</strong>
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>TERCERA :</u> Tasa de Interés.</strong> 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        El <strong>Monto del Préstamo</strong> devengará intereses convencionales sobre saldos deudores a partir de la fecha de su liquidación, calculados inicialmente a la tasa activa del doce coma cinco por ciento (12.5%) anual, la cual estará sujeta a revisión por parte de la Juanta Dirctiva o el Comité que al efecto se designe por parte de la Junta Directiva, Dichos intereses serán pagados mensualmente, en forma conjunta con la cuota de capital, tal como se indicó en la cláusula anterior. 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>CUARTA :</u> Deducción de las Cuotas de Préstamo.</strong> 
      </p>
       <p style="text-align: justify; line-height: 1.5">
        El <strong>TRABAJADOR PRESTATARIO</strong> autoriza irrevocablemente a <strong>BANCAMIGA</strong> a que deduzca del monto que se deposita en su cuenta de nómina de <strong>BANCAMIGA</strong> por concepto de salario mensual, el equivalente a la <strong>"Cuota del Préstamo"</strong> (Capital e Intereses), hasta el pago total.   
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>QUINTA :</u> Obligación de pago del saldo adeudado en caso de cese de la relación laboral.</strong> 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        En caso de cese de la relación laboral, <strong>TRABAJADOR PRESTATARIO</strong> se obliga a pagar el cincuenta por ciento (50%) de la totalidad de las cuotas adeudadas a razón  del presente <strong>CONTRATO DE PRESTAMO</strong>, independientemente de la naturaleza de la causa que dio origen a dicho cese, Para ello, autoriza irrevocablemente a <strong>BANCAMIGA</strong> a deducir del monto de la liquidación que le corresponde por concepto de prestaciones sociales, dicho porcentaje del saldo deudor.
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>SEXTA :</u> Aceptación del Contrato.</strong> 
      </p>
       <p style="text-align: justify; line-height: 1.5">
        <strong>LAS PARTES</strong> declaran que conocen suficientemente los términos del presente contrato y las bases de <strong>CREDIAMIGO</strong> y en razón de tal, lo aceptan en todas sus partes.
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong><u>SÉPTIMA :</u> Jurisdicción.</strong> 
      </p>
      <p style="text-align: justify; line-height: 1.5">
        Para todos los efectos, derivadas y consecuencias de esta negociación elegimos como domicilio especial la ciudad de Caracas, a la jurisdicción  de cuyos Tribunales, nos sometemos sin perjuicio para <strong>BANCAMIGA</strong> de que pueda ocurrir por ante cualquier otro Tribunal de la República Bolivariana de Venezuela, cuando asi lo considere conveniente de conformidad con la ley.
      </p>
        <p style="text-align: justify; line-height: 1.5">
        En Caracas a los 18 días del mes de Julio de 2019.
      </p>

      <table>
        <tbody>
          <tr style="text-align: center">
            <td  style="text-align: center">
            <div>___________________________________________________</div>
              <div><strong>EL TRABAJADOR PRESTATARIO</strong> </div>
           </td>
           <td  style="text-align: center">
             <div>___________________________________________________</div>
             <div>Por <strong>BANCAMIGA BANCO UNIVERSAL, C.A.</strong> </div>
           </td>
          </tr>
        </tbody>
      </table>
    </div>
    </p>
        <p style="text-align: justify; line-height: 1.5">
        <strong>Nombres y Apellidos: </strong>
      </p>
      <p style="text-align: justify; line-height: 1.5">
        <strong>Cédula de Identidad: </strong>
      </p>
  </main>
    
  </body>
</html>