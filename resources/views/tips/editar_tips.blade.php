@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar tips</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/ver-tips')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar Tips
                </a>
              </li>
            
              <li><a href="{{asset('categoria_tips')}}">
              <i class="fa fa-folder-open"></i>
              Crear Categoría</a></li>
            </ol>
          </div>   
     <br>
      <br>
      <br>  

@if ($errors->any())
<div class="col-lg-12 col-md-12 col-xs-12">
    <div class="alert alert-danger content">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
 </div>   
@endif
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizartipsbancario')}}" enctype="multipart/form-data">
	     <input type="hidden" name="id" value="{{$tips1->id}}"> 
      {{ csrf_field() }} 
							    <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Imagen</label>
                
                <div class="col-sm-5">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                      <img src="/tips/{{$tips1->image_tips}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                    <div>
                      <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Seleccionar imagen</span>
                        <span class="fileinput-exists">Cambio</span>
                        <input type="file" name="image_tips" accept="image/*" value="{{$tips1->image_tips}}">
                      </span>
                      <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Retirar</a>
                    </div>
                    <span style="font-size: 10px; color: red"><strong>Atención!</strong> La Imágen debe tener un tamaño de <strong>720px ancho</strong> y<strong> 440px alto</strong></span>
                  </div>
                </div>
              </div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Título Principal</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="titulo_tips" required="required" value="{{$tips1->titulo_tips}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Extracto</label>
								
								<div class="col-sm-8">
									<textarea class="form-control" id="field-ta" name="extracto_tips" maxlength="150" required="required">{{$tips1->extracto_tips}}</textarea>
								<span style="font-size: 10px; color: red"><strong>Atención!</strong> Extracto corto de<strong>100 letras</strong></span>
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Contenido del tips</label>
								
								<div class="col-sm-8">
											<textarea class="form-control ckeditor" name="contenido_tips">{{$tips1->contenido_tips}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Categoría</label>
								
								<div class="col-sm-8">
									<select name="id_catips" class="select2" data-allow-clear="true" data-placeholder="Seleccione Categoría..." required="required">
										<option></option>
										@php
                                        $categoria = App\Categoria_tips::all();
                                        @endphp
										<optgroup label="Categorías">
										@foreach($categoria as $key)
										<option value="{{$key->id}}">{{($key->nombre_ctips)}}</option>
										@endforeach
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/ba-admin')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
@endsection