@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar Categoría</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/ba-admin')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar Noticias
                </a>
              </li>
            
              <li><a href="{{asset('/nueva-noticias')}}">
              <i class="fa fa-folder-open"></i>
              Crear Noticia</a></li>
            </ol>
          </div>   
     <br>
      <br>
      <br> 

<div class="row">
  <div class="col-md-12">
   <div class="col-md-6"> 
  <form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarctips')}}">
     <input type="hidden" name="id" value="{{$tips->id}}"> 
      {{ csrf_field() }} 
                   <div class="panel-body">
                          <div class="form-group">
                             <label>Nombre Categoría :</label>
         <input type="text" class="form-control"  name="nombre_ctips" id="nombre" required autofocus value="{{$tips->nombre_ctips}}">
                          </div>
                   <div class="form-group">
                            <label>Descripción :</label>      
                            <textarea class="form-control" id="field-ta" name="descripcion_ctips" required="required">{{$tips->descripcion_ctips}}</textarea>
                            
                    </div>
                            <div class="form-group">
        <div class="col-sm-offset-8 col-sm-2">

        <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
              </div>   
                        </div>
                  </form>
                    
                </div>
        <div class="col-md-6">
          <div class="panel-body">
            <div class="form-group">
      <div class="col-md-12">
                            <form role="form" class="form-horizontal form-groups-bordered" action="{{url('categoria_noticia')}}" method="GET" >
                              
                        
                         <div class="input-group">
                             <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Buscar Categoría">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
                        </div>
                        <br>
                  </div>
                  <br>
       @if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Mensaje!</strong> {{Session::get('msj')}}
    </div>
         </div>
        @endif
        @if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Alerta!</strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif
        <table class="table table-bordered responsive">
          <thead>
            <caption style="background-color: #5686a5; color: #fff; text-align: center;">Listado de Categorías</caption>
            <tr>
                <th>Cod</th>
                <th>Categoría</th>
                <th>Descripción</th>
                <th>Acciones</th></tr>
          </thead>
          <tbody>
          @php
          $tips = App\Categoria_tips::all();
          @endphp
          @foreach($tips as $key)
                  <tr>
                    <td>{{($key->id)}}</td>   
                    <td>{{($key->nombre_ctips)}}</td> 
                    <td>{{($key->descripcion_ctips)}}</td>
                    <td width="25%">
                    <a href="#" class="btn btn-default"> <i class="fa fa-pencil"></i></a>
                    <a href="" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
                    </td>
                 </tr>
            @endforeach                                   
          </tbody>
        </table>
     
          </div>
        </div>          
</div>
</div>
@endsection