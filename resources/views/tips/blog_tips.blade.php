@extends('layouts.intranet')

@section('content')
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
         
          	<li>Filtro: tips Bancario</li>
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>

<div class="container">
    <div class="well"> 
        <div class="row">
             <div class="col-md-12">
                 <div class="row hidden-md hidden-lg"><h1 class="text-center" >{{$valor_t->titulo_tips}}</h1></div>   
                 <div class="pull-left col-md-4 col-xs-12 thumb-contenido"><img class="center-block img-responsive" src='/tips/{{$tips->image_tips}}' /></div>
                 <div class="">
                     <h1  class="hidden-xs hidden-sm">{{$tips->titulo_tips}}</h1>
                     <hr>
                     <small>Fecha: <strong>{{$tips->created_at}}</strong></small><br>
                     <small>Categoría: <strong>{{$valor_t->nombre_ctips}}</strong></small>
                     <hr>
                     <p class="text-justify">{!! html_entity_decode($tips->contenido_tips) !!}</p></div>
             </div>
        </div>
    </div>
</div>


@endsection