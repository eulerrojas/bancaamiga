@extends('layouts.intranet')

@section('content')
<!-- primera seccion imagen principal -->

  <div id="about1" class="about1-area area-padding">
        <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">
  
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Calendario de Eventos</h2>
          </div>
           
        </div>
      </div>
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center; margin-bottom: 50px">
       
           
      <div id="calendar"></div>
            
      
        </div>
        <!-- End col-->
      </div>
 
    </div>
  </div>
@endsection