@extends('layouts.intranet')

@section('content')
<!-- primera seccion imagen principal -->

  <div id="about" class="about-area area-padding">
        <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">
       @if(!empty($cumplemes)) 
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>{{$cumplemes->titulo_eventoc}}</h2>
          </div>
          <div class="single-well">
              <p style="text-align: center;font-size:16px ">
              Eventos Mensuales
              </p>  
            </div>
        </div>
      </div>
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center; margin-bottom: 50px; margin-top: 50px">
       
           
				<img class="detalle_cumplemes" src="/evento_cumpleanos/{{$cumplemes->image_eventoc}}" alt="" width="900">
			
            
      
        </div>
        <!-- End col-->
      </div>
      @else
      <div class="row">
      
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-warning"><strong>Observación!</strong> No hay Información de esta sección en la Base de Datos</div>
      </div>

    </div>

    @endif 
    </div>
  </div>
@endsection