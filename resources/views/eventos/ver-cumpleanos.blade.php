@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Eventos Cumpleaños Actuales</h3>
     </div>
      <br>
      <br>
      <br>  

     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
    
           <div class="panel-body">
 @if(!empty($cumple)) 
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                            <th style="font-size: 12px !important">Título General</th>
                            <th style="font-size: 12px !important">Galería Cumpleaños</th>
                            <th style="font-size: 12px !important">Fecha de Actualización</th>
                            <th style="font-size: 12px !important">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      @foreach($cumple as $key)
             
                      <tr> 
                        <td>{{$key->titulo_eventoc}}</td>
                        <td><img src="/evento_cumpleanos/{{$key->image_eventoc}}" alt="..." width="100px"></td>
                        <td>{{$key->updated_at}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editarcumplemes', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                        </td>
                      </tr>
       @endforeach                                
                    </tbody>
            </table>
         
        </div>
         @else
      <div class="row">
      
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-warning"><strong>Observación!</strong> No hay Información de esta sección en la Base de Datos</div>
      </div>

    </div>

    @endif 
    </div>
@endsection