@extends('layouts.googlecalendar')

@section('content')


<div id="calendar"></div>

<form class="" action="{{url('registraevento')}}" method="post">
	{{ csrf_field() }}
<!-- Modal -->


	<div class="modal fade" id="mdlEvent">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><strong style="color: #000; font-weight: 700"><i class="fa fa-calendar"></i> Crear Evento</strong></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-1" class="control-label">Titulo :</label>
								
								<input type="text" class="form-control" name="titulo" id="titulo" placeholder="Ingrese titulo">
							</div>	
							
						</div>
						
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Fecha de inicio :</label>
								
								<input type="text" name="fecha_inicio" id="fecha_inicio" readonly class="form-control">
							</div>	
						
						</div>
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Hora de inicio:</label>
								
								<input type="time" name="hora_inicio" id="hora_inicio"  class="form-control" >
							</div>	
						
						</div>
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Fecha Final :</label>
								
								<input type="date" name="fecha_final" id="fecha_final"  class="form-control" >
							</div>	
						
						</div>
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Hora Final:</label>
								
								<input type="time" name="hora_final" id="hora_final"  class="form-control" >
							</div>	
						
						</div>
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Seleccione El color del Evento:</label>
								
								<select class="form-control" name="color">
										<option>Seleccione </option>
										<option class="rojo" style="background-color: #FF0000;">#FF0000;</option>
                                        <option class="azul" style="background-color: #0066FF;">#0066FF;</option>
                                        <option class="blanco" style="background-color: #FFFFFF;">#FFFFFF;</option>
                                        <option class="negro" style="background-color: #000;">#000</option>
                                        <option class="morado1" style="background-color: #4C0F71;">#4C0F71;</option>
                                        <option class="verde" style="background-color: #650AF9;">#650AF9;</option>
                                        <option class="verde" style="background-color: #989D9D;">#989D9D;</option>
                                        <option class="verde" style="background-color: #0A18F9;">#0A18F9;</option>
                                        <option class="verde" style="background-color: #052504;">#052504;</option>
                                        <option class="verde" style="background-color: #95F805;">#95F805;</option>
                                        <option class="verde" style="background-color: #FAD400;">#FAD400;</option>
                                        <option class="verde" style="background-color: #FC07D0;">#FC07D0;</option>
									</select>
							</div>	
						
						</div>
					</div>
				
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar Evento</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection