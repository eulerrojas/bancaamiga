@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Usuarios Activos</h3>
     </div>
      <br>
      <br>
      <br>  
            <div class="col-md-12">
   <div class="col-md-3"></div>
        <div class="col-md-3"></div>
         <div class="col-md-6">
            <div class="form-group">
       <form role="search" class="form-horizontal form-groups-bordered" action="{{url('ba-admin')}}" method="GET" >
                                    
                        
                         <div class="input-group">

                             <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Teclee para ver sugerencias...">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
 
        </div>
        </div>
 </div> 
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
    
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                            <th style="font-size: 12px !important;text-align: center">nombre</th>
                            <th style="font-size: 12px !important;text-align: center">Correo Electrónico</th>
                            <th style="font-size: 12px !important;text-align: center">Fecha de Creación</th>
                            <th style="font-size: 12px !important;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
      
     @foreach($user as $key)
                      <tr> 
                        <td style="text-align: center">{{$key->name}}</td>
                        <td style="text-align: center">{{$key->email}}</td>
                        <td style="text-align: center">{{$key->created_at}}</td>
                        <td style="text-align: center">
                            <button type="button" class="btn btn-default">
                              <a href="" onclick="return confirm('Deseas suspender el Usuario? ')" ><i class="fa fa-eye-slash" title="suspender"></i></a>
                            </button>
                        </td>
                      </tr>
                          @endforeach                         
                    </tbody>
            </table>

         <div style="float: right;"></div> 
        </div>
        
    </div>
@endsection