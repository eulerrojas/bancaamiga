@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Ficha de Empleado</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/lista_usuarios')}}">
                  <i class="fa fa-reorder"></i>
                 Lista de Usuarios
                </a>
              </li>
            
            </ol>
          </div>   
     <br>
      <br>
      <br> 
<div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
<ul>
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul> 
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('registrar_ficha')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
							<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Nombres</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="nombres" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Apellidos</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="apellidos" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Cédula de Identidad</label>
								<div class="row">
  									<div class="col-md-1">
  										<select class="form-control" name="cod_cedula">
										<option>--</option>
										@php
                                        $cod_c = App\codigo_cedula::all();
                                        @endphp
                                        @foreach($cod_c as $key)
										<option value="{{$key->id}}">{{($key->nombre_valor)}}</option>
										@endforeach
									</select>
  									</div>
  									<div class="col-md-4">
  										<input type="text" class="form-control" id="field-1" name="cedula" required="required">
  									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Teléfono Habitación</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="telefono_hab" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Teléfono Celular</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="celular" required="required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Unidad Administrativa</label>
								
								<div class="col-sm-8">
									<select name="id_departamento" class="select2" data-allow-clear="true" data-placeholder="Seleccione Departamento..." required="required">
										<option></option>
										@php
                                        $departamento = App\Departamento::all();
                                        @endphp
										<optgroup label="Departamentos">
										@foreach($departamento as $key)
										<option value="{{$key->id}}">{{($key->descripcion)}}</option>
										@endforeach
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Cargo</label>
								
								<div class="col-sm-8">
									<select name="id_rol" class="select2" data-allow-clear="true" data-placeholder="Seleccione Cargo..." required="required">
										<option></option>
										@php
                                        $rol = App\Roles::all();
                                        @endphp
										<optgroup label="Cargo">
										@foreach($rol as $key)
										<option value="{{$key->id}}">{{($key->nombre_roles)}}</option>
										@endforeach
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/lista_usuarios')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
@endsection