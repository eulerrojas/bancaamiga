<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="" />

    <link rel="icon" href="{{asset('img/favicon.png')}}">

    <title>Bancamiga | Intranet</title>

    <link rel="stylesheet" href="{{asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
            <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        </script>
</head>
<body class="page-body login-page login-form-fall">


        @yield('content')
  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
            <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/twitter2.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                      <li>
                      <a href="#"><img src="{{asset('img/rrss/instagram.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/facebook.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/youtube.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/likenlin.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                  </ul>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Bancamiga</strong>.Todos los derechos reservados - RIF J-31628759-9
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- Footer -->
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>  
<script type="text/javascript">
    $(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});
     // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  }); 

</script>

    <!-- Bottom scripts (common) -->
    <script src="{{asset('js/gsap/TweenMax.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/joinable.js')}}"></script>
    <script src="{{asset('js/resizeable.js')}}"></script>
    <script src="{{asset('js/neon-api.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/neon-login.js')}}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{asset('js/neon-custom.js')}}"></script>


    <!-- Demo Settings -->
    <script src="{{asset('js/neon-demo.js')}}"></script>

</body>
</html>