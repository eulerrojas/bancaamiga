<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="" />

    <link rel="icon" href="{{asset('/img/favicon.png')}}">

    <title>Bancamiga | Intranet</title>

    <link rel="stylesheet" href="{{asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('css/bancamiga.css')}}">
    <link rel="stylesheet" href="{{asset('css/white.css')}}">
    <link rel="stylesheet" href="{{asset('js/select2/select2-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('js/select2/select2.css')}}">
    <link rel="stylesheet" href="{{asset('js/selectboxit/jquery.selectBoxIt.css')}}">
    <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
            <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        </script>
</head>
<body class="page-body skin-white">

   <section class="hero" id="intro">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-right navicon">
          <a id="nav-toggle" class="nav_slide_button" href="#"><span></span></a>
        </div>
      </div>
    <div class="row">
       <div class="col-xs-4 col-md-4">
         <a href="{{asset('/home')}}" title="Home"><img class="logo_banner" alt="Bancamiga" src="{{ asset('img/logo-transparente-blanco.png') }}"></a>
       </div>
       <div class="col-xs-4 col-md-4">
         
       </div>
       <div class="col-xs-4 col-md-4 loginamiga_banner">
           <div class="dropdown" style="margin-left: 150px;">
  <button class="btn btn-dafault dropdown-toggle" type="button" data-toggle="dropdown"><i class="entypo-user" style="color: #035786!important"></i><span style="color: #333!important; font-weight: 600"> {{Auth::user()->name}}</span>
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="{{asset('ba-admin')}}"><i class="fa fa-dashboard"></i> Administrar Web</a></li>
    <li><a href="#">
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="fa fa-plug">Desconectar</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                          </form>
    </li>
  </ul>
</div>
       </div>
      </div>
    </div>
  </section>
    <nav class="navbar navbar-default navbar-fixed-button" style="background-color: #f3f3f3 !important; border-color: #fff !important;border-bottom: 1px solid #f3f3f3 !important; font-size: 12px">
   <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    
    <div class="collapse navbar-collapse js-navbar-collapse">
     <ul class="nav navbar-nav navbar-left" style="color: #005694;">   
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">La Organización<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" style="font-weight: 700 !important">
            <li><a href="{{asset('/historia')}}">Historia</a></li>
            <li><a href="{{asset('/junta-directiva')}}">Junta Directiva</a></li>
            <li><a href="{{asset('/estructuras-organizativas')}}">Estructura Organizativa</a></li>
          </ul>
        </li>
          <li><a href="{{asset('/servicios-empleados')}}">Servicio al Empleado</a></li> 
      </ul>

        <ul class="nav navbar-nav navbar-left" style="color: #005694; font-weight: 700">   
        <li>
          <a href="#">Biblioteca de Documentos</a>
        </li>
      </ul>
        <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
           <ul class="user-info pull-left pull-right-xs pull-none-xsm nav navbar-nav navbar-right" style="margin-top: 10px">
    
          <!-- Raw Notifications -->
          <li class="notifications dropdown">
    
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
              <i class="fa fa-bell"></i>
              <span class="badge badge-danger">0</span>
            </a>
    
            <ul class="dropdown-menu">
              <li class="top">
                <p class="small">
                  Tienes <strong>0</strong> notificaciones.
                </p>
              </li>
              
              <li>
                <ul class="dropdown-menu-list scroller">
                  <li class="unread notification-info">
                    <a href="#">
                      <i class="fa fa-car pull-right"></i>
                      
                      <span class="line">
                        <strong>Solicitud Transporte</strong>
                      </span>
                      
                      <span class="line small">
                        30 mint
                      </span>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="external">
                <a href="#">Ver todas las Notificaciones</a>
              </li>
            </ul>
    
          </li>
        </ul>
     
    </div><!-- /.nav-collapse -->
  </div>
  </nav>
<div class="page-container">
  
  <div class="sidebar-menu">

    <div class="sidebar-menu-inner">
      
      <header class="logo-env">

        <!-- logo -->
        <div class="logo">
        
        <span style="font-weight: 700; font-size: 18px; color: #045392; float: left;"><i class="fa fa-circle"></i> Servicios al Empleado</span>

                </div>

        <div class="sidebar-mobile-menu visible-xs">
          <a href="#" class="with-animation">
            <i class="entypo-menu"></i>
          </a>
        </div>

      </header>
      
                  
      <ul id="main-menu" class="main-menu">
        <li class="has-sub">
          <a href="#">
            <i class="fa fa-circle" style="color: #7eb63e;"></i>
            <span class="title" style="font-weight: 700">Solicitudes Gestión Humana</span>
          </a>
          <ul class="">
            <li>
              <a href="{{url('/workflow-crediamigo')}}">
                <i class="fa fa-circle"></i>
                <span class="title">Crediamigo</span>
                <span class="badge badge-success badge-roundless" style="background-color: #7eb63e">WF</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/uniformes',$data1->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Financiamientos Unif. Escolares</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/file',$data->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Guarderia</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/prestaciones',$data2->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Prestaciones Sociales</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/hcm',$data3->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Servicio HCM</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/vacaciones',$data4->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Vacaciones</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/recorrido-trabajador',$data5->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Recorrido Habitual del Trabajador</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="has-sub">
          <a href="#">
            <i class="fa fa-circle" style="color: #7eb63e;"></i>
            <span class="title" style="font-weight: 700">Solicitudes Administración</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/workflow-viaticos')}}">
                <i class="fa fa-circle"></i>
                <span class="title">Viaticos</span>
                <span class="badge badge-success badge-roundless" style="background-color: #7eb63e">WF</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/sede-principal',$data6->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Material Sede Principal</span>
              </a>
            </li>
            <li>
              <a href="{{url('/workflow-material-proveeduria')}}">
                <i class="fa fa-circle"></i>
                <span class="title">Material Proveeduría</span>
                <span class="badge badge-success badge-roundless" style="background-color: #7eb63e">WF</span>
              </a>
            </li>
            <li>
              <a href="{{url('/uploads/mantenimiento',$data7->file_doc)}}" target="_blank">
                <i class="fa fa-circle"></i>
                <span class="title">Mantenimiento</span>
              </a>
            </li>
            <li>
              <!-- <a href="{{url('/workflow-transporte', Auth::user()->id)}}"> -->
              <a href="{{url('/workflow-transporte')}}">
                <i class="fa fa-circle"></i>
                <span class="title">Transporte</span>
                <span class="badge badge-success badge-roundless" style="background-color: #7eb63e">WF</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="has-sub">
          <a href="#">
            <i class="fa fa-circle" style="color: #7eb63e;"></i>
            <span class="title" style="font-weight: 700">Hemeroteca de UPCLC/FT</span>
          </a>
          <ul>
            <li>
              <a href="#">
                <i class="fa fa-circle"></i>
                <span class="title">Archivos</span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
      
    </div>

  </div>

  <div class="main-content">

    
       @yield('content')
  
  </div>

</div>

     
  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
            <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/twitter2.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                      <li>
                      <a href="#"><img src="{{asset('img/rrss/instagram.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/facebook.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/youtube.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/likenlin.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                  </ul>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Bancamiga</strong>.Todos los derechos reservados - RIF J-31628759-9
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".content").fadeOut(500);
    },250);
});
</script>
  <script src="{{asset('js/gsap/TweenMax.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/joinable.js')}}"></script>
    <script src="{{asset('js/resizeable.js')}}"></script>
    <script src="{{asset('js/neon-api.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/neon-login.js')}}"></script>
    <script src="{{asset('js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>
    <script src="{{asset('js/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <!-- JavaScripts initializations and stuff -->
    <script src="{{asset('js/neon-custom.js')}}"></script>


    <!-- Demo Settings -->
    <script src="{{asset('js/neon-demo.js')}}"></script>


</body>
</html>