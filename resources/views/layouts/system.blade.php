<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="" />

    <link rel="icon" href="{{asset('img/favicon.png')}}">

    <title>Bancamiga | Intranet</title>

    <link rel="stylesheet" href="{{asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('css/bancamiga.css')}}">

    <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
            <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        </script>
</head>
<body class="page-body login-page login-form-fall">

  <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #ffffff !important; border-color: #fff !important; border-top: 1px solid #57ab28 !important; border-bottom: 1px solid #e7e8e6 !important;">
   <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{asset('/home')}}"><img src="{{ asset('img/logoba.png') }}" width="150px" style="    margin-top: -8px;"></a>
    </div>
    
    <div class="collapse navbar-collapse js-navbar-collapse">
     <ul class="nav navbar-nav navbar-left" style="color: #005694; font-weight: 700">   
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">La Organización<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" style="font-weight: 700 !important">
            <li><a href="{{asset('/historia')}}">Historia</a></li>
            <li><a href="{{asset('/junta_directiva')}}">Junta Directiva</a></li>
            <li><a href="{{asset('/estructuras-organizativas')}}">Estructura Organizativa</a></li>
          </ul>
        </li>
          <li><a href="{{asset('/servicios-empleados')}}">Servicio al Empleado</a></li> 
      </ul>

        <ul class="nav navbar-nav navbar-left" style="color: #005694; font-weight: 700">   
        <li>
          <a href="#">Biblioteca de Documentos</a>
        </li>
      </ul>
        <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
        <ul class="nav navbar-nav navbar-right" style="font-weight: 700">
         <li><a href="#"><i class="fa fa-bell" style="color: #005694"></i></a></li>    
        <li class="dropdown">
          <a href="{{url('#')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{Auth::user()->name}}<span class="caret"></span></a>

          <ul class="dropdown-menu" role="menu">
              <li style="font-weight: 700">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="fa fa-plug">
                                            Desconectar
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
        
          </ul>
        </li>
      </ul>
    </div><!-- /.nav-collapse -->
  </div>
  </nav>
        @yield('content')
  <!-- Start Footer bottom Area -->
     
  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
            <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/twitter2.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                      <li>
                      <a href="#"><img src="{{asset('img/rrss/instagram.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/facebook.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/youtube.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/likenlin.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                  </ul>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Bancamiga</strong>.Todos los derechos reservados - RIF J-31628759-9
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- Footer -->
 <a href="#" class="scroll-top" title="Ir arriba"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript">

    $(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script type="text/javascript">
    $(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});
    $(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
        $('a.scroll-top').fadeIn('slow');

    } else {
        $('a.scroll-top').fadeOut('slow');
    }
});

$('a.scroll-top').click(function(event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 600);
});
</script>

    <!-- Bottom scripts (common) -->
    <script src="{{asset('js/gsap/TweenMax.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/joinable.js')}}"></script>
    <script src="{{asset('js/resizeable.js')}}"></script>
    <script src="{{asset('js/neon-api.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/neon-login.js')}}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{asset('js/neon-custom.js')}}"></script>


    <!-- Demo Settings -->
    <script src="{{asset('js/neon-demo.js')}}"></script>

</body>
</html>