<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="author" content="" />

	<link rel="icon" href="{{asset('img/favicon.png')}}">

	<title>Bancamiga | Administrador</title>

	<link rel="stylesheet" href="{{asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('css/admin_blue.css')}}">

	<link href="{{asset('fullcalendar-3.9.0/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('fullcalendar-3.9.0/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
	<script src="{{asset('fullcalendar-3.9.0/lib/moment.min.js')}}"></script>
    <script src="{{asset('fullcalendar-3.9.0/lib/jquery.min.js')}}"></script>

	<script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
</head>
<body class="page-body skin-facebook">
<div class="page-container">
		<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="{{asset('/home')}}">
						<img src="{{ asset('img/logo-transparente-blanco.png') }}" width="160" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon">
						<i class="entypo-menu"></i>
					</a>
				</div>
				
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation">
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>					
		<ul id="main-menu" class="main-menu">
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-newspaper-o"></i>
						<span class="title">Noticias</span>
					</a>
					<ul>
						<li>
							<a href="{{asset('/nueva-noticias')}}">
								<span class="title">Nueva Noticia</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/ba-admin')}}">
								<span class="title">Ver Listado de Noticias</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/categoria-noticia')}}">
								<span class="title">Categorías</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-bank"></i>
						<span class="title">Solicitudes Bancarias</span>
					</a>
					<ul>
						<li>
							<a href="{{asset('/nueva-solicitud')}}">
								<span class="title">Nueva Solicitud</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/solicitudes')}}">
								<span class="title">Ver Listado de Solicitudes</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-bullhorn"></i>
						<span class="title">Eventos</span>
					</a>
					<ul>
						<li>
							<a href="#">
								<i class="fa fa-birthday-cake"></i>
								<span class="title">Cumpleaños</span>
							</a>
								<ul>
						<!--			<li>
							<a href="{{asset('/crearcumpleanos')}}">

								<span class="title">Crear Evento Cumpleaños</span>
							</a>
						</li> -->
						<li>
							<a href="{{asset('/ver-cumpleanos')}}">

								<span class="title">Ver Cumpleaños</span>
							</a>
						</li>
					</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="title">Nuevo Ingreso</span>
							</a>
							<ul>
				<!--		<li>
							<a href="{{asset('/crear-nuevoingreso')}}">

								<span class="title">Crear Evento Empleados</span>
							</a>
						</li> -->
						<li>
							<a href="{{asset('/ver_nuevoingresos')}}">

								<span class="title">Ver Empleados</span>
							</a>
						</li>
					</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-calendar"></i>
								<span class="title">Calendario</span>
							</a>
								<ul>

						<li>
							<a href="{{asset('/ver_calendario')}}"><span class="title">Evento</span>
							</a>
						</li>
						
					</ul>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-thumbs-o-up"></i>
						<span class="title">Tips de Seguridad</span>
					</a>
					<ul>
						<li>
							<a href="{{asset('nuevo_tips')}}">
								<span class="title">Crear Tips</span>
							</a>
						</li>
							<li>
							<a href="{{asset('/ver-tips')}}">
								<span class="title">Ver Listados de Tips</span>
							</a>
						</li>
						<li>
							<a href="{{asset('categoria_tips')}}">
								<span class="title">Categorías</span>
							</a>
						</li>
					</ul>
				</li>
					<li class="has-sub">
							<a href="#">
								<i class="entypo-network"></i>
								<span class="title">La Organización</span>
							</a>
							<ul>
									<li class="has-sub">
					<a href="#">
						<i class="fa fa-building"></i>
						<span class="title">Historia</span>
					</a>
					<ul>
						<li>
							<a href="{{asset('/vista_historia')}}">
								<span class="title">Vista Previa</span>
							</a>
						</li>
						<!--	<li>
							<a href="{{asset('/redactar_historia')}}">
								<span class="title">Redactar Sección Conócenos</span>
							</a>
						</li> -->
						<li>
							<a href="{{asset('/sliders')}}">
								<span class="title">Gestionar Slider</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/mision')}}">
								<span class="title">Misión</span>
							</a>

						</li>
						<li>
							<a href="{{asset('/vision')}}">
								<span class="title">Visión</span>
							</a>
							
						</li>
						<li>
							<a href="{{asset('/valores')}}">
								<span class="title">Valores</span>
							</a>
							
						</li>
						
					</ul>
				</li>
								<li class="has-sub">
					<a href="#">
						<i class="entypo-vcard"></i>
						<span class="title">Junta Directiva</span>
					</a>
					<ul>
							<li>
							<a href="{{asset('/crear-junta')}}">
								<span class="title">Agregar Nuevo </span>
							</a>
						</li>
						<li>
							<a href="{{asset('/vista_junta_directiva')}}">
								<span class="title">Vista Previa</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-sitemap"></i>
						<span class="title">Estructura Organizativa</span>
					</a>
					<ul>
							<li>
							<a href="{{asset('/vista_estructura')}}">
								<span class="title">Vista Previa</span>
							</a>
						</li>
				<!--		<li>
							<a href="{{asset('/crear-estructura')}}">
								<span class="title">Redactar Estructura Organizativa</span>
							</a>
						</li> -->
					</ul>
				</li>
							</ul>
						</li>
			
				
					<li class="has-sub">
					<a href="#">
						<i class="fa fa-cloud-upload"></i>
						<span class="title">Administración de Documentos</span>
					</a>
					<ul>

						<li>
							<a href="">
								<i class="fa fa-file-pdf-o"></i>
								<span class="title">Documentos Gestión Humana</span>
							</a>
								<ul>
						<li>
							<a href="{{asset('/lista-uniformes-escolares')}}">

								<span class="title">Financiamiento Uniformes Escolares</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/lista-guarderia')}}">

								<span class="title">Guarderia</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/lista-prestaciones-sociales')}}">

								<span class="title">Prestaciones Sociales</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/lista-hcm')}}">

								<span class="title">Servicio H.C.M</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/lista-vacaciones')}}">

								<span class="title">Vacaciones</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/lista-recorrido-trabajador')}}">

								<span class="title">Recorrido Habt del Trabajador</span>
							</a>
						</li>
					</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-file-pdf-o"></i>
								<span class="title">Documentos Administrativos</span>
							</a>
								<ul>
						
						<li>
							<a href="{{asset('/lista-material-sede-principal')}}">

								<span class="title">Material Sede Principal</span>
							</a>
						</li>
						<li>
							<a href="{{asset('/formulario-material-agencia')}}">

								<span class="title">Material de Agencia</span>
							</a>
						</li>
					</ul>
						</li>
					</ul>

				</li> 
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-users"></i>
						<span class="title">Personal Bancamiga</span>
					</a>
					<ul>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="title">Usuarios</span>
							</a>
								<ul>
									<li>
							<a href="{{asset('/lista_usuarios')}}">

								<span class="title">Ver Usuarios</span>
							</a>
						</li> 
						<li>
							<a href="{{asset('/ficha_empleado')}}">

								<span class="title">Ficha del Empleado</span>
							</a>
						</li>
					</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-expeditedssl"></i>
								<span class="title">Roles</span>
							</a>
							<ul>
				<!--		<li>
							<a href="{{asset('/crear-nuevoingreso')}}">

								<span class="title">Crear Evento Empleados</span>
							</a>
						</li> -->
						<li>
							<a href="">

								<span class="title">Ver Roles</span>
							</a>
						</li>
					</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-key"></i>
								<span class="title">Permisologia</span>
							</a>
								<ul>

						<li>
							<a href="">
								<span class="title">Ver Permisología</span>
							</a>
						</li>
						
					</ul>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="fa fa-building-o"></i>
						<span class="title">Unidad Administrativa</span>
					</a>
					<ul>
						<li>
							<a href="#">
								<span class="title">Crear Unidad</span>
							</a>
								
						</li>
						<li>
							<a href="{{asset('/ver_listados_unidad')}}">
								<span class="title">Ver Listado de Unidades Administrativas</span>
							</a>
								
						</li>
					</ul>
				</li>
			</ul>
			
		</div>

	</div>
	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					
				<ul class="user-info pull-left pull-right-xs pull-none-xsm">


		
				</ul>
		
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
					<li class="sep"></li>
		
					<li>
						<a href="{{asset('/home')}}">
							Volver a HOME <i class="fa fa-mail-reply"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr/>
		
		
        @yield('content')

	
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2019 <strong>Bancamiga</strong>.Todos los derechos reservados - RIF J-31628759-9</a>
		
		</footer>
	</div>
</div>
	<script>

  $(function() {

  	"use strict";
  	var evt = [];
  	$.ajax({
    url: '/eventos/get',
    type: "GET",
    dataType:"JSON",
    async:false
  	}).done(function(r){
  		evt = r;
  	})

	 $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay,listDay'
      },

      navLinks: true, // can click day/week names to navigate views
     
      eventLimit: true, // allow "more" link when too many events
      events: evt,
      
        dayClick: function(date, jsEvent, view, resourceObj) {

        	$("#fecha_inicio").val(date.format());
        	$("#mdlEvent").modal();

 				 },
 		 editable: true,			 
 		 eventDrop: function(event, delta, revertFunct) {
   			 alert(event.title + " se dejó mover en " + event.start.format());

    		if (!confirm("¿Estás seguro de este cambio?")) {
      			revertFunct();
    		}
  		}		 	 
  		
    });


  });

</script>


	<link rel="stylesheet" href="{{asset('js/select2/select2-bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('js/select2/select2.css')}}">
	<link rel="stylesheet" href="{{asset('js/selectboxit/jquery.selectBoxIt.css')}}">
	
	<script src="{{asset('js/gsap/TweenMax.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/joinable.js')}}"></script>
    <script src="{{asset('js/resizeable.js')}}"></script>
    <script src="{{asset('js/neon-api.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/neon-login.js')}}"></script>
    <script src="{{asset('js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>
    <script src="{{asset('js/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('js/ckeditor/adapters/jquery.js')}}"></script>
	<script src="{{asset('js/select2/select2.min.js')}}"></script>
	<!-- JavaScripts initializations and stuff -->
    <script src="{{asset('js/neon-custom.js')}}"></script>
    <script src="{{asset('js/fileinput.js')}}"></script>
	<script src="{{asset('js/dropzone/dropzone.js')}}"></script>

	<script src="{{asset('fullcalendar-3.9.0/fullcalendar.min.js')}}"></script>
    <script src="{{asset('fullcalendar-3.9.0/locale/es.js')}}"></script>
    <script src="{{asset('fullcalendar-3.9.0/gcal.js')}}"></script>
	<!-- Demo Settings -->
    <script src="{{asset('js/neon-demo.js')}}"></script>

</body>
</html>