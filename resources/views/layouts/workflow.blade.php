<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="" />

    <link rel="icon" href="{{asset('img/favicon.png')}}">

    <title>Bancamiga | Intranet</title>

    <link rel="stylesheet" href="{{asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-icons/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/neon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('css/bancamiga.css')}}">
    <link rel="stylesheet" href="{{asset('js/select2/select2-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('js/select2/select2.css')}}">
    <link rel="stylesheet" href="{{asset('js/selectboxit/jquery.selectBoxIt.css')}}">
    <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
            <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        </script>
</head>
<body class="page-body login-page login-form-fall">

   <section class="hero" id="intro">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-right navicon">
          <a id="nav-toggle" class="nav_slide_button" href="#"><span></span></a>
        </div>
      </div>
    <div class="row">
       <div class="col-xs-4 col-md-4">
         <a href="{{asset('/home')}}" title="Home"><img class="logo_banner" alt="Bancamiga" src="{{ asset('img/logo-transparente-blanco.png') }}"></a>
       </div>
       <div class="col-xs-4 col-md-4">
         
       </div>
       <div class="col-xs-4 col-md-4 loginamiga_banner">
           <div class="dropdown" style="margin-left: 150px;">
  <button class="btn btn-dafault dropdown-toggle" type="button" data-toggle="dropdown"><i class="entypo-user" style="color: #035786!important"></i><span style="color: #333!important; font-weight: 600"> {{Auth::user()->name}}</span>
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="{{asset('ba-admin')}}"><i class="fa fa-dashboard"></i> Administrar Web</a></li>
    <li><a href="#">
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="fa fa-plug">Desconectar</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                          </form>
    </li>
  </ul>
</div>
       </div>
      </div>
    </div>
  </section>
      <nav class="navbar navbar-default navbar-fixed-button" style="background-color: #f3f3f3 !important; border-color: #fff !important;border-bottom: 1px solid #f3f3f3 !important; font-size: 12px">
   <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    
    <div class="collapse navbar-collapse js-navbar-collapse">
     <ul class="nav navbar-nav navbar-left" style="color: #005694;">   
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">La Organización<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" style="font-weight: 700 !important">
            <li><a href="{{asset('/historia')}}">Historia</a></li>
            <li><a href="{{asset('/junta-directiva')}}">Junta Directiva</a></li>
            <li><a href="{{asset('/estructuras-organizativas')}}">Estructura Organizativa</a></li>
          </ul>
        </li>
          <li><a href="{{asset('/servicios-empleados')}}">Servicio al Empleado</a></li> 
      </ul>

        <ul class="nav navbar-nav navbar-left" style="color: #005694; font-weight: 700">   
       <li>
          <a href="#">Biblioteca de Documentos</a>
        </li>
      </ul>
        <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
           <ul class="user-info pull-left pull-right-xs pull-none-xsm nav navbar-nav navbar-right" style="margin-top: 10px">
    
          <!-- Raw Notifications -->
          <li class="notifications dropdown">
    
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
              <i class="fa fa-bell"></i>
              <span class="badge badge-danger">0</span>
            </a>
    
            <ul class="dropdown-menu">
              <li class="top">
                <p class="small">
                  Tienes <strong>0</strong> notificaciones.
                </p>
              </li>
              
              <li>
                <ul class="dropdown-menu-list scroller">
                  <li class="unread notification-info">
                    <a href="#">
                      <i class="fa fa-car pull-right"></i>
                      
                      <span class="line">
                        <strong>Solicitud Transporte</strong>
                      </span>
                      
                      <span class="line small">
                        30 mint
                      </span>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="external">
                <a href="#">Ver todas las Notificaciones</a>
              </li>
            </ul>
    
          </li>
        </ul>
    </div><!-- /.nav-collapse -->
  </div>
  </nav>
  <section class="post-content-section" style="background-color: #fff;">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12">
                <div class="row">
      
      <div class="col-md-12">
    
   @yield('content')
      
      </div>

    </div>

             </div>
            <div class="col-lg-3  col-md-3 col-sm-12" style="border-left: 1px solid #e4e2e2; margin-top: -17px; background-color: #fbfbfb;">
         
         <div class="row" style="text-align: center;background-color: #54a6d0;padding-top: 10px; padding-bottom: 10px">
           <span style="font-weight: 700; color: #fff; font-size: 14px;">Solicitud de Transporte</span>
         </div>
         <br>

    <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Nombre</div>
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Creado</div>
  <div class="col-md-4" style="font-size: 10px !important; text-align: center; color: #005490; font-weight: 700">Estatus</div>
</div>
<br>
@if(count($consulta) > 0)
@foreach($consulta as $key)
         <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center;">{{$key->name}}</div>
  <div class="col-md-5" style="font-size: 10px !important;text-align: center;line-height: 2">{{$key->created_at}}</div>
  <div class="col-md-3" style="font-size: 10px !important; text-align: center;">{{$key->estado}}</div>
</div>
@endforeach
 @else
 <p style="font-size: 10px; text-align: center">
  No hay consultas para mostrar en esta vista "Solicitud de Transporte". Para agregar un nuevo elemento, haga clic en "Servicio de Empleados".
</p>
    @endif 
<br>
    <div class="row" style="text-align: center;background-color: #896fb4; padding-top: 10px; padding-bottom: 10px">
           <span style="font-weight: 700; color: #fff; font-size: 14px;">Solicitud de Material de Proveeduría</span>
         </div>
         <br>
               <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Nombre</div>
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Creado</div>
  <div class="col-md-4" style="font-size: 10px !important; text-align: center; color: #005490; font-weight: 700">Estatus</div>
</div>
<br>
<p style="font-size: 10px; text-align: center">
  No hay consultas para mostrar en esta vista "Solicitud de Material de Proveeduría". Para agregar un nuevo elemento, haga clic en "Servicio de Empleados".
</p>
<br>
    <div class="row" style="text-align: center;background-color: #f8c259; padding-top: 10px; padding-bottom: 10px">
           <span style="font-weight: 700; color: #fff; font-size: 14px;">Solicitud de Viaticos</span>
         </div>
          <br> 
               <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Nombre</div>
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Creado</div>
  <div class="col-md-4" style="font-size: 10px !important; text-align: center; color: #005490; font-weight: 700">Estatus</div>
</div>
<br>
@if(count($consulta1) > 0)
@foreach($consulta1 as $key)
         <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center;">{{$key->name}}</div>
  <div class="col-md-5" style="font-size: 10px !important;text-align: center;line-height: 2">{{$key->created_at}}</div>
  <div class="col-md-3" style="font-size: 10px !important; text-align: center;">{{$key->estatus}}</div>
</div>
@endforeach
 @else
 <p style="font-size: 10px; text-align: center">
 No hay consultas para mostrar en esta vista "Solicitud de Viaticos". Para agregar un nuevo elemento, haga clic en "Servicio de Empleados".
</p>
    @endif 
<br>
    <div class="row" style="text-align: center; background-color: #83bd3f; padding-top: 10px; padding-bottom: 10px">
           <span style="font-weight: 700; color: #fff; font-size: 14px;">Solicitud de Crediamigo</span>
         </div>
          <br>  
               <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Nombre</div>
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Creado</div>
  <div class="col-md-4" style="font-size: 10px !important; text-align: center; color: #005490; font-weight: 700">Estatus</div>
</div>
<br>
<p style="font-size: 10px; text-align: center">
  No hay consultas para mostrar en esta vista "Solicitud de Crediamigo". Para agregar un nuevo elemento, haga clic en "Servicio de Empleados".
</p>
<br>
 <div class="row" style="text-align: center; background-color: #008945; padding-top: 10px; padding-bottom: 10px">
           <span style="font-weight: 700; color: #fff; font-size: 14px;">Solicitud de F. Unif. Escolares</span>
         </div>
<br>
           <div class="row">
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Nombre</div>
  <div class="col-md-4" style="font-size: 10px !important;text-align: center; color: #005490; font-weight: 700">Creado</div>
  <div class="col-md-4" style="font-size: 10px !important; text-align: center; color: #005490; font-weight: 700">Estatus</div>
</div>
<br>
<p style="font-size: 10px; text-align: center">
  No hay consultas para mostrar en esta vista "Solicitud de F. Unif. Escolares". Para agregar un nuevo elemento, haga clic en "Servicio de Empleados".
</p>

<br>

            </div>
        </div>
      

    </div> <!-- /container -->
</section>
     
  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
            <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/twitter2.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                      <li>
                      <a href="#"><img src="{{asset('img/rrss/instagram.png')}}" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/facebook.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/youtube.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                    <li>
                      <a href="#"><img src="{{asset('img/rrss/likenlin.png')}}" width="43px" height="43px" style="margin-top: -3px;margin-left: -3px;"></a>
                    </li>
                  </ul>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Bancamiga</strong>.Todos los derechos reservados - RIF J-31628759-9
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- Footer -->
 <a href="#" class="scroll-top" title="Ir arriba"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript">

    $(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script type="text/javascript">
    $(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});
    $(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
        $('a.scroll-top').fadeIn('slow');

    } else {
        $('a.scroll-top').fadeOut('slow');
    }
});

$('a.scroll-top').click(function(event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 600);
});
// otros en selecionar
function d1(selectTag){
    if(selectTag.value == 'otro1'){
      document.getElementById('prg1').disabled = false;
    }else{
      document.getElementById('prg1').disable = true;
    }

}
</script>

    <!-- Bottom scripts (common) -->
    <script src="{{asset('js/gsap/TweenMax.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/joinable.js')}}"></script>
    <script src="{{asset('js/resizeable.js')}}"></script>
    <script src="{{asset('js/neon-api.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/neon-login.js')}}"></script>
    <script src="{{asset('js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>
    <script src="{{asset('js/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <!-- JavaScripts initializations and stuff -->
    <script src="{{asset('js/neon-custom.js')}}"></script>


    <!-- Demo Settings -->
    <script src="{{asset('js/neon-demo.js')}}"></script>

</body>
</html>