<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> Solicitud de Servicio de Transporte</title>
    <link rel="stylesheet" href="{{asset('css/pdf.css')}}" media="all" />
  </head>
  <body>

   <div class='container'>
            @yield('content')
        </div>
	
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>