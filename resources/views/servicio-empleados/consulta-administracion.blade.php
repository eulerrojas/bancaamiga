@extends('layouts.servicios')

@section('content')

<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Solicitudes Generales Administración</h3>
     </div>
      <br>
      <br>
      <br>
     <!-- Breadcrumb 1 -->
       <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/servicios-empleados')}}">
                  <i class="fa fa-square"></i>
                 Consulta General de Solicitudes
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-square"></i>
                 Transporte
                </a>
              </li>
              <li>
                <a href="{{asset('/consulta-materiales-proveeduria')}}">
                  <i class="fa fa-square"></i>
                  Material de Proveeduría
                </a>
              </li>
              <li><a href="{{asset('/consulta-viaticos')}}">
              <i class="fa fa-square"></i>
              viaticos</a></li>
            </ol>
          </div>  
 </div>                   
        <div class="panel-body">
<p>Consultas > <strong>Solicitud Transporte</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                    @foreach($listarPendientes as $key)
                      <tr style="text-align: center"> 
                        <td>SST-{{$key->id}}</td>
                        <td>{{$key->solicitante}}</td>
                        <td>{{$key->created_at}}</td>
                        <td>
                            @if($key->estado == 'aprobado')
                          <div class="label label-success" style="background: #60a511; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                           @elseif($key->estado == 'anulado')
                          <div class="label label-success" style="background: #b30000; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                            @elseif($key->estado == 'devuelto')
                          <div class="label label-success" style="background: #29a2d9; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                           @else 
                          <div class="label label-warning" style="background: #f2a73b; border-radius: 5px; font-size: 12px">
                          {{$key->estado}}</div>
                           @endif
                         
                        </td>
                        <td>{{$key->supervisor}} </td>
                        <td>
                         @if(Auth::user()->id_rol == 2)
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editar_wf_transporte', $key->id)}}" onclick="return confirm('Revisar Formulario Servicio Transporte. ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>

                        @else 
                            <button type="button" class="btn btn-primary" style="background-color: #662d88">
                              <a href="{{ url('reporte-transporte',$key->id) }}" style="color: #fff"><i class="fa fa-file-pdf-o" title="Descargar PDF"></i></a>
                            </button>
                         @endif 
                        </td>
                      </tr>
                    @endforeach                                       
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
@endsection