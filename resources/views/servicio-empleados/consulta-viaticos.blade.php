@extends('layouts.servicios')

@section('content')

<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Solicitudes Generales Administración</h3>
     </div>
      <br>
      <br>
      <br>
     <!-- Breadcrumb 1 -->
       <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/servicios-empleados')}}">
                  <i class="fa fa-square"></i>
                 Consulta General de Solicitudes
                </a>
              </li>
              <li>
                <a href="{{asset('/consulta-transporte')}}">
                  <i class="fa fa-square"></i>
                 Transporte
                </a>
              </li>
              <li>
                <a href="{{asset('/consulta-materiales-proveeduria')}}">
                  <i class="fa fa-square"></i>
                  Material de Proveeduría
                </a>
              </li>
              <li><a href="{{asset('/consulta-viaticos')}}">
              <i class="fa fa-square"></i>
              viaticos</a></li>
            </ol>
          </div>  
 </div>                   
          <div class="panel-body">
<p>Consultas > <strong>Solicitud Viaticos</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                      <tr style="text-align: center"> 
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        </td>
                      </tr>
                                                     
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>

@endsection