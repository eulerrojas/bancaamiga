@extends('layouts.servicios')

@section('content')

<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Solicitudes Generales</h3>
     </div>
      <br>
      <br>
      <br>
     <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/servicios-empleados')}}">
                  <i class="fa fa-square"></i>
                 <span style="font-weight: 700">Consulta General de Solicitudes</span>
                </a>
              </li>
              <li>
                <a href="{{asset('/consulta-crediamigo')}}">
                  <i class="fa fa-square"></i>
                  <span style="font-weight: 700">Consulta General Gestión Humana</span>
                </a>
              </li>
              <li><a href="{{asset('/consulta-transporte')}}">
              <i class="fa fa-square"></i>
               <span style="font-weight: 700">Consulta General Administración</span>
            </a></li>
            </ol>
          </div>  
           
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Solicitud! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
 </div> 
   
           <div class="panel-body">
<p>Consultas > <strong>Solicitud Transporte</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                    @foreach($listarPendientes as $key)
                      <tr style="text-align: center"> 
                        <td>SST-{{$key->id}}</td>
                        <td>{{$key->solicitante}}</td>
                        <td>{{$key->created_at}}</td>
                        <td>
                            @if($key->estado == 'aprobado')
                          <div class="label label-success" style="background: #60a511; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                           @elseif($key->estado == 'anulado')
                          <div class="label label-success" style="background: #b30000; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                            @elseif($key->estado == 'devuelto')
                          <div class="label label-success" style="background: #29a2d9; border-radius: 5px; font-size: 12px">{{$key->estado}}
                            </div>
                           @else 
                          <div class="label label-warning" style="background: #f2a73b; border-radius: 5px; font-size: 12px">
                          {{$key->estado}}</div>
                           @endif
                         
                        </td>
                        <td>{{$key->supervisor}} </td>
                        <td>
                         @if(Auth::user()->id_rol == 2)
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editar_wf_transporte', $key->id)}}" onclick="return confirm('Revisar Formulario Servicio Transporte. ')" ><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>

                        @else 
                            <button type="button" class="btn btn-primary" style="background-color: #662d88">
                              <a href="{{ url('reporte-transporte',$key->id) }}" style="color: #fff"><i class="fa fa-file-pdf-o" title="Descargar PDF"></i></a>
                            </button>
                         @endif 
                        </td>
                      </tr>
                    @endforeach                                       
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
           <div class="panel-body">
<p>Consultas > <strong>Solicitud Material de Proveeduria</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                   
                      <tr style="text-align: center"> 
                        <td>SSM-</td>
                        <td></td>
                        <td></td>
                        <td><div class="label label-warning" style="background: #f2a73b; border-radius: 5px; font-size: 12px"></div></td>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-primary" style="background-color: #662d88">
                              <a href="" style="color: #fff"><i class="fa fa-file-pdf-o" title="Descargar PDF"></i></a>
                            </button>
                        </td>
                      </tr>
                                                         
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
               <div class="panel-body">
<p>Consultas > <strong>Solicitud Viaticos</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                     <tr style="text-align: center"> 
                        <td>SSM-</td>
                        <td></td>
                        <td></td>
                        <td><div class="label label-warning" style="background: #f2a73b; border-radius: 5px; font-size: 12px"></div></td>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-primary" style="background-color: #662d88">
                              <a href="" style="color: #fff"><i class="fa fa-file-pdf-o" title="Descargar PDF"></i></a>
                            </button>
                        </td>
                      </tr>                                     
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
               <div class="panel-body">
<p>Consultas > <strong>Solicitud Crediamigo</strong></p>
                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; border-top-left-radius: 20px; border: 1px solid #fff; text-align: center">Código</th>
                            <th style="font-size: 12px !important; text-align: center">Nombre del Solicitante</th>
                            <th style="font-size: 12px !important; text-align: center">Fecha de Solicitud</th>
                            <th style="font-size: 12px !important; text-align: center">Estatus</th>
                            <th style="font-size: 12px !important; text-align: center">Supervisor Responsable</th>
                            <th style="font-size: 12px !important; border-top-right-radius: 20px; border: 1px solid #fff;text-align: center">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                      <tr style="text-align: center"> 
                        <td>SSM-</td>
                        <td></td>
                        <td></td>
                        <td><div class="label label-warning" style="background: #f2a73b; border-radius: 5px; font-size: 12px"></div></td>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-primary" style="background-color: #662d88">
                              <a href="" style="color: #fff"><i class="fa fa-file-pdf-o" title="Descargar PDF"></i></a>
                            </button>

                        </td>
                      </tr>                                       
                    </tbody>
            </table>
         <div style="float: right;"></div> 
        </div>
    </div>
@endsection
