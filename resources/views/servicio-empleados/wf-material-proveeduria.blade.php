@extends('layouts.workflow')

@section('content')
<style>
    td, input {padding:5px;}
    </style>
		<div class="row" style="padding-left: 40px">
			<h3 style="margin-left: 30px">SOLICITUD DE MATERIAL PROVEEDURÍA</h3>
		<br />
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registar_workflow_transporte')}}" method="POST">
			<div class="col-md-12" style="margin-top: 50px">
		
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I. DATOS DE LA UNIDAD SOLICITANTE
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-4" style="text-align: center; padding-top: 5px; color: #333">
								<label class="label_bancamiga"><strong>TIPO DE SOLICITANTE</strong></label>
							</div>
							
							<div class="col-md-8">
								<div class="row">
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio"  name="ivuelta"  value="1">
										<label>Sede Administrativa</label>
									</div>
  									</div>
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio"  name="ivuelta" checked  value="2">
										<label>Red de Agencias</label>
									</div>
  									</div>
								</div>
							</div>
							</div>
						<div id="div1" style="display:;">
    <div class="form-group">
								<div class="col-md-4" style="text-align: center; padding-top: 5px; color: #333">
								<label class="label_bancamiga"><strong>RED DE AGENCIA</strong></label>
							</div>
								<div class="col-md-8">
									<select name="cod_oficina" class="select2 cod_oficina" data-allow-clear="true" data-placeholder="Seleccione Agencia" id="cod_oficia">
										<option selected="selected"></option>
										<optgroup label="Red de Agencias Bancamiga">
										@php
                                        $red_ofi = App\Red_oficina::all();
                                        @endphp
                                        @foreach($red_ofi as $key)
											<option value="{{$key->id}}">{{$key->cod_oficina}} - {{$key->nom_oficina}}</option>
										@endforeach	
										</optgroup>
									</select>
							</div>
							
							</div>
							<div class="form-group">
								<div class="col-md-4" style="text-align: center; padding-top: 5px; color: #333">
								<label class="label_bancamiga"><strong>ALMACÉN</strong></label>
							</div>
								<div class="col-md-8">
									<select name="cod_almacen" class="select2 cod_almacen" data-allow-clear="true" data-placeholder="Seleccione Almacén" id="cod_almacen">
										<option selected="selected"></option>
										<optgroup label="Almacenes asociados a Red de Agencias Bancamiga">
										
											<option selected="selected"></option>
									
										</optgroup>
									</select>
							</div>
							
							</div>
</div>
        
<div id="div2" style="display:none;">
    <div class="form-group">
								<div class="col-md-4" style="text-align: center; padding-top: 5px; color: #333">
								<label class="label_bancamiga"><strong>UNIDAD ADMINISTRATIVA</strong></label>
							</div>
								<div class="col-md-8">
									<select name="cod_oficina" class="select2" data-allow-clear="true" data-placeholder="Seleccione Unidad Administrativa">
										<option></option>
										<optgroup label="Unidades Administrativas Bancamiga">
										@php
                                        $unidad = App\Departamento::all();
                                        @endphp
                                        @foreach($unidad as $key)
											<option value="{{$key->id}}">{{$key->cod_departamento}} - {{$key->descripcion}}</option>
										@endforeach	
										</optgroup>
									</select>
							</div>
							
							</div>
</div>
							
					</div>
				</div>

				<!-- 2 -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DESCRIPCIÓN DE LA SOLICITUD
						</div>
					</div>
					<div class="panel-heading">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							MATERIAL(ES) PROVEEDURÍA

						</div>
					</div>
					<div class="panel-body">
						<button type="button" id="add" class="btn btn-default tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar Fila"><i class="fa fa-plus"></i></button> 
						<button type="button" class="btn btn-default tooltip-primary" id="del" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Fila"><i class="fa fa-close"></i></button> 
<p>
    <table id="tabla" class='table table-bordered'>
        <tr>
            <td style="text-align: center; font-weight: 700; color: #333">Descripción</td>
            <td style="text-align: center; font-weight: 700; color: #333">Cantidad solicitada</td>
            <td style="text-align: center; font-weight: 700; color: #333">Cantidad recibida</td>
        </tr>
    </table>
</p>
					</div>
				</div>
				<!-- Tercera parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. OBSERVACIONES
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<textarea class="form-control autogrow" id="field-ta" name="motivo" required></textarea>
							</div>
							</div>
					</div>
				</div>
				<!-- cuarta parte -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV. FIRMAS
						</div>
					</div>
					<div class="panel-heading">
					<div class="col-md-12">	
						<div class="col-md-6">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							ELABORADO POR (SOLICITANTE): 

						</div>
						</div>
						<div class="col-md-6">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							APROBADO POR (SUPERVISOR INMEDIATO):

						</div>
						</div>
					</div>	
					</div>
					<div class="panel-body">
					
							<div class="col-md-6">
							
								<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" required="required">
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">CARGO :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
						<div class="row"  style="margin-top: 7px">
							<div class="col-md-5">
								<label class="label_bancamiga">FIRMA :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
							</div>

							<div class="col-md-6">
								<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" required="required">
							</div>
						</div>
							<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">CARGO :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
						<div class="row"  style="margin-top: 7px">
							<div class="col-md-5">
								<label class="label_bancamiga">FIRMA :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
							</div>
						
					</div>
				</div>
								<!-- quinta parte -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							V. USO EXCLUSIVO DE LA GERENCIA DE ADMINISTRACIÓN
						</div>
					</div>
					<div class="panel-heading">
					<div class="col-md-12">	
						<div class="col-md-6">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							PROCESADO POR (Coordinación Servicios Administrativos):  

						</div>
						</div>
						<div class="col-md-6">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							AUTORIZADO POR (Gerencia de Administración):

						</div>
						</div>
					</div>	
					</div>
					<div class="panel-body">
					
							<div class="col-md-6">
							
								<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" required="required">
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">CARGO :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
						<div class="row"  style="margin-top: 7px">
							<div class="col-md-5">
								<label class="label_bancamiga">FIRMA :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
							</div>

							<div class="col-md-6">
								<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" required="required">
							</div>
						</div>
							<div class="row">
							<div class="col-md-5">
								<label class="label_bancamiga">CARGO :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
						<div class="row"  style="margin-top: 7px">
							<div class="col-md-5">
								<label class="label_bancamiga">FIRMA :</label>
							</div>
							<div class="col-md-7">
								<input type="text" class="form-control" disabled="disabled">
							</div>
						</div>
							</div>
						
					</div>
				</div>
				<!-- quinta parte -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							VI. RECEPCIÓN DEL MATERIAL
						</div>
					</div>
					<div class="panel-heading">
						<div class="panel-title" style="background: #eaeaea; text-align: center !important; border-radius: 2px; color: #333; font-weight: 700; font-size: 10px">
							PROCESADO POR (Coordinación Servicios Administrativos):  

						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
								
							<div class="col-md-3"  style="text-align: center">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS</label>
							</div>
							
							<div class="col-md-9">
								<input type="text" class="form-control">
							</div>
						
							</div>
				<div class="form-group">
								
							<div class="col-md-3"  style="text-align: center">
								<label class="label_bancamiga">CARGO</label>
							</div>
							
							<div class="col-md-9">
								<input type="text" class="form-control">
							</div>
						
							</div>
							<div class="form-group">
								
							<div class="col-md-3"  style="text-align: center">
								<label class="label_bancamiga">FIRMA</label>
							</div>
							
							<div class="col-md-9">
								<input type="text" class="form-control">
							</div>
						
							</div>
					</div>
				</div>
	<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
					
				</div>
			</form>
			</div>
 <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        /**
         * Funcion para añadir una nueva columna en la tabla
         */
        $("#add").click(function(){
            // Obtenemos el numero de filas (td) que tiene la primera columna
            // (tr) del id "tabla"
            var tds=$("#tabla tr:first td").length;
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla tr").length;
            var nuevaFila="<tr>";
            for(var i=0;i<tds;i++){
                // añadimos las columnas
                nuevaFila+="<td><input type='text' class='form-control'></td>";
            }
            // Añadimos una columna con el numero total de filas.
            // Añadimos uno al total, ya que cuando cargamos los valores para la
            // columna, todavia no esta añadida
            
            
            nuevaFila+="</tr>";
            $("#tabla").append(nuevaFila);
        });
 
        /**
         * Funcion para eliminar la ultima columna de la tabla.
         * Si unicamente queda una columna, esta no sera eliminada
         */
        $("#del").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla tr").length;
            if(trs>1)
            {
                // Eliminamos la ultima columna
                $("#tabla tr:last").remove();
            }
        });
    });
    </script>
   <!-- script para los efectos de red de agencias bancarias y unidad administrativa -->
    <script type="text/javascript">
    $(document).ready(function() {
    $("input[type=radio]").click(function(event){
        var valor = $(event.target).val();
        if(valor =="2"){
            $("#div1").show();
            $("#div2").hide();
        } else if (valor == "1") {
            $("#div1").hide();
            $("#div2").show();
        } else { 
            // Otra cosa
        }
    });
});
    </script>
 <!-- script para relacionar Red de Agencias con Almacenes -->
<script type="text/javascript">

</script>  
@endsection