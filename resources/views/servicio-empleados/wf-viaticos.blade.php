@extends('layouts.workflow')

@section('content')
<style type="text/css">
*{ font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif}
.main{ margin:auto; border:1px solid #7C7A7A; width:40%; text-align:left; padding:30px; background:#85c587}
input[type=submit]{ background:#6ca16e; width:100%;
    padding:5px 15px; 
    background:#ccc; 
    cursor:pointer;
    font-size:16px;
   
}
input[type=text]{ margin: 5px;
   
}
</style>
<div class="row" style="padding-left: 40px">
	
	<h3 style="margin-left: 30px">SOLICITUD DE VIATICOS</h3>
		<br />
	<div class="col-md-12"><span style="float: right; font-size: 16px; color: #333"><stong>N° de Solicitud :<span style="color: #cc2424"> SV-001</span></stong></span></div>
	<div class="col-md-12"><span style="float: right; color: #333"><stong>Fecha : 28/07/2019</stong></span></div>	
	<div class="col-md-12" style="margin-top: 20px">
				<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registrarviatico')}}" method="POST">
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading" style="background-color: #fff">
						<div class="panel-title" style="background: #fff; border-radius: 2px; color: #333;">
							<div class="row">
  								<div class="col-md-4">
  									<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" name="tipo_servicio" value="1">
										<label style="font-size: 10px">NACIONAL</label>
									</div>
  								</div>
  								<div class="col-md-4">
  									<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" name="tipo_servicio" value="2">
										<label style="font-size: 10px">ALOJAMIENTO</label>
									</div>
  								</div>
  								<div class="col-md-4">
  									<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" name="tipo_servicio" value="3">
										<label style="font-size: 10px">BOLETO AÉREO</label>
									</div>
  								</div>
					</div>
						</div>
					</div>
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I. INFORMACIÓN DEL EMPLEADO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-3">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled >
							</div>
							
							<div class="col-md-3">
								<label class="label_bancamiga">N° DE EMPLEADO</label>
								<input type="text" class="form-control" disabled >
							</div>
							<div class="col-md-3">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES</label>
								<input type="text" class="form-control" disabled >
							</div>
							<div class="col-md-3">
								<label class="label_bancamiga">CARGO</label>
								<input type="text" class="form-control" disabled >
							</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3">
								<label class="label_bancamiga">NRO. LOCAL</label>	
								<input type="text" class="form-control" disabled >
							</div>
								<div class="col-md-3">
								<label class="label_bancamiga">NRO. CELULAR</label>		
								<input type="text" class="form-control" disabled>
							</div>
							<div class="col-md-6">
								<label class="label_bancamiga">NÚMERO DE CUENTA A DEPOSITAR</label>	
								<input type="text" class="form-control" name="n_cuentabancaria" required="" value="" autofocus placeholder="Ejem 0175-0000-0000-0000-0000" maxlength="24">
							</div>
							</div>
				
					</div>
					<!-- SEGUNDA PARTE -->
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DATOS DEL VIAJE
						</div>
					</div>
						<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">DESTINO</label>
								<select name="estado_id" class="select2" data-allow-clear="true" data-placeholder="Seleccione Ciudad..." required="required">
										<option></option>
										@php
                                        $estado = App\Estados::all();
                                        @endphp
										<optgroup label="Ciudad">
											@foreach($estado as $key)
											<option value="{{$key->id}}">{{$key->estado}}</option>
											@endforeach
										</optgroup>
									</select>
							</div>
							
							</div>
							
							<div class="form-group">
								
							<div class="col-md-3">
								<label class="label_bancamiga">FECHA DE SALIDA</label>	
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha_salida" required>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga"> HORA SALIDA</label>	
								<input type="text" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="12:00 AM" data-show-meridian="true" data-minute-step="5" name="hora_salida" required />
							</div>
							<div class="col-md-3">
								<label class="label_bancamiga">FECHA DE REGRESO</label>	
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha_regreso" required>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">HORA REGRESO</label>	
								<input type="text" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="12:00 AM" data-show-meridian="true" data-minute-step="5" name="hora_regreso" required>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° DE DIAS</label>	
								<input type="text" class="form-control" name="n_dias" required>
							</div>
							</div>
								<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">MOTIVO DEL VIAJE</label>
								<textarea class="form-control autogrow" id="field-ta" name="motivo_viaje" rows="3" required></textarea>
							</div>
							
							</div>
					</div>
					<!-- TERCERA PARTE -->
					<div class="panel-heading">
						<div class="panel-title" style="background: #f5f5f5; text-align: center !important; border-radius: 2px; color: #333;">
							<span>Los montos son representados en</span> <strong>Bolívares Soberanos.</strong>
						</div>
					</div>
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. GASTOS DE ALIMENTACIÓN ESTIMADOS
						</div>
					</div>
					<div class="panel-body">
						<table class='table table-bordered'>
							<thead style="color: #333 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; text-align: center">Tipo</th>
                            <th style="font-size: 12px !important; text-align: center">Cantidad</th>
                            <th style="font-size: 12px !important; text-align: center">Costo Unidad</th>
                            <th style="font-size: 12px !important; text-align: center">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	<td style="text-align: center; color: #333; width: 40%"><strong>Desayuno</strong></td>
                    	<td><input type="text" name="cant" id="multiplicando" value=0 onchange="multiplicar();"/></td>
						<td><input type="text" name="costo_u" id="multiplicador" onchange="multiplicar();" value=12.150 /></td>
                    	<td align="right"><input type="text" name="monto_1" id="resultado" onchange="sumar();" value=0 /></td>
                    </tr>
                    <tr>
                    	<td style="text-align: center; color: #333; width: 40%"><strong>Almuerzo</strong></td>
                    	<td><input type="text" name="cant2" id="multiplicando2" value=0 onchange="multiplicar();" /></td>
						<td><input type="text" name="costo_u2" id="multiplicador2" onchange="multiplicar();" value=20.250  /></td>
                    	<td align="right"><input type="text" name="monto_2" id="resultado2" onchange="sumar();" value=0  /></td>
                    </tr>
                    <tr>
                    	<td style="text-align: center; color: #333; width: 40%"><strong>Cena</strong></td>
                    	<td><input type="text" name="cant3" id="multiplicando3" value=0 onchange="multiplicar();"/></td>
						<td><input name="costo_u3" id="multiplicador3" onchange="multiplicar();" value=17.500 /></td>
                    	<td align="right"><input type="text" name="monto_3" id="resultado3" onchange="sumar();" value=0  /></td>
                    </tr>
                    <tr > 
					<td colspan="3" align="right" style="background-color: #f5f5f5"><strong style="color: #333; font-size: 14px">Sub Total: </strong></td>
					<td align="right" colspan="6" >
  					<input type="text" name="subtotal1" id="total" size="20" onfocus="sumar()" />
					 </td>
 					</tr>
                    </tbody>
						</table> 
					</div>
					<!-- CUARTA PARTE -->
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV.  OTROS GASTOS 
						</div>
					</div>
					<div class="panel-body">
					<table class='table table-bordered'>
						<thead style="color: #333 !important;">
                        <tr style="color: #000000 !important;">
                            <th style="font-size: 12px !important; text-align: center">Tipo</th>
                            <th style="font-size: 12px !important; text-align: center">Cantidad</th>
                            <th style="font-size: 12px !important; text-align: center">Costo Unidad</th>
                            <th style="font-size: 12px !important; text-align: center">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                    	<td style="text-align: center; color: #333; width: 40%">
                    		<select class="form-control" name="gastos_id">
                    					@php
                                        $gastos = App\Gastos_viaticos::all();
                                        @endphp	
										<option>Seleccione</option>
										@foreach($gastos as $key)
										<option value="{{$key->id}}">{{$key->titulo_gasto}}</option>
										@endforeach
									</select>
                    	</td>
                    	<td><input type="text" name="cant_gasto" id="cantidad" value=0 onchange="multiplicar_gastos();"/></td>
						<td><input type="text" name="costo_gasto" id="costo" onchange="multiplicar_gastos();" value=75 /></td>
                    	<td align="right"><input type="text" name="monto_gasto" id="resultado_costo" onchange="sumar_gastos();" value=0 /></td>
                    </tr>
                    <tr>
                    	<td style="text-align: center; color: #333; width: 40%">
                    		<select class="form-control" name="gastos_id1">
                    					@php
                                        $gastos = App\Gastos_viaticos::all();
                                        @endphp	
										<option>Seleccione</option>
										@foreach($gastos as $key)
										<option value="{{$key->id}}">{{$key->titulo_gasto}}</option>
										@endforeach
									</select>
                    	</td>
                    	<td><input type="text" name="cant_gasto2" id="cantidad2" value=0 onchange="multiplicar_gastos();"/></td>
						<td><input type="text" name="costo_gasto2" id="costo2" onchange="multiplicar_gastos();" value=10.000 /></td>
                    	<td align="right"><input type="text" name="monto_gasto2" id="resultado_costo2" onchange="sumar_gastos();" value=0 /></td>
                    </tr>
                    <tr>
                    	<td style="text-align: center; color: #333; width: 40%">
                    		<select class="form-control" name="gastos_id2">
                    					@php
                                        $gastos = App\Gastos_viaticos::all();
                                        @endphp	
										<option>Seleccione</option>
										@foreach($gastos as $key)
										<option value="{{$key->id}}">{{$key->titulo_gasto}}</option>
										@endforeach
									</select>
                    	</td>
                    	<td><input type="text" name="cant_gasto3" id="cantidad3" value=0 onchange="multiplicar_gastos();"/></td>
						<td><input type="text" name="costo_gasto3" id="costo3" onchange="multiplicar_gastos();" value=75.000 /></td>
                    	<td align="right"><input type="text" name="monto_gasto3" id="resultado_costo3" onchange="sumar_gastos();" value=0 /></td>
                    </tr>
                    <tr > 
					<td colspan="3" align="right" style="background-color: #f5f5f5"><strong style="color: #333; font-size: 14px">Sub Total: </strong></td>
					<td align="right" colspan="6" >
  					<input type="text" name="subtotal_gasto" id="totales" size="20" onfocus="sumar_costos()" />
					 </td>
 					</tr>
 					<tr > 
					<td colspan="3" align="right" style="background-color: #f5f5f5"><strong style="color: #333; font-size: 14px">Gasto Total: </strong></td>
					<td align="right" colspan="6" >
  					<input type="text" name="total_general" id="totalgeneral" size="20" onfocus="sumar_general()" />
					 </td>
 					</tr>
                    </tbody>
					</table>	
					</div>
					<!-- QUINTA PARTE -->
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							V.  OBSERVACIONES 
						</div>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
								<textarea class="form-control autogrow" id="field-ta" name="observacion_viaticos" rows="3" required></textarea>
							</div>
					</div>
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							VI. PROCESADO POR :
						</div>
					</div>
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES DEL SOLICITANTE:</label>
								<input type="text" class="form-control" disabled value="{{Auth::user()->name}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES SUPERVISOR INMEDIATO</label>
								<select name="supervisor_id" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Supervisores">
										
                                     
											<option value=""></option>
									
										</optgroup>
									</select>
							</div>
							
							</div>
				
					</div>
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
				</div>
			</form>
		
	</div>			
</div>
<!-- Desayuno / Almuerzo / Cena -->
<script type="text/javascript">
function multiplicar() {
m1 = document.getElementById("multiplicando").value;
m2 = document.getElementById("multiplicador").value;
r = m1*m2;
document.getElementById("resultado").value = r;
 
m1 = document.getElementById("multiplicando2").value;
m2 = document.getElementById("multiplicador2").value;
r2 = m1*m2;
document.getElementById("resultado2").value = r2;
 
m1 = document.getElementById("multiplicando3").value;
m2 = document.getElementById("multiplicador3").value;
r3 = m1*m2;
document.getElementById("resultado3").value = r3;
 
 
}
 
 function sumar() {

t1 = document.getElementById("resultado").value;
t2 = document.getElementById("resultado2").value;
t3 = document.getElementById("resultado3").value;

var rt = parseFloat(t1) + parseFloat(t2) + parseFloat(t3);

document.getElementById("total").value = rt;
}
</script>
<!-- Desayuno / Almuerzo / Cena -->
<!-- Otros Gastos -->
<script type="text/javascript">
function multiplicar_gastos() {
c1 = document.getElementById("cantidad").value;
c2 = document.getElementById("costo").value;
res = c1*c2;
document.getElementById("resultado_costo").value = res;
 
c1 = document.getElementById("cantidad2").value;
c2 = document.getElementById("costo2").value;
res2 = c1*c2;
document.getElementById("resultado_costo2").value = res2;
 
c1 = document.getElementById("cantidad3").value;
c2 = document.getElementById("costo3").value;
res3 = c1*c2;
document.getElementById("resultado_costo3").value = res3;
 
 
}
 
 function sumar_costos() {

tt1 = document.getElementById("resultado_costo").value;
tt2 = document.getElementById("resultado_costo2").value;
tt3 = document.getElementById("resultado_costo3").value;

var restotal = parseFloat(tt1) + parseFloat(tt2) + parseFloat(tt3);

document.getElementById("totales").value = restotal;
}
 function sumar_general() {

gasto_t1 = document.getElementById("total").value;
gasto_t2 = document.getElementById("totales").value;

var general_total = parseFloat(gasto_t1) + parseFloat(gasto_t2);

document.getElementById("totalgeneral").value = general_total;
}
</script>



@endsection