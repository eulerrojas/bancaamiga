@extends('layouts.workflow')

@section('content')
		<div class="row" style="padding-left: 40px">
			<h3 style="margin-left: 30px">SOLICITUD DE SERVICIO DE MANTENIMIENTO</h3>
		<br />
			<div class="col-md-12">
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registar_workflow_transporte')}}" method="POST">
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I.  DATOS DE LA UNIDAD SOLICITANTE
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">AGENCIA: </label>
								<input type="text" class="form-control" required="" autofocus="">
							</div>
							</div>
					</div>
				</div>
				<!-- Segundo formulario -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DESCIPCIÓN DE LA SOLICITUD
						</div>
					</div>
					<div class="panel-body">
				
						<div class="tabbable-panel" style="margin-top: -30px;">
				<div class="tabbable-line">
					<ul class="nav nav-tabs">
						<li class="active"  style="width: 25% !important;">
							<a href="#tab_default_1" data-toggle="tab">
							<span style="font-size: 10px">ELECTRICIDAD / ILUMINACIÓN</span></a>
						</li>
						<li  style="width: 15% !important;">
							<a href="#tab_default_2" data-toggle="tab">
							<span style="font-size: 10px">INFRAESTRUCTURA</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_3" data-toggle="tab">
							<span style="font-size: 10px">PLOMERÍA</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_4" data-toggle="tab">
							<span style="font-size: 10px">MOBILIARIO</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_5" data-toggle="tab">
							<span style="font-size: 10px">EQUIPOS</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_6" data-toggle="tab">
							<span style="font-size: 10px">SEGURIDAD</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_7" data-toggle="tab">
							<span style="font-size: 10px">PUBLICIDAD</span></a>
						</li>
						<li  style="width: 10% !important;">
							<a href="#tab_default_8" data-toggle="tab">
							<span style="font-size: 10px">OTROS</span></a>
						</li>
					</ul>
					<div class="tab-content">
						
						<div class="tab-pane active" id="tab_default_1">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>ELECTRICIDAD / ILUMINACIÓN</strong></span></div>
						<br>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_2">
<div class="col-md-12" style="text-align: center; color: #333"><span><strong>INFRAESTRUCTURA</strong></span></div>
						<br>
							<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_3">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>PLOMERÍA</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_4">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>MOBILIARIO</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_5">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>EQUIPOS</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_6">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>SEGURIDAD</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_7">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>PUBLICIDAD</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
											
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_default_8">
							<div class="col-md-12" style="text-align: center; color: #333"><span><strong>OTROS</strong></span></div>
								<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Servicio Solicitado</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-12">
								<label class="label_bancamiga">Descripción de la Avería</label>	
								<textarea class="form-control" id="field-ta"></textarea>
							</div>
						</div>
						<div class="form-group">
								<div class="col-md-8">
								<label class="label_bancamiga">Unidad Responsable</label>	
								<select name="test" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Departamento">
											<option value="1">Seguridad</option>
										</optgroup>
									</select>
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">Fecha de Solución</label>		
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				
					</div>
			
				</div>
			<!-- Tercera parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. OBSERVACIONES
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<textarea class="form-control autogrow" id="field-ta" name="motivo" required></textarea>
							</div>
							</div>
					</div>
				</div>	
			<!-- cuarta parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV.  PROCESADO POR :
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES DEL SOLICITANTE:</label>
								<input type="text" class="form-control" disabled value="{{Auth::user()->name}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES SUPERVISOR INMEDIATO</label>
								<select name="id_supervisor" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Supervisores">
											@php
                                        $supervisor = App\Supervisor::all();
                                        @endphp
                                        @foreach($supervisor as $key)
											<option value="{{$key->id}}">{{$key->nombre_supervisor}} {{$key->apellido_supervisor}}</option>
										@endforeach	
										</optgroup>
									</select>
							</div>
							
							</div>
				
					</div>
				</div>
	<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
						</form>
				</div>
			
			</div>
	
<script type="text/javascript">
	
</script>
@endsection