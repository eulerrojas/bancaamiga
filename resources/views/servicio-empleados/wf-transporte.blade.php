@extends('layouts.workflow')

@section('content')
		<div class="row" style="padding-left: 40px">
			<h3 style="margin-left: 30px">SOLICITUD DE SERVICIO DE TRANSPORTE</h3>
		<br />
		<ul>
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul> 
			<div class="col-md-12">
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registar_workflow_transporte')}}" method="POST" name="fvalida">
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I. DATOS DEL SOLICITANTE
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombres}} {{$dato->apellidos}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombre_valor}} {{$dato->cedula}}">
							</div>
						
							</div>
							
							<div class="form-group">
								<div class="col-md-4">
								<label class="label_bancamiga">NRO. CELULAR</label>	
								<input type="text" class="form-control" disabled value="{{$dato->celular}}">
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">UNIDAD ADMINISTRATIVA</label>		
								<input type="text" class="form-control" disabled value="{{$dato->descripcion}}">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">CARGO</label>	
								<input type="text" class="form-control" disabled value="{{$dato->nombre_roles}}">
							</div>
							</div>
				
					</div>
				</div>
				<!-- Segundo formulario -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DATOS DEL SERVICIO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-2">
								<label style=" float: right;" class="label_bancamiga">TIPO DE SERVICIO :</label>
							
							</div>
							
							<div class="col-md-10">
					<select name="servicio_id" class="select2" data-allow-clear="true" data-placeholder="Seleccione ..."  onchange="if(this.value=='0') document.getElementById('servicio_otros').disabled = false">
										<option></option>
										@php
                                        $tipo_serv = App\Tipo_servicio::all();
                                        @endphp
										<optgroup label="Servicio">
										@foreach($tipo_serv as $key)
										<option value="{{$key->id}}">{{($key->descripcion)}}</option>
										@endforeach
										<option value="0" name="servicio_otros">Otros</option>
										</optgroup>

									</select>
							
							
							</div>
							
							</div>
							<div class="form-group">
									<div class="col-md-2">
								<label style=" float: right;" class="label_bancamiga">Sugerencia:</label>
							
							</div>
							
							<div class="col-md-10">
							<input type="text" class="form-control" id="servicio_otros" disabled="disabled" name="servicio_otros" size="12">
							
							</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-2">
								<label class="label_bancamiga">FECHA </label>	
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha"  autofocus="autofocus">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga"> HORA</label>	
								<input type="text" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="12:00 AM" data-show-meridian="true" data-minute-step="5" name="hora"  autofocus="autofocus" />
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° PERSONAS</label>	
								<select class="form-control" name="cantp_id">
										@php
                                        $cant_p = App\Cant_persona::all();
                                        @endphp
                                        <option value="0">---</option>
                                        @foreach($cant_p as $key)
										<option value="{{$key->id}}" name="cantp_id">{{($key->cantidad)}}</option>
										@endforeach
									</select>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° DE UNIDADES</label>	
								<select class="form-control" name="unidad_id">
										@php
                                        $cant_u = App\Cant_unidad::all();
                                        @endphp
                                        <option value="0">---</option>
                                        @foreach($cant_u as $key)
										<option value="{{$key->id}}" name="unidad_id">{{($key->cantidad)}}</option>
										@endforeach
									</select>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">IDA Y VUELTA</label>	
								<div class="row">
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio" id="si" value="1" name="ivuelta" checked >
										<label style="font-size: 10px">Si</label>
									</div>
  									</div>
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio" id="no" value="2" name="ivuelta" checked >
										<label style="font-size: 10px">No</label>
									</div>
  									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">T. DE ESPERA</label>	
								<select name="espera" class="form-control">
										<option value="30 mm">30 mm</option>
										<option value="01:00 h">01:00 h</option>
										<option value="01:30 h">01:30 h</option>
										<option value="02:00 h">02:00 h</option>
										<option value="02:30 h">02:30 h</option>
										<option value="03:00 h">03:00 h</option>
									</select>
							</div>
							</div>
				
					</div>
				</div>
			<!-- Tercera parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. MOTIVO DE TRASLADO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<textarea class="form-control autogrow" id="field-ta" name="motivo" autofocus="autofocus"></textarea>
							</div>
							</div>
					</div>
				</div>	
			<!-- cuarta parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV. PROCESADO POR :
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES DEL SOLICITANTE:</label>
								<input type="text" class="form-control" disabled value="{{Auth::user()->name}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombre_valor}} {{$dato->cedula}}">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES SUPERVISOR INMEDIATO</label>
								<select name="id_supervisor" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Supervisores">
										
                                        @foreach($supervisores as $key)
											<option value="{{$key->id_rol}}">{{$key->nombres}} {{$key->apellidos}}</option>
										@endforeach
										</optgroup>
									</select>
							</div>
							
							</div>
				
					</div>
				</div>	
	<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="button" onclick="valida_envia()" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
						</form>
				</div>
			
			</div>

<script type="text/javascript">
function valida_envia(){ 
	
   	//Validación Tipo de Servicio
   	if (document.fvalida.servicio_id.selectedIndex==0){ 
      	alert("Debe seleccionar una Opcion Tipo de Servicio") 
      	document.fvalida.servicio_id.focus() 
      	return 0; 
   	} 
	//Validación Fecha
   	if (document.fvalida.fecha.value.length==0){ 
      	alert("Asignar Fecha de Solicitud de Servicio") 
      	document.fvalida.fecha.focus() 
      	return 0; 
   	} 
   	//Validación hora
   	if (document.fvalida.hora.value.length==0){ 
      	alert("Asignar Hora de Solicitud de Servicio") 
      	document.fvalida.hora.focus() 
      	return 0; 
   	} 
   		//Validación cant personas
   	if (document.fvalida.cantp_id.selectedIndex==0){ 
      	alert("Debe indicar la Cantidad de Personas en su Solicitud") 
      	document.fvalida.cantp_id.focus() 
      	return 0; 
   	} 
   		//Validación cant personas
   	if (document.fvalida.unidad_id.selectedIndex==0){ 
      	alert("Debe indicar la Cantidad de Unidades en su Solicitud") 
      	document.fvalida.unidad_id.focus() 
      	return 0; 
   	}
 		//Validación hora
   	if (document.fvalida.espera.value.length==0){ 
      	alert("Asignar Tiempo de Espera de Solicitud de Servicio") 
      	document.fvalida.espera.focus() 
      	return 0; 
   	} 
   	if (document.fvalida.motivo.value.length==0){ 
      	alert("Indique el Motivo de Solicitud de Servicio") 
      	document.fvalida.motivo.focus() 
      	return 0; 
   	}
   	//valido el interés 
   	if (document.fvalida.id_supervisor.selectedIndex==0){ 
      	alert("Debe seleccionar el Supervisor Inmediato.") 
      	document.fvalida.id_supervisor.focus() 
      	return 0; 
   	} 

   	//el formulario se envia 
   	alert("Registro de Solicitud de Transporte, Exitoso!"); 
   	document.fvalida.submit(); 
}
</script>
@endsection
