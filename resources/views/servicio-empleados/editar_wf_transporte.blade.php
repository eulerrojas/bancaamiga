@extends('layouts.workflow')

@section('content')
	<div class="row" style="padding-left: 40px">
			<h3 style="margin-left: 30px">REVISIÓN DE DATOS -> SOLICITUD DE SERVICIO DE TRANSPORTE</h3>
		<br />
		<ul>
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul> 
			<div class="col-md-12">
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('actualizarwftransporte')}}" method="POST" name="fvalida">
			<input type="hidden" name="user_id" value="{{$dato->user_id}}"> 
			 <input type="hidden" name="id" value="{{$transporte->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I. DATOS DEL SOLICITANTE
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombres}} {{$dato->apellidos}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombre_valor}} {{$dato->cedula}}">
							</div>
						
							</div>
							
							<div class="form-group">
								<div class="col-md-4">
								<label class="label_bancamiga">NRO. CELULAR</label>	
								<input type="text" class="form-control" disabled value="{{$dato->celular}}">
							</div>
								<div class="col-md-4">
								<label class="label_bancamiga">UNIDAD ADMINISTRATIVA</label>		
								<input type="text" class="form-control" disabled value="{{$dato->descripcion}}">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">CARGO</label>	
								<input type="text" class="form-control" disabled value="{{$dato->nombre_roles}}">
							</div>
							</div>
				
					</div>
				</div>
				<!-- Segundo formulario -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DATOS DEL SERVICIO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-2">
								<label style=" float: right;" class="label_bancamiga">TIPO DE SERVICIO :</label>
							
							</div>
							
							<div class="col-md-10">
					<select name="servicio_id" class="select2" data-allow-clear="true" onchange="if(this.value=='0') document.getElementById('servicio_otros').disabled = false">
										<option></option>

										@foreach($tipo_serv as $key)
										<option value="{{$transporte->servicio_id}}" @if( old('servicio_id') == $key->servicio_id) selected="selected" @endif>{{$key->descripcion}}</option>
										@endforeach
										<option value="0" name="servicio_otros">Otros</option>

									</select>
							
							
							</div>
							
							</div>
							<div class="form-group">
									<div class="col-md-2">
								<label style=" float: right;" class="label_bancamiga">Sugerencia:</label>
							
							</div>
							
							<div class="col-md-10">
							<input type="text" class="form-control" id="servicio_otros" disabled="disabled" name="servicio_otros" size="12" value="{{$transporte->servicio_otros}}">
							
							</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-2">
								<label class="label_bancamiga">FECHA </label>	
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha"  autofocus="autofocus" value="{{$transporte->fecha}}" disabled="disabled">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga"> HORA</label>	
								<input type="text" class="form-control timepicker" data-template="dropdown" data-show-seconds="true"  data-show-meridian="true" data-minute-step="5" name="hora"  autofocus="autofocus"  value="{{$transporte->hora}}" disabled="disabled"/>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° PERSONAS</label>	
								<select class="form-control" name="cantp_id">
										@php
                                        $cant_p = App\Cant_persona::all();
                                        @endphp
                                        @foreach($cant_p as $key)
										<option value="{{$transporte->cantp_id}}" @if( old('cantp_id') == $key->cantp_id) selected="selected" @endif>{{$transporte->cantp_id}}</option>
										@endforeach
									</select>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° DE UNIDADES</label>	
								<select class="form-control" name="unidad_id">
										@php
                                        $cant_u = App\Cant_unidad::all();
                                        @endphp
                                        @foreach($cant_u as $key)
										<option value="{{$transporte->unidad_id}}" @if( old('unidad_id') == $key->unidad_id) selected="selected" @endif>{{$transporte->unidad_id}}</option>
										@endforeach
									</select>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">IDA Y VUELTA</label>	
								<div class="row">
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio" id="si" value="1" name="ivuelta" checked >
										<label style="font-size: 10px">Si</label>
									</div>
  									</div>
  									<div class="col-xs-6 col-md-6">
  										<div class="radio radio-replace">
										<input type="radio" id="no" value="2" name="ivuelta" checked >
										<label style="font-size: 10px">No</label>
									</div>
  									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">T. DE ESPERA</label>	
								<input type="text" class="form-control" name="espera" value="{{$transporte->espera}}">
							</div>
							</div>
				
					</div>
				</div>
			<!-- Tercera parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. MOTIVO DE TRASLADO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<textarea class="form-control autogrow" id="field-ta" name="motivo" autofocus="autofocus">{{$transporte->motivo}}</textarea>
							</div>
							</div>
					</div>
				</div>	
			<!-- cuarta parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV. PROCESADO POR :
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES DEL SOLICITANTE:</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombres}} {{$dato->apellidos}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="{{$dato->nombre_valor}} {{$dato->cedula}}">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">APELLIDOS Y NOMBRES SUPERVISOR INMEDIATO</label>
								<select name="id_supervisor" class="select2" data-allow-clear="true">
									<option></option>
                                        @foreach($supervisores as $key)
											<option value="{{$transporte->id_supervisor}}" @if( old('id_supervisor') == $key->id_supervisor) selected="selected" @endif>{{$key->nombres}} {{$key->apellidos}}</option>
										@endforeach
									</select>
							</div>
							
							</div>
				
					</div>
				</div>	
						<!-- cuarta parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							V. USO EXCLUSIVO PARA LA GERENCIA DE ADMINISTRACIÓN :
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">RECIBIDO POR:</label>
								<input type="text" class="form-control" disabled value="{{Auth::user()->name}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">FECHA DE REVISIÓN</label>
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha_revision"  autofocus="autofocus">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-12">
								<label class="label_bancamiga">OBSERVACIONES</label>
								<textarea class="form-control autogrow" id="field-ta" name="observacion_dev" autofocus="autofocus"rows="3"></textarea>
							</div>
							
							</div>
				
					</div>
				</div>
					<!-- cuarta parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							ESTATUS DE LA SOLICITUD DE SERVICIO DE TRANSPORTE 
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group"  style="text-align: center">
							
						<div class="col-md-12">
							<div class="row">
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="1" name="estado" value="aprobado">
										<label style="font-size: 12px; font-weight: bold; color: #008945">APROBADO</label>
									</div>
  									</div>
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="2" name="estado" value="anulado">
										<label style="font-size: 12px; font-weight: bold; color: #ee0a0a">ANULADO</label>
									</div>
  									</div>
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="3" name="estado" value="devuelto">
										<label style="font-size: 12px; font-weight: bold; color: #f8c259">DEVUELTO</label>
									</div>
  									</div>
								</div>
							</div>
							</div>
					</div>
				</div>
	<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="button" onclick="valida_envia()" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
						</form>
				</div>
			
			</div>

<script type="text/javascript">
function valida_envia(){ 
	
   	//Validación Tipo de Servicio
   	if (document.fvalida.servicio_id.selectedIndex==0){ 
      	alert("Debe seleccionar una Opcion Tipo de Servicio") 
      	document.fvalida.servicio_id.focus() 
      	return 0; 
   	} 
	//Validación Fecha
   	if (document.fvalida.fecha.value.length==0){ 
      	alert("Asignar Fecha de Solicitud de Servicio") 
      	document.fvalida.fecha.focus() 
      	return 0; 
   	} 
   	//Validación hora
   	if (document.fvalida.hora.value.length==0){ 
      	alert("Asignar Hora de Solicitud de Servicio") 
      	document.fvalida.hora.focus() 
      	return 0; 
   	} 
   		//Validación cant personas
   	if (document.fvalida.cantp_id.selectedIndex==0){ 
      	alert("Debe indicar la Cantidad de Personas en su Solicitud") 
      	document.fvalida.cantp_id.focus() 
      	return 0; 
   	} 
   		//Validación cant personas
   	if (document.fvalida.unidad_id.selectedIndex==0){ 
      	alert("Debe indicar la Cantidad de Unidades en su Solicitud") 
      	document.fvalida.unidad_id.focus() 
      	return 0; 
   	}
 		//Validación hora
   	if (document.fvalida.espera.value.length==0){ 
      	alert("Asignar Tiempo de Espera de Solicitud de Servicio") 
      	document.fvalida.espera.focus() 
      	return 0; 
   	} 
   	if (document.fvalida.motivo.value.length==0){ 
      	alert("Indique el Motivo de Solicitud de Servicio") 
      	document.fvalida.motivo.focus() 
      	return 0; 
   	}
   	//valido el interés 
   	if (document.fvalida.id_supervisor.selectedIndex==0){ 
      	alert("Debe seleccionar el Supervisor Inmediato.") 
      	document.fvalida.id_supervisor.focus() 
      	return 0; 
   	} 
  		
   	//el formulario se envia 
   	alert("Revisión de Solicitud de Transporte, Exitoso!"); 
   	document.fvalida.submit(); 
}
</script>
@endsection
