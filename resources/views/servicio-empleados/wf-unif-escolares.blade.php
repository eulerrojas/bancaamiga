@extends('layouts.workflow')

@section('content')
	<div class="row" style="padding-left: 40px">
			<h3 style="margin-left: 30px">SOLICITUD DE FINANCIAMIENTO DE UNIFORMES ESCOLARES</h3>
		<br />
			<div class="col-md-12">
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registar_workflow_transporte')}}" method="POST">
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
						{{ csrf_field() }}

				<div class="panel panel-primary" data-collapsed="0">
					
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							I. DATOS DEL SOLICITANTE
						</div>
					</div>
					
					<div class="panel-body">

							<div class="form-group">
								
							<div class="col-md-4">
								<label class="label_bancamiga">NOMBRES Y APELLIDOS</label>
								<input type="text" class="form-control" disabled value="{{Auth::user()->name}}">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">CÉDULA DE IDENTIDAD</label>
								<input type="text" class="form-control" disabled value="">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">CARGO QUE DESEMPEÑA</label>
								<input type="text" class="form-control" disabled value="">
							</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-4">
								<label class="label_bancamiga">UNIDAD ADMINISTRATIVA</label>	
								<input type="text" class="form-control" disabled value="">
							</div>
								<div class="col-md-2">
								<label class="label_bancamiga">FECH. INGRESO</label>		
								<input type="text" class="form-control" disabled value="">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">ANTIGUEDAD</label>	
								<input type="text" class="form-control" disabled value="">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">N° TELÉFONICO</label>	
								<input type="text" class="form-control" disabled value="">
							</div>
							</div>
				
					</div>
				</div>
				<!-- Segundo formulario -->
					<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							II. DATOS DEL CRÉDITO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
							<div class="col-md-2">
								<label class="label_bancamiga"> MONTO</label>	
								<input type="text" class="form-control" value="Bs.">
							</div>	
							<div class="col-md-2">
								<label class="label_bancamiga">PLAZO A MESES</label>	
								<input type="text" class="form-control" disabled value="12 MESES" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">N° DE CUOTAS</label>	
								<input type="text" class="form-control" disabled value="12 CUOTAS" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">TASA DE INTERÉS</label>	
								<input type="text" class="form-control" disabled value="10,0 %" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">N° DE CUENTA BANCARIA BANCAMIGA </label>	
								<input type="text" class="form-control" placeholder="0000 - 0000 - 00 - 0000000000">
							</div>
							</div>
					</div>
				</div>
			<!-- Tercera parte -->
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							III. AUTORIZACIÓN
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12">
								<br>
							<p style="color: #000; text-align: justify;">YO, CECIBEL ROJAS LÓPEZ  TITULAR DE LA CÉDULA DE IDENTIDAD N° V- 18.761.015  - AUTORIZO A BANCAMIGA BANCO UNIVERSAL C.A.: 
							<ul>
								<li style="color: #000; text-align: justify;">QUE SE REALICE EL DESEMBOLSO DEL CRÉDITO APROBADO EN MI CUENTA BANCARIA DE BANCAMIGA, ESPECIFICADA EN EL PRESENTE FORMULARIO.</li>
								<li style="color: #000; text-align: justify;">QUE LAS CUOTAS CORRESPONDIENTES AL PAGO DE LA DEUDA REFLEJADAS EN EL <strong>PLAN DE PAGO</strong>, SEAN DESCONTADAS MENSUALMENTE EN LA FECHA INDICADA EN EL <strong>PLAN DE PAGO</strong>, DE MI CUENTA DE BANCAMIGA EN EL MES CORRESPONDIENTE, HASTA LA TOTAL CANCELACIÓN DEL MISMO. </li>
								<li style="color: #000; text-align: justify;">QUE EL MONTO DEUDOR SE DEDUZCA DEL CINCUENTA POR CIENTO (50%) DEL SALDO QUE RESULTEA MI FAVOR COMO TRABAJADOR POR CUALQUIER CONCEPTO DE LA PRESTACIÓN DE SERVICIO.</li>
							</ul>	
							</p>
							</div>
							</div>
					</div>
				</div>	
			<!-- cuarta parte -->
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title" style="background: #266ea7; text-align: left !important; border-radius: 2px; color: #fff; font-weight: 700;">
							IV. SOLO PARA USO DE LA GERENCIA DE CAPITAL HUMANO
						</div>
					</div>
					
					<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-4">
								<label class="label_bancamiga">DISPONIBLE EN PRESTACIONES SOCIALES</label>
								<input type="text" class="form-control" value="Bs.">
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">FECHA DE EVALUACIÓN DEL CREDITO</label>
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">EVALUADO POR:</label>
								<input type="text" class="form-control" id="field-1">
							</div>
							</div>
							
							<div class="form-group">
								
							<div class="col-md-6">
								<label class="label_bancamiga">RESULTADOS</label>
							<div class="row">
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" checked>
										<label style="font-size: 10px">APROBADO</label>
									</div>
  									</div>
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" checked>
										<label style="font-size: 10px">NEGADO</label>
									</div>
  									</div>
  									<div class="col-xs-4 col-md-4">
  										<div class="checkbox checkbox-replace color-primary">
										<input type="checkbox" id="chk-1" checked>
										<label style="font-size: 10px">DIFERIDO</label>
									</div>
  									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">PLAZO A MESES</label>	
								<input type="text" class="form-control" disabled value="12 MESES" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-4">
								<label class="label_bancamiga">FECHA DE DESCUENTO DE LA 1ERA CUOTA</label>
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
						<div class="form-group">
								
							<div class="col-md-2">
								<label class="label_bancamiga">N° DE CUOTAS</label>	
								<input type="text" class="form-control" disabled value="12 CUOTAS" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-3">
								<label class="label_bancamiga">FRECUENCIA DE PAGO</label>	
								<input type="text" class="form-control" disabled value="MENSUAL" style="text-align: center; font-weight: 700">
							</div>
							<div class="col-md-3">
								<label class="label_bancamiga">F. LIQUIDACIÓN DE LA CUENTA</label>
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">MONTO APROBADO</label>
								<input type="text" class="form-control" required value="Bs.">
							</div>
							<div class="col-md-2">
								<label class="label_bancamiga">MONTO CUOTA</label>
								<input type="text" class="form-control" required value="Bs.">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label class="label_bancamiga">OBSERVACIONES GENERALES DE LA GERENCIA DE CAPITAL HUMANO</label>
								<textarea class="form-control" id="field-ta" rows="4"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label class="label_bancamiga">OBSERVACIONES GENERALES DEL GERENTE (RESPONSABLE) DEL ÁREA</label>
								<textarea class="form-control" id="field-ta" rows="4"></textarea>
							</div>
						</div>
							<div class="form-group">
								
							<div class="col-md-8">
								<label class="label_bancamiga">PROCESADO POR :</label>
								<select name="id_supervisor" class="select2" data-allow-clear="true" data-placeholder="Seleccionar.">
										<option></option>
										<optgroup label="Supervisores">
											@php
                                        $supervisor = App\Supervisor::all();
                                        @endphp
                                        @foreach($supervisor as $key)
											<option value="{{$key->id}}">{{$key->nombre_supervisor}} {{$key->apellido_supervisor}}</option>
										@endforeach	
										</optgroup>
									</select>
							</div>
							
							<div class="col-md-4">
								<label class="label_bancamiga">FECHA</label>
								<input type="text" class="form-control datepicker" data-start-date="-2d" data-end-date="+1w" name="fecha" required>
							</div>
							</div>
					</div>
				</div>	
	<div class="panel-body">
							<div class="form-group">
								
							<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/servicios-empleados')}}" class="btn btn-default">Volver</a>
							</div>
						
						
							</div>
							
						
				
					</div>
						</form>
				</div>
			
			</div>
@endsection