@extends('layouts.app')

@section('content')


  <div class="login-form">
    <div class="login-content">
      
      <a href="index.html" class="logo">
        <img src="{{ asset('img/logoba.png') }}"  alt="" />
      </a>
      
      <h1 class="titulo_login">Regístrate</h1>
      <span>¿Ya tienes una cuenta con Bancamiga? <a href="{{asset('/')}}" style="font-weight: 700">Iniciar Sesión</a></span>
        <hr>
      
    </div>
    
    <div class="login-content">
      
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
        
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-user" style="color: #005694"></i>
            </div>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombres">                          
         @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
               </div>
        </div>
         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-mail" style="color: #005694"></i>
            </div>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Correo Electrónico">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
               </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          
          <div class="input-group">
            <div class="input-group-addon" style="color: #005694">
              <i class="entypo-key"></i>
            </div>
            
            <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
               @if ($errors->has('password'))
                 <span class="help-block">
                   <strong>{{ $errors->first('password') }}</strong>
                 </span>
                        @endif
            </div>
        </div>
          <div class="form-group">
          
          <div class="input-group">
            <div class="input-group-addon" style="color: #005694">
              <i class="entypo-key"></i>
            </div>
            
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="confirmar Contraseña">
              
            </div>
        </div>
        <div class="form-group">
          <button type="submit"  data-rel="reload" class="btn btn-primary btn-block btn-login" style="background-color: 
          #005694 !important">
            <i class="entypo-login"></i>
            Registrar
          </button>
                                    
        </div>
     
      </form>
   
      <div class="login-bottom-links">
    
        <a class="btn btn-link" href="{{asset('/')}}" style="font-weight: 700"> Volver</a>
        
        
      </div>
      
    </div>
    
  </div>

@endsection
