@extends('layouts.app')

@section('content')

  <div class="login-form">
    <div class="login-content">
      
      <a href="index.html" class="logo">
        <img src="{{ asset('img/logoba.png') }}"  alt="" />
      </a>
      
      <h1 class="titulo_login">Restablecer contraseña</h1>
      <span>Ingresa tu email y te enviaremos un enlace para restaurar tu contraseña.</span>
        <hr>
      
    </div>
    
    <div class="login-content">
      
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
        
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-mail" style="color: #005694"></i>
            </div>
             <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
               </div>
        </div>
        
 
        <div class="form-group">
          <button type="submit"  data-rel="reload" class="btn btn-primary btn-block btn-login" style="background-color: 
          #005694 !important">
            <i class="entypo-login"></i>
            Restablecer Contraseña
          </button>
                                    
        </div>
     
      </form>
   
      <div class="login-bottom-links">
    
        <a class="btn btn-link" href="{{ asset('/') }}" style="font-weight: 700"> Volver</a>
        
        
      </div>
      
    </div>
    
  </div>

@endsection
