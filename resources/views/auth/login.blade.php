@extends('layouts.app')

@section('content')

  <div class="login-form">
    <div class="login-content">
      
      <a href="index.html" class="logo">
        <img src="{{ asset('img/logoba.png') }}"  alt="" />
      </a>
      
      <h1 class="titulo_login">Iniciar Sesión</h1>
      <span>¿Nuevo en Bancamiga? <a href="{{asset('register')}}" style="font-weight: 700">Regístrate</a></span>
        <hr>
      
    </div>
    
    <div class="login-content">
      
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
        
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-mail" style="color: #005694"></i>
            </div>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email" >                          
          @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
                      @endif
               </div>
        </div>
        
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          
          <div class="input-group">
            <div class="input-group-addon" style="color: #005694">
              <i class="entypo-key"></i>
            </div>
            
            <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
               @if ($errors->has('password'))
                 <span class="help-block">
                   <strong>{{ $errors->first('password') }}</strong>
                 </span>
                        @endif
            </div>
        </div>
        
        <div class="form-group">
          <button type="submit"  data-rel="reload" class="btn btn-primary btn-block btn-login" style="background-color: 
          #005694 !important">
            <i class="entypo-login"></i>
            Iniciar Sesión
          </button>
                                    
        </div>
     
      </form>
   
      <div class="login-bottom-links">
    
        <a class="btn btn-link" href="{{ route('password.request') }}" style="font-weight: 700"> Recuperar Contraseña?</a>
        
        
      </div>
      
    </div>
    
  </div>

@endsection
