@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Módulo Solicitudes</h3>
     </div>
      <br>
      <br>
      <br>  
            <div class="col-md-12">
   <div class="col-md-3"></div>
        <div class="col-md-3"></div>
         <div class="col-md-6">
            <div class="form-group">
       <form role="search" class="form-horizontal form-groups-bordered" action="{{url('solicitudes')}}" method="GET" >
                                    
                        
                         <div class="input-group">
                             <input type="text" id="titulo_solicitud" name="titulo_solicitud" class="form-control" placeholder="Teclee para ver sugerencias...">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
 
        </div>
        </div>
 </div> 
     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
   
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                         
                            <th style="font-size: 12px !important">Título</th>
                            <th style="font-size: 12px !important">Imagen</th>
                            <th style="font-size: 12px !important">Link</th>
                            <th style="font-size: 12px !important">Fecha de Creación</th>
                            <th style="font-size: 12px !important">Opciones</th>
                        </tr>
                    </thead>
                  <tbody>
                    @foreach($solicitud as $key)
                      <tr> 
                      
                        <td>{{$key->titulo_solicitud}}</td>
                        <td><img src="/solicitud/{{$key->image_solicitud}}" alt="..." width="60px"></td>
                        <td>{{$key->link}}</td>
                        <td>{{$key->created_at}}</td>
                        <td>
                            <button type="button" class="btn btn-default">
                              <a href="{{url('editarsolicitud', $key->id)}}" onclick="return confirm('Deseas Actualizar? ')"><i class="fa fa-pencil" title="Editar"></i></a>
                            </button>
                        </td>
                      </tr>
                    @endforeach                     
                    </tbody>
            </table>
            <div style="float: right;">{!! $solicitud->render() !!} </div> 
        </div>
    </div>
@endsection