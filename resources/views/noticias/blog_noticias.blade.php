@extends('layouts.intranet')

@section('content')

<section class="post-content-section" style="margin-top: 10px;">
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
<div class="container">
	

    <div class="well"> 
        <div class="row">
       
             <div class="col-md-12">
             	 @if(!empty($blog)) 
                 <div class="row hidden-md hidden-lg"><h1 class="text-center" ></h1></div>
                     
                 <div class="pull-left col-md-4 col-xs-12 thumb-contenido"><img class="center-block img-responsive" src='/noticia/{{$blog->image }}'/></div>
                 <div class="">
                     <h1  class="hidden-xs hidden-sm">{{$blog->titulo}}</h1>
                     <hr>
                     <small>Fecha: {{$blog->created_at}}</small><br>
                      <small>Categoría: <strong>{{$valor->nombre}}</strong></small>
                     <hr>
                     <p class="text-justify">{!! html_entity_decode($blog->contenido) !!}</div>

                  @else
      <div class="row">
      
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-warning"><strong>Observación!</strong> No hay Información de esta sección en la Base de Datos</div>
      </div>

    </div>

    @endif 
             </div>

        </div>
    </div>
</div>
<hr>

             </div>
</section>
@endsection