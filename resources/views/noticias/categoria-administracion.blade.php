@extends('layouts.intranet')

@section('content')
<section class="post-content-section" style="margin-top: 10px">
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
         
          	<li>Filtro: Administración</li>
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom: 50px">
           @if(!empty($blog_categoria))
            	<div class="row blog-row">
            @foreach($blog_categoria as $key) 		
			<div class="col-md-4 col-sm-4 col-xs-4">
			<a href="#">
				<img class="img-responsive center-block" src="noticia/{{$key->image }}" height="250">
				</a>
				<div class="blog-content bg-white">
				<h3>{{$key->titulo }}</h3>
				<p>Categoría : <a href="#">{{$key->nombre }}</a></p>
				<p>{!! html_entity_decode($key->extracto) !!}....</p>
				<hr>
				<p><span>Fecha: <i class="fa fa-calendar"></i> {{$key->created_at }}
				 </span> 
				<span class="pull-right"><strong><a href="{{url('blog_noticias',$key->id)}}"  class="heading_color">  Continuar leyendo <i class="fa fa-angle-right"></i></a></strong></span> </p>
				</div>
			</div>
@endforeach
		</div>
        @else
      <div class="row">
      
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-warning"><strong>Observación!</strong> No hay Noticias Relacionadas a esta Categoría.</div>
      </div>

    </div>

    @endif 
		{!! $blog_categoria->render() !!} 
             </div>
        </div>
      

    </div> <!-- /container -->
</section>
@endsection