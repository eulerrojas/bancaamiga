@extends('layouts.intranet')

@section('content')

<section class="post-content-section" style="margin-top: 10px">
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
    
          <li>
            <a href="{{asset('/home')}}">
              Volver a HOME <i class="fa fa-mail-reply"></i>
            </a>
          </li>
        </ul>
    
        </div>
      </div>
    </div>
    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom: 50px">
          
            	<div class="row blog-row">
            @foreach($blog as $key) 		
			<div class="col-md-4 col-sm-4 col-xs-4">
			<a href="#">
				<img class="img-responsive center-block" src="noticia/{{$key->image }}" height="250">
				</a>
				<div class="blog-content bg-white">
				<h3>{{$key->titulo }}</h3>
				<p>Categoría : <a href="#">{{$key->nombre }}</a></p>
				<p>{{$key->extracto }}....</p>
				<hr>
				<p><span>Fecha: <i class="fa fa-calendar"></i> {{$key->created_at }}
				 </span> 
				<span class="pull-right"><strong><a href="javascript::;"  class="heading_color">  Continuar leyendo <i class="fa fa-angle-right"></i></a></strong></span> </p>
				</div>
			</div>
@endforeach
		</div>
		{!! $blog->render() !!} 
             </div>
    <!-- usar despues        <div class="col-lg-3  col-md-3 col-sm-12" style="border-left: 1px solid #e3e3e3 !important;">
                
                <div class="well" style="background-color: #fff !important; border: 1px solid #fff !important">
                	    <h2>Noticias Recientes</h2>
                	    <hr>
                	<br>  
                	@foreach($blog as $key)  
                    <div class="media"> <div class="media-left"> <a href="#"> <img data-src="holder.js/64x64" class="media-object" alt="64x64" style="width: 64px; height: 64px;" src="noticia/{{$key->image }}" data-holder-rendered="true"> </a> </div> <div class="media-body"> <h4 class="media-heading">{{$key->titulo }}</h4>{{$key->created_at }}</div> </div>
                    @endforeach
                </div>
                <hr>
                <div class="well" style="background-color: #fff !important; border: 1px solid #fff !important">
                	 <h2>Categorías</h2>
                	    <hr>
                	 <div class="row" style="text-align: center">
 						<div class="col-xs-6 col-sm-6 col-lg-6"><a href="#" class="btn boton_categoria">Institucional</a></div>
  						<div class="col-xs-6 col-sm-6 col-lg-6"><a href="#" class="btn boton_categoria">Administración </a></div>
					</div>
					<br>
					<div class="row" style="text-align: center">
 						<div class="col-xs-6 col-sm-6 col-lg-6"><a href="#" class="btn boton_categoria">Gestión Humana</a></div>
  						<div class="col-xs-6 col-sm-6 col-lg-6"><a href="#" class="btn boton_categoria">Todos</a></div>
					</div>   
                	<br> 
                </div>
            </div> -->
        </div>
      

    </div> <!-- /container -->
</section>
@endsection