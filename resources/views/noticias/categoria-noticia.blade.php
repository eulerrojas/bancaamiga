@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Nueva Categoría</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/ba-admin')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar Noticias
                </a>
              </li>
            
              <li><a href="{{asset('/nueva-noticias')}}">
              <i class="fa fa-folder-open"></i>
              Crear Noticia</a></li>
            </ol>
          </div>   
     <br>
      <br>
      <br> 
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger content"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
<div class="row">
  <div class="col-md-12">
   <div class="col-md-6"> 
    <form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('registrar-categoria')}}">
      {{ csrf_field() }} 
                   <div class="panel-body">
                          <div class="form-group">
                             <label>Nombre Categoría :</label>
                             <input type="text" class="form-control"  name="nombre" id="nombre" required="required" autofocus>
                          </div>
                   <div class="form-group">
                            <label>Descripción :</label>      
                            <textarea class="form-control" id="field-ta" name="descripcion" required="required"></textarea>
                            
                    </div>
                            <div class="form-group">
        <div class="col-sm-12" style="text-align: right;">
        <button type="submit" class="btn btn-primary">Registrar</button>
        <button type="reset" class="btn btn-default">Limpiar</button> 
        </div>
              </div>   
                        </div>
                  </form>
                    
                </div>
        <div class="col-md-6">
          <div class="panel-body">
            <div class="form-group">
      <div class="col-md-12">
                            <form role="search" class="form-horizontal form-groups-bordered" action="{{url('categoria-noticia')}}" method="GET" >
                              
                        
                         <div class="input-group">
                             <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Buscar Categoría">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
                        </div>
                        <br>
                  </div>
                  <br>
       @if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Mensaje!</strong> {{Session::get('msj')}}
    </div>
         </div>
        @endif
        @if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Alerta!</strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif
       @if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Mensaje!</strong> {{Session::get('mjs2')}}
    </div>
         </div>
        @endif    
        <table class="table table-bordered responsive">
          <thead>
            <caption style="background-color: #5686a5; color: #fff; text-align: center;">Listado de Categorías</caption>
            <tr>
             
                <th>Categoría</th>
                <th>Descripción</th>
                <th>Acciones</th></tr>
          </thead>
          <tbody>
                @foreach($cat as $key)
                  <tr>
        
                    <td>{{($key->nombre)}}</td> 
                    <td>{{($key->descripcion)}}</td>
                    <td width="25%">
                    <a href="{{url('editarcategoria', $key->id)}}" class="btn btn-default" onclick="return confirm('Deseas Actualizar? ')"> <i class="fa fa-pencil"></i></a>
                    <a href="{{url('eliminar_cat', $key->id)}}" class="btn btn-danger" onclick="return confirm('Deseas Eliminar? ')"> <i class="fa fa-trash"></i></a>
                    </td>
                 </tr>
               @endforeach                                          
          </tbody>
        </table>
         {!! $cat->render() !!} 
          </div>
        </div>          
</div>
</div>
@endsection