@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar Noticia</h3>
     </div>
</div> 
<br>
    <!-- Breadcrumb 1 -->
     <div class="col-lg-12 col-md-12 col-xs-12">
            <ol class="breadcrumb">
              <li>
                <a href="{{asset('/ba-admin')}}">
                  <i class="fa fa-reorder"></i>
                 Consultar Noticias
                </a>
              </li>
            
              <li><a href="{{asset('/categoria-noticia')}}">
              <i class="fa fa-folder-open"></i>
              Crear Categoría</a></li>
            </ol>
          </div>   
     <br>
      <br>
      <br>  
<ul>
    @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger content"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul>
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarnoticia')}}" enctype="multipart/form-data">
      <input type="hidden" name="id" value="{{$noticia->id}}"> 
      {{ csrf_field() }}  
							    <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Imagen</label>
                
                <div class="col-sm-5">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                      <img src="/noticia/{{$noticia->image}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                    <div>
                      <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Seleccionar imagen</span>
                        <span class="fileinput-exists">Cambio</span>
                        <input type="file" name="image" accept="image/*" value="{{$noticia->image}}">
                      </span>
                      <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Retirar</a>
                    </div>
                    <span style="font-size: 10px; color: red"><strong>Atención!</strong> La Imágen debe tener un tamaño de <strong>1920px ancho</strong> y<strong> 980px alto</strong></span>
                  </div>
                </div>
              </div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Título Principal</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="titulo" required="required" value="{{$noticia->titulo}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Subtítulo</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="subtitulo" required="required" value="{{$noticia->subtitulo}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Extracto</label>
								
								<div class="col-sm-8">
									<textarea class="form-control" id="field-ta" name="extracto" maxlength="150" required="required">{{$noticia->extracto}}</textarea>
								<span style="font-size: 10px; color: red"><strong>Atención!</strong> Extracto corto de<strong>100 letras</strong></span>
								</div>
							</div>
							<div class="form-group">
								<label for="field-ta" class="col-sm-3 control-label">Contenido de la Noticia</label>
								
								<div class="col-sm-8">
												<textarea class="form-control ckeditor"  name="contenido">{{$noticia->contenido}}</textarea>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Categoría</label>
								
								<div class="col-sm-8">
									<select name="id_catenot" class="select2" data-allow-clear="true" data-placeholder="Seleccione Categoría..." required="required">
										<option></option>
										@php
                                        $categoria = App\Categoria_noticia::all();
                                        @endphp
										<optgroup label="Categorías">
										@foreach($categoria as $key)
										<option value="{{$key->id}}">{{($key->nombre)}}</option>
										@endforeach
										</optgroup>
									</select>
								</div>
							</div>
						
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Actualizar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/solicitudes')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
@endsection