@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Archivos Generales Gestión Humana en Formato PDF</h3>
     </div>
      <br>
      <br>
      <br>  

     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 
           <div class="panel-body">

                    <div class='table-responsive'>
                      <table class='table table-bordered table-hover' >
                   <thead style="color: #000000 !important">
                        <tr style="color: #000000 !important">
                            <th style="font-size: 12px !important">Nombre del Documento</th>
                            <th style="font-size: 12px !important">PDF</th>
                            <th style="font-size: 12px !important">Categoría</th>
                            <th style="font-size: 12px !important">Fecha de Creación</th>
                            <th style="font-size: 12px !important">Usuario</th>
                            
                        </tr>
                    </thead>
                  <tbody>
      
             @foreach($data as $key)
                      <tr> 
                        <td>{{$key->nombre_doc}}</td>
                        <td>{{$key->file_doc}}</td>
                        <td>{{$key->nombre_documentos}}</td>
                        <td>{{$key->created_at}}</td>
                        <td>{{$key->name}}</td>
                      </tr>
               @endforeach                               
                    </tbody>
            </table>

         <div style="float: right;"></div> 
        </div>
     
    </div>
@endsection