@extends('layouts.administrador')

@section('content')

<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Formulario <i class="fa fa-angle-right"></i>  Viaticos </h3>
     </div>
      <br>
      <br>
      <br>  

     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 

	<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
						<form role="form" class="form-horizontal form-groups-bordered">
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label" >Nombre del Docuemnto</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="field-1">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Documento</label>
								
								<div class="col-sm-5">
									
									<input type="file" class="form-control" />
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Categoría del Documento</label>
								
								<div class="col-sm-5">
									@php
                                        $doc = App\Categoria_documentos::all();
                                        @endphp
									<select class="form-control">
										<option>Seleccione</option>
										@foreach($doc as $key)
										<option>{{$key->nombre_documentos}}</option>
										@endforeach
									</select>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
							</div>
							</div>
						</form>
		
					</div>
				
				</div>
			
			</div>
		</div>
@endsection