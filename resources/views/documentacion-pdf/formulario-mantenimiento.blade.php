@extends('layouts.administrador')

@section('content')

<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Formulario  <i class="fa fa-angle-right"></i> Mantenimiento</h3>
     </div>
      <br>
      <br>
      <br>  

     <div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
         <div class="col-md-12">@if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif</div>
        <div class="col-md-12">@if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('mjs2')}}
    </div>
         </div>
        @endif</div>
 </div> 

	<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
		<form role="form" class="form-horizontal form-groups-bordered" action="{{url('registrosmantenimiento')}}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}"> 
							 {{ csrf_field() }}
							 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label" >Nombre del Docuemnto</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="field-1" name="nombre_doc">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Documento</label>
								
								<div class="col-sm-5">
									
									<input type="file" class="form-control" name="file_doc" />
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Categoría de Documentos</label>
								
								<div class="col-sm-5">
									
									<select class="form-control" name="id_catd">
										@php
                                        $cant_doc = App\Categoria_documento::all();
                                        @endphp
                                        @foreach($cant_doc as $key)
										<option value="{{$key->id}}">{{($key->nombre_documentos)}}</option>
										@endforeach
									</select>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
							</div>
							</div>
						</form>
		
					</div>
				
				</div>
			
			</div>
		</div>
@endsection