@extends('layouts.system')

@section('content')

<div class="container">
    <div class="row" style="margin-top: 150px">
                    <form method="POST" action="{{url('pdf/create')}}" accept-charset="UTF-8" enctype="multipart/form-data" >
                    	{{ csrf_field() }}
                    <div class="file-field input-field">
                      <div class="btn">
                        <span>Seleccione Archivo</span>
                        <input type="file" name="file">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                      </div>
                    </div>
                    <div class="input-field col s12">
                                <button style="background-color: #00538f;" class="btn waves-effect btn-small waves-light right" type="submit" name="action">Enviar</button>
                            </div>
                  </form>
                  </div>
</div>	
@endsection
