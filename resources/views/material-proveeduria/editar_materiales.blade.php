@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Materiales de Proveeduría <i class="fa fa-angle-right"></i> Editar Material </h3>
     </div>
</div> 
<br>
<div class="col-md-12">@if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>  
    <strong>Mensaje! </strong>{{Session::get('msj')}}
    </div>
         </div>
        @endif</div>
<ul>
   @foreach ($errors->all() as $error)
     <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="alert alert-danger"><strong>Alerta!</strong> {{ $error }} </div>
      </div>
    @endforeach
</ul> 
	<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-body">
						
<form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarproductosm')}}">
      {{ csrf_field() }} 
							<input type="hidden" name="id" value="{{$materiales->id}}"> 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Código del Material</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="cod_material"  value="{{$materiales->cod_material}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Descripción  </label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="field-1" name="nom_material"  value="{{$materiales->nom_material}}">
								</div>
							</div>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Categoría:</label>
								
								<div class="col-sm-8">
									<select name="id_categoria_material" class="select2" data-allow-clear="true" data-placeholder="Seleccione Oficina" required="required">
										<option></option>
										<optgroup label="Red de Agencias Bancarias">
										@php
                                        $cat_material = App\Categoria_materiales::all();
                                        @endphp
                                        @foreach($cat_material as $key)
											<option value="{{$key->id}}">{{$key->categoria_proveeduria}}</option>
										@endforeach	
										</optgroup>
									</select>
									
								</div>
							</div>
							<div class="form-group">
									<div class="col-md-12" style="text-align: center">
								<button type="submit" class="btn btn-default">Guardar</button>
								<button type="reset" class="btn btn-default">Limpiar</button>
								<a href="{{asset('/listado-almacenes')}}" class="btn btn-default">Volver</a>
							</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
@endsection