@extends('layouts.administrador')

@section('content')
<div class="row">
     <div class="col-sm-12 col-xs-12">
            <h3>Editar Material de Proveeduría</h3>
     </div>
</div> 
<br>

<div class="row">
  <div class="col-md-12">
   <div class="col-md-6"> 
    <form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{url('actualizarmateriales')}}">
      {{ csrf_field() }} 
           <input type="hidden" name="id" value="{{$cate_material->id}}"> 

                   <div class="panel-body">
                          <div class="form-group">
                             <label>Nombre Categoría :</label>
                             <input type="text" class="form-control"  name="categoria_proveeduria" value="{{$cate_material->categoria_proveeduria}}" autofocus>
                          </div>
                  
                            <div class="form-group">
        <div class="col-sm-12" style="text-align: right;">
        <button type="submit" class="btn btn-primary">Registrar</button>
        <button type="reset" class="btn btn-default">Limpiar</button> 
        </div>
              </div>   
                        </div>
                  </form>
                    
                </div>
        <div class="col-md-6">
          <div class="panel-body">
            <div class="form-group">
      <div class="col-md-12">
                            <form role="search" class="form-horizontal form-groups-bordered" action="{{url('categoria_materiales')}}" method="GET" >
                              
                        
                         <div class="input-group">
                             <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Buscar Categoría">
                             <span class="input-group-addon"><i class="fa fa-search"></i></span>
                         </div>
                         
                         </form>
                        </div>
                        <br>
                  </div>
                  <br>
       @if(Session::has('msj'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Mensaje!</strong> {{Session::get('msj')}}
    </div>
         </div>
        @endif
        @if(Session::has('mjs1'))
         <div class="col-md-12">
    <div class="alert alert-danger alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Alerta!</strong>{{Session::get('mjs1')}}
    </div>
         </div>
        @endif
       @if(Session::has('mjs2'))
         <div class="col-md-12">
    <div class="alert alert-success alert-dismissable content">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Mensaje!</strong> {{Session::get('mjs2')}}
    </div>
         </div>
        @endif    
        <table class="table table-bordered responsive">
          <thead>
            <caption style="background-color: #5686a5; color: #fff; text-align: center;">Listado de Categorías</caption>
            <tr>
             
                <th>Categoría</th>
                <th>Fecha de Registro</th>
                <th>Acciones</th></tr>
          </thead>
          <tbody>
          	   @php
          $materiales = App\Categoria_materiales::all();
        @endphp          	
        @foreach($materiales as $key)
                  <tr>
     
                    <td>{{$key->categoria_proveeduria}}</td> 
                    <td>{{$key->created_at}}</td>
                    <td width="25%">
                    <a href="{{url('edit-categoria-proveeduria', $key->id)}}" class="btn btn-default" onclick="return confirm('Deseas Actualizar? ')"> <i class="fa fa-pencil"></i></a>
                    <a href="{{url('eliminar_categoria_proveeduria', $key->id)}}" class="btn btn-danger" onclick="return confirm('Deseas Eliminar? ')"> <i class="fa fa-trash"></i></a>
                    </td>
                
                 </tr>
                   @endforeach  
          </tbody>
        </table>
          </div>
        </div>          
</div>
</div>
@endsection