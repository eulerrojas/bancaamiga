<div style="background-color: #f5f8fa; width: 85%; margin: 5px 5px 5px 7%; padding: 5% 5%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif">
    <div style="background-color: white; border-radius: 4px; border: 1px solid #d6d6d6; padding: 5% 2%">

        <div class="">
            <div class="columns large-12" style="margin-top: 20px">
                <center><img src="img/Bancamiga-BancoUniversal-EE.jpg" width="250"></center>
            </div>
        </div>

        <div class="" style="margin-top: 30px; margin-bottom: 15px; margin-left: 20px">
            <div class="columns large-12">
                <span style="font-size: 14px; font-weight: bold; color: #82888a;">Bienvenidos a Bancamiga intranet.</span>
                <br>
                <span style="font-size: 12px;  color: #82888a;">Servicio de Solicitud de Transporte {{$fecha}}</span>               
            </div>
        </div>

        <a href="#" style="text-decoration: none">

            <div  style="float: right; margin-right: 10px">
                <div align="right"><span style="font-size: 11px; color: #9c9c9c;"></span></div>
            </div>
            <div class="columns large-12" style="margin-top:10px;margin-bottom: 5px; margin-left: 15px">

                <table>
                    <tr>
                        <td style="font-size: 13px;line-height: 1.5; color: #b9b9b9">
                            <span><p>Congratulations! You are part of this incredible community. You can now get valuable answers to all your questions. Toortl was born because we believe that the transmission of knowledge is the best instrument to make a positive impact in the world, and you are already part of that mission.<br> 
                                    Welcome to a fascinating and inspiring community!<br><br> 
                                    Bancamiga Banco Universal, C.A. J-31628759-9,<br> 
                                    Sede Principal. Av. Francisco de Miranda con Av. Eugenio Mendoza, Edif. Sede Gerencial, Piso 2, Urb. La Castellana.  Código postal 1060. Caracas –Venezuela. Teléfonos: máster +58 (212)- 201.88.87.<br><br>
                                    </p></span>
                        </td>
                    </tr>    
                </table>

            </div>    
            <br>
<a href="" style="text-decoration: none">
            <div style="text-align: center; border-radius: 4px; padding: 10px 10px; background-color: #4c9db7; font-size: 13px !important; color: #fff">
                Revisar Solicitud de Servicio de Transporte
            </div>
        </a>
        </a>
    </div>
</div>
